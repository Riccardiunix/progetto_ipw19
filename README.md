# Aid Supply

## Descrizione progetto
> L’obiettivo di questo progetto è quello di creare un sito web per rendere più semplice e intuitiva la gestione del sistema sanitario. Grazie a questo sito pazienti, medici e il sistema sanitario provinciale  sono in grado di eseguire compiti come prescrivere un esame o pagare un ticket attraverso un’interfaccia pulita e semplice.

## Landing page(index.jsp)
> All’interno di questa pagina si possono trovare informazioni utili come, per esempio, i servizi offerti dal sito oppure i motivi per sceglierci. Inoltre si possono visualizzare le informazioni di tutti i medici registrati nel sito, di base e specialistici. In questa pagina è possibile infine eseguire il login.

## Homepage Paziente

> Il paziente nella sua homepage può visualizzare le sue informazioni
> personali, tutte le visite ed esami eseguiti e le ricette
> prescrittegli. Per quanto riguarda i report dei ticket pagati, puù
> trovarli cliccando un bottone nelle tabelle delle visite
> specialistiche e degli esami. Il paziente è in grado inoltre di
> entrare modificare il medico di base, la password e l’immagine di
> profilo tramite la voce “modifica dati” del menù in alto a destra

## Homepage Medico di Base

> All’interno di questa pagine un medico di base è in grado di
> visualizzare le proprie informazioni e la lista dei suoi pazienti. Può
> entrare nella scheda personale di ogni paziente semplicemente
> cliccando sul corrispondente bottone nell'entry della tabella

## Homepage Medico Specialista

> All’interno di questa pagina un medico specialista è in grado di
> visualizzare le proprie informazioni e la lista di tutti i pazienti. Può
> entrare nella scheda personale di ogni paziente semplicemente
> cliccando sul corrispondente bottone nell'entry della tabella

## Scheda paziente(paziente.jsp)

> In questa pagina ogni medico è in grado di sapere le informazioni personali relative ad un determinato paziente e le visite, gli esami e le ricette relative ad esso.
> Un medico di base in questa pagina può fare una semplice visita al paziente oppure prescrivere una visita specialistica, un esame o una ricetta. 
> Un medico specialista invece può eseguire una visita relativa alla sua specializzazione

### Codici di Errore
0: Utente non trovato

1: Errore su password cambiata

2: Errore su password dimenticata

3: Inserisci esame

4: Inserisci immagine

5: Inserisci immagine

6: Inserisci visita

7: Inserisci visita specialistica

8: Update esame

9: Update medico di base

10: Update ticket

11: Update visita specialista

