<%-- 
    Document   : error.jsp
    Created on : 29 ott 2019, 14:16:56
    Author     : orion
--%>
<%@ page isErrorPage="true" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Error Page</title>
    </head>
    <body>
        Something bad happened:
        <h2><%= exception.toString() %></h2>
    </body>
</html>
