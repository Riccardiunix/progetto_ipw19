<!DOCTYPE html>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page errorPage="../error.jsp" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<c:if test="${paziente != null}">
    <html lang="it">
      <head>
        <link rel="shortcut icon" type="image/png" href="../img/ico.ico"/>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

        <title>IPW Home ${paziente.getNome()} ${paziente.getCognome()}</title>

        <!-- css -->
        <link href="../font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <link href="../plugins/cubeportfolio/css/cubeportfolio.min.css" rel="stylesheet" type="text/css">
        <link href="../css/bootstrap.min.css" rel="stylesheet" type="text/css">
        <link href="../css/nivo-lightbox.css" rel="stylesheet" />
        <link href="../css/nivo-lightbox-theme/default/default.css" rel="stylesheet" type="text/css" />
        <link href="../css/owl.carousel.css" rel="stylesheet" media="screen" />
        <link href="../css/owl.theme.css" rel="stylesheet" media="screen" />
        <link href="../css/animate.css" rel="stylesheet" />
        <link href="../css/style.css" rel="stylesheet">
        <link href="../css/datatables.min.css" rel="stylesheet">

        <!-- boxed bg -->
        <link id="bodybg" href="../bodybg/bg1.css" rel="stylesheet" type="text/css" />
        <!-- template skin -->
        <link id="t-colors" href="../color/default.css" rel="stylesheet">
        <style>
            .back-to-top {
                cursor: pointer;
                position: fixed;
                bottom: 10%;
                right: 5%;
                display: none;
            }
            /* unvisited link */
            a:link {
              color: gwhite;
            }

            /* visited link */
            a:visited {
              color: white;
            }
            
            a:hover {
                color: white;
            }
            
            .anchor{
                position: absolute;
                margin-top: -10%;
            }
        </style>
    </head>
    <body id="page-top" data-spy="scroll" data-target=".navbar-custom">


      <div id="wrapper">

        <nav class="navbar navbar-custom navbar-fixed-top" role="navigation">
          <div class="top-area">
            <div class="container">
              <div class="row">
                <div class="col-sm-6 col-md-6">
                    <p class="bold text-left">
                        <c:if test="${paziente.getSesso() == 'M'}">Benvenuto </c:if>
                        <c:if test="${paziente.getSesso() == 'F'}">Benvenuta </c:if>
                        ${paziente.getNome()} ${paziente.getCognome()}
                    </p>
                </div>
              </div>
            </div>
          </div>
          <div class="container navigation">
            <div class="navbar-header page-scroll">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-main-collapse">
                    <i class="fa fa-bars"></i>
                </button>
                <a class="navbar-brand" href="../index.jsp">
                    <img src="../img/logo.png" alt="" width="150" height="40" />
                </a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse navbar-right navbar-main-collapse">
              <ul class="nav navbar-nav">
                <li><a href="#visite">Visite</a></li>
                <li><a href="#visite_spec">Visite Spec</a></li>
                <li><a href="#esami">Esami</a></li>
                <li><a href="#ricette">Ricette</a></li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Account<b class="caret"></b></a>
                    <ul class="dropdown-menu">
                      <li><a href="#" data-toggle="modal" data-target="#modify_data">Modifica dati</a></li>
                      <li> <a href="${pageContext.request.contextPath}/Logout">Logout</a> </li>
                    </ul>
                </li>
              </ul>
            </div>
            <!-- /.navbar-collapse -->
          </div>
          <!-- /.container -->
        </nav>
          <!-- Section: profile -->
          <section id="profilo" class="home-section paddingtop-110 paddingbot-20">
                <div class="container">
                    <div class="row">                                                
                        <div class="col-sm-2 col-md-2"></div>
                        <div class="col-sm-3 col-md-3">
                            <img src='../img_paz/${paziente.getCf()}/avataaars.png' class="img-responsive center-block"/>
                        </div>
                        <div class="col-sm-5 col-md-5">
                            <div class="panel panel-info">
                                <div class="panel-heading" style="background-color:#3fbbc0;">
                                    <center><h3 class="panel-title" style="color:white;font-size:150%;">Informazioni personali</h3></center>
                                </div>  
                                <div class="panel-body" style="overflow-x: auto;">
                                    <table style="width: 100%">
                                        <tr>
                                            <th>Codice Fiscale: </th>
                                            <th>${paziente.getCf()}</th>
                                        </tr>
                                        <tr>
                                            <th>Nome: </th>
                                            <th>${paziente.getNome()}</th>
                                        </tr>
                                        <tr>
                                            <th>Cognome: </th>
                                            <th>${paziente.getCognome()}</th>
                                        </tr>
                                        <tr>
                                            <th>Sesso: </th>
                                            <th>${paziente.getSesso()}</th>
                                        </tr>
                                        <tr>
                                            <th>Data di Nascita: </th>
                                            <th>${paziente.getData_nascita()}</th>
                                        </tr>
                                        <tr>
                                            <th>Luogo di Nascita: </th>
                                            <th>${paziente.getLuogo_nascita()}</th>
                                        </tr>
                                        <tr>
                                            <th>Codice Provinciale: </th>
                                            <th>${paziente.getCod_prov()}</th>
                                        </tr>
                                        <tr>
                                            <th>Email: </th>
                                            <th>${paziente.getEmail()}</th>
                                        </tr>
                                        <tr>
                                            <th>Medico di Base: </th>
                                            <th>${paziente.getNome_med()} ${paziente.getCognome_med()}</th>
                                        </tr>                            

                                    </table>     
                                </div>
                            </div>
                        </div>
                    </div>
               </div>
               </section>
                
                <!-- Modal cambio password e mail-->
                <div class="modal fade" id="modify_data" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                  <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content">
                      <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLongTitle">Modifica dati</h5>
                      </div>
                      <div class="modal-body">
                          <ul class="nav nav-tabs">
                            <li class="active"><a data-toggle="tab" href="#medico">Medico di Base</a></li>
                            <li><a data-toggle="tab" href="#password">Password</a></li>
                            <li><a data-toggle="tab" href="#immagine">Immagine</a></li>
                          </ul>

                          <div class="tab-content">
                            <div id="medico" class="tab-pane fade in active">
                              <form action="../UpdateMedicoBase"  method="POST" >
                                <div class="form-group margintop-20">
                                  <label for="exampleFormControlInput1">Cambio medico di base</label>
                                  <select name="change_medico">
                                    <option value="-1">${paziente.getNome_med()} ${paziente.getCognome_med()}</option>
                                    <c:forEach var="c" items="${paziente.getMediciDisp()}">
                                        <option value="${c[0]}">${c[1]} ${c[2]}</option>
                                    </c:forEach>
                                  </select>
                                  <input class="form-control" type="hidden" name="cf_paz" value="${paziente.getCf()}">                                  
                                </div>
                                <div class="modal-footer">
                                     <button type="reset" class="btn btn-info" data-dismiss="modal">Annulla</button>
                                     <button type="submit" class="btn btn-primary">Inserisci</button>
                                </div>
                              </form>
                            </div>
                            <div id="password" class="tab-pane fade">
                              <form action="../ChangePassword"  method="POST" >
                                <div class="form-group margintop-20">
                                    <label for="exampleFormControlInput1">Password vecchia</label>
                                    <input class="form-control" type="password" name="old_pass" value="">                                    
                                </div>
                                <div class="form-group">
                                    <label for="exampleFormControlInput1">Password nuova</label>
                                    <input class="form-control" type="password" name="new_pass" value="">                                  
                                </div>  
                                <div class="form-group">
                                    <label for="exampleFormControlInput1">Conferma password nuova</label>
                                    <input class="form-control" type="password" name="new_pass_conf" value="">                                  
                                </div>                               
                                <div class="modal-footer">
                                     <button type="reset" class="btn btn-info" data-dismiss="modal">Annulla</button>
                                     <button type="submit" class="btn btn-primary">Inserisci</button>
                                </div>
                              </form>
                            </div>
                            <div id="immagine" class="tab-pane fade">
                              <form action="../InsertImmagine" method="POST" enctype="multipart/form-data">
                                <div class="form-group">
                                  <label for="exampleFormControlInput1">File</label>
                                  <input type="file" name="file" size="50" class="form-control-file" id="exampleFormControlFile1"  accept="image/png">
                                </div> 
                                <div class="modal-footer">
                                   <button type="reset" class="btn btn-info" data-dismiss="modal">Annulla</button>
                                   <button type="submit" class="btn btn-primary">Inserisci</button>
                                </div>
                              </form>
                            </div>
                          </div> 
                        </div>
                        </form>
                      </div>
                    </div>
                  </div>
                <!-- Modal cambio password e mail-->
            <!-- Section: profile -->
            
            <!-- Section: visite -->
            <div class="anchor" id="visite"></div>
            <section class="home-section paddingtop-30 paddingbot-30">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-2 col-md-2"></div>
                        <div class="col-sm-8 col-md-8">
                            <div class="panel panel-info">
                                <div class="panel-heading" style="background-color:#3fbbc0;">
                                    <center><h3 class="panel-title" style="color:white;font-size:200%;">Visite</h3></center>
                                </div>  
                                <div class="panel-body">
                                    <center><table class="table table-hover table-bordered tabledata" style="width: 99%" id ="tablePagVisite" >
                                        <thead>
                                            <tr>
                                                <th scope="col">Data</th>
                                                <th scope="col">Medico</th>
                                                <th scope="col">Esito</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <c:forEach var="v" items="${paziente.getVisite()}">
                                            <tr>
                                                <td>${v.getData_visita()}</td>
                                                <td>${v.getNomeMed()} ${v.getCognomeMed()}</td>
                                                <td>${v.getRisultato()}</td>
                                            </tr>
                                            </c:forEach>
                                        </tbody>
                                        </table></center>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!-- Section: visite -->
            <!-- Section: visite spec -->
            <div class="anchor" id="visite_spec"></div>
            <section class="home-section paddingtop-30 paddingbot-30">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-2 col-md-2"></div>
                        <div class="col-sm-8 col-md-8">
                            <div class="panel panel-info">
                                <div class="panel-heading" style="background-color:#3fbbc0;">
                                    <center><h3 class="panel-title" style="color:white;font-size:200%;">Visite Specialistiche</h3></center>
                                </div>  
                                <div class="panel-body">
                                    <center><table class="table table-hover table-bordered tabledata" style="width: 99%" id="tablePagVisiteSpec">
                                        <thead>
                                            <tr>
                                                <th scope="col">Data</th>
                                                <th scope="col">Medico</th>
                                                <th scope="col">Medico Esame</th>
                                                <th scope="col">Tipo Visita</th>
                                                <th scope="col">Esito Visita</th>
                                                <th scope="col">Ticket</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <c:if test="${paziente.getVisite_spec().size() > 0}">
                                                <c:forEach var="i" begin="0" end="${paziente.getVisite_spec().size() - 1}">
                                                <tr>
                                                    <td>${paziente.getVisite_spec().get(i).getData_visita()}</td>
                                                    <td>${paziente.getVisite_spec().get(i).getNomeMed()} ${paziente.getVisite_spec().get(i).getCognomeMed()}</td>
                                                    <td>${paziente.getVisite_spec().get(i).getNomeMed_visita()} ${paziente.getVisite_spec().get(i).getCognomeMed_visita()}</td>
                                                    <td>${paziente.getVisite_spec().get(i).getNome_visita()}</td>
                                                    <td>${paziente.getVisite_spec().get(i).getRisultato()}</td>
                                                    <c:if test="${paziente.getVisite_spec().get(i).getTicket() == 0}">
                                                        <td>                                                    
                                                            <form action="../UpdateTicket" method="POST">
                                                                <span class="glyphicon glyphicon-remove" style="color:red" data-toggle="tooltip" data-placement="right" title="Non pagato"></span>
                                                                <input class="form-control" type="hidden" name="tipo" value="0">
                                                                <input class="form-control" type="hidden" name="id_visita" value="${i}">
                                                                <button type="submit" class="btn btn-primary" data-toggle="tooltip" data-placement="top" title="Paga esame">50 <span class="glyphicon glyphicon-euro"></span></button>
                                                            </form>
                                                        </td>
                                                    </c:if>
                                                    <c:if test="${paziente.getVisite_spec().get(i).getTicket() == 1}"><td><span class="glyphicon glyphicon-ok" style="color:green" data-toggle="tooltip" data-placement="right" title="Pagato"></span></td></c:if>
                                                </tr>
                                                </c:forEach>
                                            </c:if>
                                        </tbody>
                                        </table></center>
                                    <div class="row">
                                        <div class="col-sm-2 col-md-2">
                                            <form method="POST" target="_blank" action="../ReportTicket">
                                                <input class="form-control" type="hidden" name="paziente" value="${paziente.getNome()} ${paziente.getCognome()}">
                                                <input class="form-control" type="hidden" name="cf_paz" value="${paziente.getCf()}">
                                                <button type="submit" class="btn btn-info btn-sm">Report ticket pagati</button>
                                            </form>
                                        </div>
                                        <div class="col-sm-10 col-md-10">                                            
                                        </div>
                                    </div>
                                </div>                                
                            </div>                            
                        </div>
                    </div>                    
                </div>
            </section>
      <!-- Section: visite spec -->
      <!-- Section: esami -->
            <div class="anchor" id="esami"></div>
            <section class="home-section paddingtop-30 paddingbot-30">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-2 col-md-2"></div>
                        <div class="col-sm-8 col-md-8">
                            <div class="panel panel-info">
                                <div class="panel-heading" style="background-color:#3fbbc0;">
                                    <center><h3 class="panel-title" style="color:white;font-size:200%;">Esami</h3></center>
                                </div>  
                                <div class="panel-body">
                                    <center> <table class="table table-hover table-bordered tabledata" style="width: 99%" id="tablePagEsami">
                                        <thead>
                                            <tr>
                                                <th scope="col">Data</th>
                                                <th scope="col">Medico</th>
                                                <th scope="col">Tipo Esame</th>
                                                <th scope="col">Esito Esame</th>
                                                <th scope="col">Ticket</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <c:if test="${paziente.getEsami().size() > 0}">
                                                <c:forEach var="i" begin="0" end="${paziente.getEsami().size() - 1}">
                                                <tr>
                                                    <td>${paziente.getEsami().get(i).getData_esame()}</td>
                                                    <td>${paziente.getEsami().get(i).getNomeMed()} ${e.getCognomeMed()}</td>
                                                    <td>${paziente.getEsami().get(i).getNome_esame()}</td>
                                                    <td>${paziente.getEsami().get(i).getRisultato()}</td>
                                                    <c:if test="${paziente.getEsami().get(i).getTicket() == 0}">
                                                        <td>                                                    
                                                            <form action="../UpdateTicket" method="POST">
                                                                <span class="glyphicon glyphicon-remove" style="color:red" data-toggle="tooltip" data-placement="right" title="Non pagato"></span>
                                                                <input class="form-control" type="hidden" name="tipo" value="1">
                                                                <input class="form-control" type="hidden" name="id_esame" value="${i}">
                                                                <button type="submit" class="btn btn-primary" data-toggle="tooltip" data-placement="top" title="Paga visita">11 <span class="glyphicon glyphicon-euro"></span></button>
                                                            </form>
                                                        </td>
                                                    </c:if>
                                                    <c:if test="${paziente.getEsami().get(i).getTicket() == 1}"><td><span class="glyphicon glyphicon-ok" style="color:green" data-toggle="tooltip" data-placement="right" title="Pagato"></span></td></c:if>
                                                </tr>
                                                </c:forEach>
                                            </c:if>
                                        </tbody>
                                        </table></center>
                                    <div class="row">
                                        <div class="col-sm-2 col-md-2">
                                            <form method="POST" target="_blank" action="../ReportTicket">
                                                <input class="form-control" type="hidden" name="paziente" value="${paziente.getNome()} ${paziente.getCognome()}">
                                                <input class="form-control" type="hidden" name="cf_paz" value="${paziente.getCf()}">
                                                <button type="submit" class="btn btn-info btn-sm">Report ticket pagati</button>
                                            </form>
                                        </div>
                                        <div class="col-sm-10 col-md-10"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>                    
                </div>
            </section>
      <!-- Section: esami -->
      <!-- Section: ricette -->
            <div class="anchor" id="ricette"></div>
            <section class="home-section paddingtop-30 paddingbot-30">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-2 col-md-2"></div>
                        <div class="col-sm-8 col-md-8">
                            <div class="panel panel-info">
                                <div class="panel-heading" style="background-color:#3fbbc0;">
                                    <center><h3 class="panel-title" style="color:white;font-size:200%;">Ricette</h3></center>
                                </div>  
                                <div class="panel-body">
                                    <center> <table class="table table-hover table-bordered tabledata" style="width: 99%" id="tablePagMedicine">
                                        <thead>
                                            <tr>
                                                <th scope="col">Data</th>
                                                <th scope="col">Medico</th>
                                                <th scope="col">Farmaco</th>
                                                <th scope="col">Pdf</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <c:forEach var="r" items="${paziente.getRicette()}">
                                            <tr>
                                                <td>${r.getData_presc()}</td>
                                                <td>${r.getNomeMed()} ${r.getCognomeMed()}</td>
                                                <td>${r.getFarmaco()}</td>
                                                <td>
                                                    <center>
                                                        <a href="../pdf_ricette/${r.getId()}.pdf" target="_blank" data-toggle="tooltip" data-placement="top" title="Visualizza pdf ricetta"><button type="submit" class="btn btn-info btn-sm"><span class="glyphicon glyphicon-eye-open"></span></button></a>
                                                        <a href="../pdf_ricette/${r.getId()}.pdf" id="download_ricetta" download="ricetta_${r.getId()}" data-toggle="tooltip" data-placement="top" title="Scarica pdf ricetta"><button type="submit" class="btn btn-info btn-sm"><span class="glyphicon glyphicon-download-alt"></span></button></a>
                                                    </center>
                                                </td>
                                            </tr>
                                            </c:forEach>
                                        </tbody>
                                        </table></center>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <a id="back-to-top" href="#" class="btn btn-info btn-lg back-to-top" role="button" title="Ritorna all'inizio della pagina" data-toggle="tooltip" data-placement="left"><span class="glyphicon glyphicon-chevron-up"></span></a>
            </section>
      </div>
      <!-- Section: ricette -->
     <!-- Core JavaScript Files -->
     <script src="../js/jquery.min.js"></script>
     <script src="../js/bootstrap.min.js"></script>
     <script src="../js/jquery.easing.min.js"></script>
     <script src="../js/wow.min.js"></script>
     <script src="../js/jquery.scrollTo.js"></script>
     <script src="../js/jquery.appear.js"></script>
     <script src="../js/stellar.js"></script>
     <script src="../plugins/cubeportfolio/js/jquery.cubeportfolio.min.js"></script>
     <script src="../js/owl.carousel.min.js"></script>
     <script src="../js/nivo-lightbox.min.js"></script>
     <script src="../js/custom.js"></script>
     <script src="../js/datatables.js"></script>
     <script>
        $(document).ready(function () {
            $('.tabledata').DataTable( {
               "scrollX": true,
                "language": {
                    "sEmptyTable":     "Nessun dato presente nella tabella",
                    "sInfo":           "Pagina _PAGE_ di _PAGES_",
                    "sInfoEmpty":      "Vista da 0 a 0 di 0 elementi",
                    "sInfoFiltered":   "(filtrati da _MAX_ elementi totali)",
                    "sInfoPostFix":    "",
                    "sInfoThousands":  ".",
                    "sLengthMenu":     "Visualizza _MENU_ elementi",
                    "sLoadingRecords": "Caricamento...",
                    "sProcessing":     "Elaborazione...",
                    "sSearch":         "Cerca:",
                    "sZeroRecords":    "La ricerca non ha portato alcun risultato.",
                    "oPaginate": {
                        "sPrevious":   "Precedente",
                        "sNext":       "Successivo"
                    }
                },
                "order": [0, 'desc']
            } );
            $('.dataTables_length').addClass('bs-select');
            $(window).scroll(function () {
                if ($(this).scrollTop() > 50) {
                    $('#back-to-top').fadeIn();
                } else {
                    $('#back-to-top').fadeOut();
                }
            });
               // scroll body to 0px on click
            $('#back-to-top').click(function () {
                $('#back-to-top').tooltip('hide');
                $('body,html').animate({
                    scrollTop: 0
                }, 800);
                return false;
            });

            $('#back-to-top').tooltip('show');
        });
    </script>
    <c:if test="${param.err == 1}">
      <script> window.alert("Errore della password");</script>
    </c:if>
    <c:if test="${param.err == 4}">
      <script> window.alert("Errore nel caricamento dell'immagine");</script>
    </c:if>
    <c:if test="${param.err == 9}">
      <script> window.alert("Errore nel cambio del medico base");</script>
    </c:if>
    <c:if test="${param.err == 10}">
      <script> window.alert("Errore nel pagamento del ticket");</script>
    </c:if>
    </body>
    </html>
</c:if>

<c:if test="${ssp != null}">
    <c:redirect url="homeSSP.jsp"/>
</c:if>

<c:if test="${medico != null}">
    <c:redirect url="homeMedico.jsp"/>
</c:if>

<c:if test="${paziente == null and medico == null and ssp == null}">
    <c:redirect url="../index.jsp"/>
</c:if>
