<!DOCTYPE html>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page errorPage="../error.jsp" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<c:if test="${ssp  != null}">
    <html lang="it">
       <head>
            <link rel="shortcut icon" type="image/png" href="../img/ico.ico"/>
            <meta charset="utf-8">
            <meta name="viewport" content="width=device-width, initial-scale=1.0">
            <meta name="description" content="">
            <meta name="author" content="">
            <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

            <title>IPW Home Servizio Sanitario di ${ssp.getCod_prov()}</title>

            <!-- css -->
            <link href="../font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
            <link href="../plugins/cubeportfolio/css/cubeportfolio.min.css" rel="stylesheet" type="text/css">
            <link href="../css/bootstrap.min.css" rel="stylesheet" type="text/css">
            <link href="../css/nivo-lightbox.css" rel="stylesheet" />
            <link href="../css/nivo-lightbox-theme/default/default.css" rel="stylesheet" type="text/css" />
            <link href="../css/owl.carousel.css" rel="stylesheet" media="screen" />
            <link href="../css/owl.theme.css" rel="stylesheet" media="screen" />
            <link href="../css/animate.css" rel="stylesheet" />
            <link href="../css/style.css" rel="stylesheet">
            <link href="../css/datatables.min.css" rel="stylesheet">

            <!-- boxed bg -->
            <link id="bodybg" href="../bodybg/bg1.css" rel="stylesheet" type="text/css" />
            <!-- template skin -->
            <link id="t-colors" href="../color/default.css" rel="stylesheet">
            <style>
                .back-to-top {
                    cursor: pointer;
                    position: fixed;
                    bottom: 10%;
                    right: 5%;
                    display:none;
                }
                
                .anchor{
                    position: absolute;
                    margin-top: -10%;
                }
                
                .dataTables_scroll{
                    overflow:auto;
                }
            </style>
        </head>
    <body id="page-top" data-spy="scroll" data-target=".navbar-custom">
        <div id="wrapper">
        <nav class="navbar navbar-custom navbar-fixed-top" role="navigation">
            <div class="top-area">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-6 col-md-6">
                            <p class="bold text-left">
                                Servizio Sanitario di ${ssp.getCod_prov()}
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="container navigation">
                <div class="navbar-header page-scroll">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-main-collapse">
                        <i class="fa fa-bars"></i>
                    </button>
                    <a class="navbar-brand" href="../index.jsp"> <img src="../img/logo.png" alt="" width="150" height="40" /></a>
                </div>

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse navbar-right navbar-main-collapse">
                    <ul class="nav navbar-nav">
                        <c:if test="${ssp.getEsami_da_fare().size() > 0}">
                            <li><a href="#" data-toggle="modal" data-target="#upd_esame">Esegui Esame</a></li>
                        </c:if> 
                        <c:if test="${ssp.getEsami_da_fare().size() == 0}">
                            <li><a href="#" onclick="window.alert('Nessun esame da eseguire')">Esegui Esame</a></li>
                        </c:if>
                        <li><a href="#esami">Esami</a></li>
                        <li><a href="#report_ricette">Report ricette</a></li>
                        <li><a href="${pageContext.request.contextPath}/Logout">Logout</a></li>
                    </ul>
                </div>
                <!-- /.navbar-collapse -->
            </div>
          <!-- /.container -->
        </nav>
        
        <!-- Modal upd esame-->
        <c:if test="${ssp.getEsami_da_fare().size() > 0}">
            <div class="modal fade" id="upd_esame" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
             <div class="modal-dialog modal-dialog-centered" role="document">
               <div class="modal-content">
                 <div class="modal-header">
                   <h5 class="modal-title" id="exampleModalLongTitle">Effettua esame</h5>
                 </div>
                 <div class="modal-body">
                   <form action="../UpdateEsame"  method="POST" >
                       
                       <div class="form-group">
                            <div class="dataTables_scroll">
                            <table class="table table-hover table-bordered tabledatamodal" style="width: 99%" id="tableEsamiDafare">
                             <thead>
                                 <tr>
                                     <th scope="col">Data</th>
                                     <th scope="col">Paziente</th>
                                     <th scope="col">Medico</th>
                                     <th scope="col">Tipo Esame</th>
                                     <th scope="col">Effettua</th>
                                 </tr>
                             </thead>
                             <tbody>
                                 <c:if test="${ssp.getEsami().size() > 0}">
                                     <c:forEach var="i" begin="0" end="${ssp.getEsami_da_fare().size() - 1}">
                                     <tr>
                                         <td>${ssp.getEsami_da_fare().get(i).getData_esame()}</td>
                                         <td>${ssp.getEsami_da_fare().get(i).getNomePaz()} ${ssp.getEsami_da_fare().get(i).getCognomePaz()}</td>
                                         <td>${ssp.getEsami_da_fare().get(i).getNomeMed()} ${ssp.getEsami_da_fare().get(i).getCognomeMed()}</td>
                                         <td>${ssp.getEsami_da_fare().get(i).getNome_esame()}</td>
                                         <td><input type="radio" name="id_esame" value="${i}"><br></td>

                                     </tr>
                                     </c:forEach>
                                 </c:if>
                             </tbody>
                             </table>     
                            </div>
                       </div>                           
                           <div class="form-group">
                             <label for="exampleFormControlTextarea1">Risultato</label>
                             <textarea class="form-control" id="exampleFormControlTextarea1" name="ris_esame" rows="3"></textarea>
                           </div>
                       <div class="modal-footer">
                           <button type="reset" class="btn btn-info" data-dismiss="modal">Annulla</button>
                           <button type="submit" class="btn btn-primary">Inserisci</button>
                       </div>
                    </form>
                 </div>
               </div>
             </div>
           </div>
        </c:if> 
        
          <!-- Section: profile -->
        <section id="profilo" class="home-section paddingtop-110 paddingbot-20">
            <div class="container">
                <div class="row">
                    <div class="col-sm-3 col-md-3"></div>                    
                    <div class="col-sm-6 col-md-6">
                        <div class="panel panel-info">
                            <div class="panel-heading" style="background-color:#3fbbc0;">
                                <center><h3 class="panel-title" style="color:white;font-size:150%;">Informazioni SSP</h3></center>
                            </div>  
                            <div class="panel-body" style="overflow-x: auto;">
                                <table style="width: 100%">
                                    <tr>
                                        <th>Codice Provinciale: </th>
                                        <th>${ssp.getCod_prov()}</th>
                                    </tr>
                                    <tr id="visite">
                                        <th>Email: </th>
                                        <th>${ssp.getEmail()}</th>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-3 col-md-3">
                        <!-- <img src='img_paz/${paziente.getCf()}/avataaars.png' class="img-responsive center-block"/> -->
                    </div>
                </div>
            </div>
        </section>
        
        <!-- Section: esami -->
        <div class="anchor" id="esami"></div>
        <section class="home-section paddingtop-30 paddingbot-30">
            <div class="container">
                <div class="row">
                    <div class="col-sm-2 col-md-2"></div>
                    <div class="col-sm-8 col-md-8">
                        <div class="panel panel-info">
                            <div class="panel-heading" style="background-color:#3fbbc0;">
                                <center><h3 class="panel-title" style="color:white;font-size:200%;">Esami</h3></center>
                            </div>  
                            <div class="panel-body">
                                <center><table class="table table-hover table-bordered tabledata" style="width: 99%" id="tablePagEsami">
                                    <thead>
                                        <tr>
                                            <th scope="col">Data</th>
                                            <th scope="col">Paziente</th>
                                            <th scope="col">Medico</th>
                                            <th scope="col">Tipo Esame</th>
                                            <th scope="col">Esito Esame</th>
                                            <th scope="col">Ticket</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <c:if test="${ssp.getEsami().size() > 0}">
                                            <c:forEach var="i" begin="0" end="${ssp.getEsami().size() - 1}">
                                            <tr>
                                                <td>${ssp.getEsami().get(i).getData_esame()}</td>
                                                <td>${ssp.getEsami().get(i).getNomePaz()} ${ssp.getEsami().get(i).getCognomePaz()}</td>
                                                <td>${ssp.getEsami().get(i).getNomeMed()} ${ssp.getEsami().get(i).getCognomeMed()}</td>
                                                <td>${ssp.getEsami().get(i).getNome_esame()}</td>
                                                <td>${ssp.getEsami().get(i).getRisultato()}</td>
                                                <c:if test="${ssp.getEsami().get(i).getTicket() == 0}"><td><span class="glyphicon glyphicon-remove" style="color:red" data-toggle="tooltip" data-placement="right" title="Non pagato"></span></td></c:if>
                                                <c:if test="${ssp.getEsami().get(i).getTicket() == 1}"><td><span class="glyphicon glyphicon-ok" style="color:green" data-toggle="tooltip" data-placement="right" title="Pagato"></span></td></c:if>
                                            </tr>
                                            </c:forEach>
                                        </c:if>
                                    </tbody>
                                    </table></center>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-2 col-md-2"></div>
                </div>
            </div>
        </section>
        
        <!-- Section: report ricette -->
            <div class="anchor" id="report_ricette"></div>
            <section class="home-section paddingtop-30 paddingbot-30">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-2 col-md-2"></div>
                        <div class="col-sm-8 col-md-8">
                            <div class="panel panel-info">
                                <div class="panel-heading" style="background-color:#3fbbc0;">
                                    <center><h3 class="panel-title" style="color:white;font-size:200%;">Report Ricette</h3></center>
                                </div>  
                                <div class="panel-body">
                                    <center><table class="table table-hover table-bordered tabledata" style="width: 99%" id="tablePagReportRic">
                                        <thead>
                                            <tr>
                                                <th scope="col">Data</th>
                                                <th scope="col">Xls</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <c:forEach var="r" items="${ssp.getDate_ricette()}">
                                            <tr>
                                                <td>${r}</td>
                                                <td>
                                                    <center>
                                                        <a href="../xls_ricette/${ssp.getCod_prov()}/${r}.xls" id="download_ricetta" download="ricetta_${r}.xls" data-toggle="tooltip" data-placement="top" title="Scarica report xls ricette"><button type="submit" class="btn btn-info btn-sm"><span class="glyphicon glyphicon-download-alt"></span></button></a>
                                                    </center>
                                                </td>
                                            </tr>
                                            </c:forEach>
                                        </tbody>
                                        </table></center>
                                </div>
                            </div>
                        </div>
                    </div>
                </div> 
                <a id="back-to-top" href="#" class="btn btn-info btn-lg back-to-top" role="button" title="Ritorna all'inizio della pagina" data-toggle="tooltip" data-placement="left">
                    <span class="glyphicon glyphicon-chevron-up"></span>
                </a>
            </section>
        
            <!-- Core JavaScript Files -->
            <script src="../js/jquery.min.js"></script>
            <script src="../js/bootstrap.min.js"></script>
            <script src="../js/jquery.easing.min.js"></script>
            <script src="../js/wow.min.js"></script>
            <script src="../js/jquery.scrollTo.js"></script>
            <script src="../js/jquery.appear.js"></script>
            <script src="../js/stellar.js"></script>
            <script src="../plugins/cubeportfolio/js/jquery.cubeportfolio.min.js"></script>
            <script src="../js/owl.carousel.min.js"></script>
            <script src="../js/nivo-lightbox.min.js"></script>
            <script src="../js/custom.js"></script>
            <script src="../js/datatables.js"></script>
            <script>
                $(document).ready(function () {
                    $('.tabledata').DataTable( {
                        "scrollX": true,
                        "language": {
                            "sEmptyTable":     "Nessun dato presente nella tabella",
                            "sInfo":           "Pagina _PAGE_ di _PAGES_",
                            "sInfoEmpty":      "Vista da 0 a 0 di 0 elementi",
                            "sInfoFiltered":   "(filtrati da _MAX_ elementi totali)",
                            "sInfoPostFix":    "",
                            "sInfoThousands":  ".",
                            "sLengthMenu":     "Visualizza _MENU_ elementi",
                            "sLoadingRecords": "Caricamento...",
                            "sProcessing":     "Elaborazione...",
                            "sSearch":         "Cerca:",
                            "sZeroRecords":    "La ricerca non ha portato alcun risultato.",
                            "oPaginate": {
                                "sPrevious":   "Precedente",
                                "sNext":       "Successivo"
                            }
                        }                                                
                    } );
                    $('.tabledatamodal').DataTable( {
                        "language": {
                            "sEmptyTable":     "Nessun dato presente nella tabella",
                            "sInfo":           "Pagina _PAGE_ di _PAGES_",
                            "sInfoEmpty":      "Vista da 0 a 0 di 0 elementi",
                            "sInfoFiltered":   "(filtrati da _MAX_ elementi totali)",
                            "sInfoPostFix":    "",
                            "sInfoThousands":  ".",
                            "sLengthMenu":     "Visualizza _MENU_ elementi",
                            "sLoadingRecords": "Caricamento...",
                            "sProcessing":     "Elaborazione...",
                            "sSearch":         "Cerca:",
                            "sZeroRecords":    "La ricerca non ha portato alcun risultato.",
                            "oPaginate": {
                                "sPrevious":   "Precedente",
                                "sNext":       "Successivo"
                            }
                        }                                                
                    } );
                    jQuery('.tabledatamodal').wrap('<div class="dataTables_scroll" />');
                    $('.dataTables_length').addClass('bs-select');
                    $(window).scroll(function () {
                        if ($(this).scrollTop() > 50) {
                            $('#back-to-top').fadeIn();
                        } else {
                            $('#back-to-top').fadeOut();
                        }
                    });
                       // scroll body to 0px on click
                    $('#back-to-top').click(function () {
                        $('#back-to-top').tooltip('hide');
                        $('body,html').animate({
                            scrollTop: 0
                        }, 800);
                        return false;
                    });
                    $('#back-to-top').tooltip('show');
                });
           </script>
           <c:if test="${param.err == 8}">
            <script> window.alert("Update dell'esame non riuscito");</script>
          </c:if>
        </body>
    </html>
</c:if>
<c:if test="${paziente != null}">
    <c:redirect url="homePaziente.jsp"/>
</c:if>
 
<c:if test="${medico != null}">
    <c:redirect url="homeMedico.jsp"/>
</c:if>

<c:if test="${paziente == null and medico == null and ssp == null}">
    <c:redirect url="../index.jsp"/>
</c:if>