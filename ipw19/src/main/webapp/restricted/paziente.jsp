<!DOCTYPE html>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page errorPage="../error.jsp" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<c:if test="${medico != null}">
    <c:if test="${id < 0 && id > medico.getPazienti().size()}">
        <c:redirect url="restricted/homeMedico.jsp"/>
    </c:if>
   
    <html lang="it">
      <head>
        <link rel="shortcut icon" type="image/png" href="../img/ico.ico"/>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

        <title>IPW Home ${medico.getPazienti().get(param.id).getNome()} ${medico.getPazienti().get(param.id).getCognome()}</title>
        
        <!-- css -->
        <link href="../font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <link href="../plugins/cubeportfolio/css/cubeportfolio.min.css" rel="stylesheet" type="text/css">
        <link href="../css/bootstrap.min.css" rel="stylesheet" type="text/css">
        <link href="../css/nivo-lightbox.css" rel="stylesheet" />
        <link href="../css/nivo-lightbox-theme/default/default.css" rel="stylesheet" type="text/css" />
        <link href="../css/owl.carousel.css" rel="stylesheet" media="screen" />
        <link href="../css/owl.theme.css" rel="stylesheet" media="screen" />
        <link href="../css/animate.css" rel="stylesheet" />
        <link href="../css/style.css" rel="stylesheet">
        <link href="../css/datatables.min.css" rel="stylesheet">

        <!-- boxed bg -->
        <link id="bodybg" href="../bodybg/bg1.css" rel="stylesheet" type="text/css" />
        <!-- template skin -->
        <link id="t-colors" href="../color/default.css" rel="stylesheet">
        <style>
            .back-to-top {
                cursor: pointer;
                position: fixed;
                bottom: 10%;
                right: 5%;
                display:none;
            }
            
            .anchor{
                position: absolute;
                margin-top: -10%;
            }
        </style>
     </head>
     <body id="page-top" data-spy="scroll" data-target=".navbar-custom">

      <div id="wrapper">
        <nav class="navbar navbar-custom navbar-fixed-top" role="navigation">
          <div class="top-area">
            <div class="container">
              <div class="row">
                <div class="col-sm-6 col-md-6">
                    <p class="bold text-left"> Paziente: ${medico.getPazienti().get(param.id).getNome()} ${medico.getPazienti().get(param.id).getCognome()}</p>
                </div>
              </div>
            </div>
          </div>
          <div class="container navigation">
            <div class="navbar-header page-scroll">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-main-collapse">
                    <i class="fa fa-bars"></i>
                </button>
                <a class="navbar-brand" href="../index.jsp">
                    <img src="../img/logo.png" alt="" width="150" height="40" />
                </a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse navbar-right navbar-main-collapse">
              <ul class="nav navbar-nav">
                <li><a href="homeMedico.jsp">Torna indietro</a></li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Azioni<b class="caret"></b></a>
                    <ul class="dropdown-menu">                      
                      <c:if test="${medico.getTipo_med() == 1}">
                            <c:if test="${medico.getVisite_da_fare().get(param.id).get(0).size() > 0}">
                                <li><a href="#" data-toggle="modal" data-target="#eff_vis_spec">Effettua visita specialistica</a></li>
                            </c:if> 
                            <c:if test="${medico.getVisite_da_fare().get(param.id).get(0).size() == 0}">
                                <li><a href="#" onclick="window.alert('Nessuna visita da eseguire')">Effettua visita specialistica</a></li>
                            </c:if>
                      </c:if>
                      <c:if test="${medico.getTipo_med() == 0}">
                            <li><a href="#" data-toggle="modal" data-target="#ins_visita">Effettua visita</a></li>
                            <li><a href="#" data-toggle="modal" data-target="#ins_vis_spec">Prescrivi visita specialistica</a></li>
                            <li><a href="#" data-toggle="modal" data-target="#ins_esame">Prescrivi esame</a></li>
                            <li><a href="#" data-toggle="modal" data-target="#ins_ricetta">Prescrivi ricetta</a></li>
                      </c:if>
                    </ul>
                </li>
                <li><a href="#visite">Visite</a></li>
                <li><a href="#visite_spec">Visite Spec</a></li>
                <li><a href="#esami">Esami</a></li>
                <li><a href="#ricette">Ricette</a></li>
                <li><a href="${pageContext.request.contextPath}/Logout">Logout</a> </li>
              </ul>
            </div>
            <!-- /.navbar-collapse -->
          </div>
          <!-- /.container -->
        </nav>                     
         
         <c:if test="${medico.getTipo_med() == 1 and medico.getVisite_da_fare().get(param.id).size() > 0 and medico.getVisite_da_fare().get(param.id).get(0).size() > 0}">
         <!-- Modal visita specialistica-->
        <div class="modal fade" id="eff_vis_spec" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
          <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Effettua visita specialistica</h5>
              </div>
              <div class="modal-body">
                <form action="../UpdateVisitaSpec"  method="POST" >
                    <div class="form-group">
                        <label for="exampleFormControlInput1">Visita: </label>
                            <input class="form-control" type="text" value="${medico.getPazienti().get(param.id).getNome()} ${medico.getPazienti().get(param.id).getCognome()}" readonly>
                            <input class="form-control" type="hidden" name="id" value="${param.id}">
                        <select name="id_visita">
                            <option value="-1"></option>
                            <c:forEach var="i" begin="0" end="${medico.getVisite_da_fare().get(param.id).size() - 1}">
                                <option value="${i}">${medico.getVisite_da_fare().get(param.id).get(i).get(0)} ${medico.getVisite_da_fare().get(param.id).get(i).get(1)}</option>
                            </c:forEach>
                        </select>
                    </div>   
                        <div class="form-group">
                          <label for="exampleFormControlTextarea1">Risultato</label>
                          <textarea class="form-control" id="exampleFormControlTextarea1" name="ris_visita" rows="3"></textarea>
                        </div>
                    <div class="modal-footer">
                        <button type="reset" class="btn btn-info" data-dismiss="modal">Annulla</button>
                        <button type="submit" class="btn btn-primary">Inserisci</button>
                    </div>
                </form>
              </div>
            </div>
          </div>
        </div>           
         </c:if>
               
         <c:if test="${medico.getTipo_med() == 0}">
             <!-- Modal visita-->
            <div class="modal fade" id="ins_visita" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
              <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Effettua Visita</h5>
                  </div>
                  <div class="modal-body">
                      <form action="../InsertVisita"  method="POST" >
                        <div class="form-group">
                          <label for="exampleFormControlInput1">Paziente</label>
                          <input class="form-control" type="text" value="${medico.getPazienti().get(param.id).getNome()} ${medico.getPazienti().get(param.id).getCognome()}" readonly>
                          <input class="form-control" type="hidden" name="id" value="${param.id}">
                        </div>
                        <div class="form-group">
                          <label for="exampleFormControlInput1">Data</label>
                          <input class="form-control"  type="text" value="<%= java.time.LocalDate.now() %>" readonly>        
                        </div>
                        <div class="form-group">
                          <label for="exampleFormControlTextarea1">Riassunto</label>
                          <textarea class="form-control" id="exampleFormControlTextarea1" name="esito_visita" rows="3"></textarea>
                        </div>
                         <div class="modal-footer">
                             <button type="reset" class="btn btn-info" data-dismiss="modal">Annulla</button>
                             <button type="submit" class="btn btn-primary">Inserisci</button>
                        </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
            <!-- Modal visita specialistica-->
            <div class="modal fade" id="ins_vis_spec" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
              <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Prescrivi visita specialistica</h5>
                  </div>
                  <div class="modal-body">
                    <form action="../InsertVisitaSpec"  method="POST" >
                        <div class="form-group">
                          <label for="exampleFormControlInput1">Paziente</label>
                            <input class="form-control" type="text" value="${medico.getPazienti().get(param.id).getNome()} ${medico.getPazienti().get(param.id).getCognome()}" readonly>
                            <input class="form-control" type="hidden" name="id" value="${param.id}">
                        </div>
                        <div class="form-group">
                          <label for="exampleFormControlInput1">Data</label>
                          <input class="form-control" name="data_visita" type="date" value="<%= java.time.LocalDate.now() %>">                      
                        </div>
                        <div class="form-group">
                            <label for="exampleFormControlInput1">Tipo visita</label>
                            <select name="tipo_visita">
                                  <option value="-1"></option>
                              <c:forEach var="t" items="${medico.getTipi_visite()}">
                                  <option value="${t[0]}">${t[1]}</option>
                              </c:forEach>
                            </select>
                        </div>  
                        <div class="modal-footer">
                            <button type="reset" class="btn btn-info" data-dismiss="modal">Annulla</button>
                            <button type="submit" class="btn btn-primary">Inserisci</button>
                        </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
            <!-- Modal esame-->
            <div class="modal fade" id="ins_esame" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
              <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Prescrivi Esame</h5>
                  </div>
                  <div class="modal-body">
                    <form action="../InsertEsame"  method="POST" >
                        <div class="form-group">
                          <label for="exampleFormControlInput1">Paziente</label>
                            <input class="form-control" type="text" value="${medico.getPazienti().get(param.id).getNome()} ${medico.getPazienti().get(param.id).getCognome()}" readonly>
                            <input class="form-control" type="hidden" name="id" value="${param.id}">
                        </div>
                        <div class="form-group">
                          <label for="exampleFormControlInput1">Data</label>
                          <input class="form-control" name="data_esame" type="date" value="<%= java.time.LocalDate.now() %>">                      
                        </div>
                        <div class="form-group">
                            <label for="exampleFormControlInput1">Tipo esame</label>
                            <select name="tipo_esame">
                                  <option value="-1"></option>
                              <c:forEach var="t" items="${medico.getTipi_esami()}">
                                  <option value="${t[0]}">${t[1]}</option>
                              </c:forEach>
                            </select>
                        </div>  
                        <div class="modal-footer">
                            <button type="reset" class="btn btn-info" data-dismiss="modal">Annulla</button>
                            <button type="submit" class="btn btn-primary">Inserisci</button>
                        </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
         </c:if>
          <!-- Modal ricetta-->
           <div class="modal fade" id="ins_ricetta" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
             <div class="modal-dialog modal-dialog-centered" role="document">
               <div class="modal-content">
                 <div class="modal-header">
                   <h5 class="modal-title" id="exampleModalLongTitle">Prescrivi Ricetta</h5>
                 </div>
                 <div class="modal-body">
                   <form action="../InsertRicetta"  method="POST">
                       <div class="form-group">
                         <label for="exampleFormControlInput1">Paziente</label>
                            <input class="form-control" type="text" name="paziente" value="${medico.getPazienti().get(param.id).getNome()} ${medico.getPazienti().get(param.id).getCognome()}" readonly>
                            <input class="form-control" type="hidden" name="id" value="${param.id}">
                       </div>
                       <div class="form-group">
                         <label for="exampleFormControlInput1">Data</label>
                         <input class="form-control" name="data" type="text" value="<%= java.time.LocalDate.now() %>" readonly>                      
                       </div>
                       <div class="form-group">
                         <label for="exampleFormControlTextarea1">Farmaco</label>
                         <textarea class="form-control" id="exampleFormControlTextarea1" name="farmaco" rows="3"></textarea>
                       </div>
                       <div class="modal-footer">
                            <button type="reset" class="btn btn-info" data-dismiss="modal">Annulla</button>
                            <button type="submit" class="btn btn-primary">Inserisci</button>
                       </div>
                   </form>
                 </div>
               </div>
             </div>
           </div>
            
          <!-- Section: profile -->
          <section id="profilo" class="home-section paddingtop-110 paddingbot-30">
               <div class="container">
                <div class="row">
                    <div class="col-sm-2 col-md-2"></div>
                    <div class="col-sm-3 col-md-3">
                        <img src='../img_paz/${medico.getPazienti().get(param.id).getCf()}/avataaars.png' class="img-responsive center-block"/>
                    </div>
                    <div class="col-sm-5 col-md-5">
                        <div class="panel panel-info">
                            <div class="panel-heading" style="background-color:#3fbbc0;">
                                <h3 class="panel-title" style="color:white;font-size:150%;">Informazioni paziente </h3>
                            </div>  
                            <div class="panel-body"  style="overflow-x: auto;">
                                <table style="width: 100%">
                                    <tr>
                                        <th>Codice Fiscale: </th>
                                        <th>${medico.getPazienti().get(param.id).getCf()}</th>
                                    </tr>
                                    <tr>
                                        <th>Nome: </th>
                                        <th>${medico.getPazienti().get(param.id).getNome()}</th>
                                    </tr>
                                    <tr>
                                        <th>Cognome: </th>
                                        <th>${medico.getPazienti().get(param.id).getCognome()}</th>
                                    </tr>
                                    <tr>
                                        <th>Sesso: </th>
                                        <th>${medico.getPazienti().get(param.id).getSesso()}</th>
                                    </tr>
                                    <tr>
                                        <th>Data di Nascita: </th>
                                        <th>${medico.getPazienti().get(param.id).getData_nascita()}</th>
                                    </tr>
                                    <tr>
                                        <th>Luogo di Nascita: </th>
                                        <th>${medico.getPazienti().get(param.id).getLuogo_nascita()}</th>
                                    </tr>
                                    <tr>
                                        <th>Codice Provinciale: </th>
                                        <th>${medico.getPazienti().get(param.id).getCod_prov()}</th>
                                    </tr>
                                    <tr id="visite">
                                        <th>Email: </th>
                                        <th>${medico.getPazienti().get(param.id).getEmail()}</th>
                                    </tr>
                                    <tr>
                                        <th>Medico di Base: </th>
                                        <th>${medico.getPazienti().get(param.id).getNome_med()} ${medico.getPazienti().get(param.id).getCognome_med()}</th>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
               </div>
               </section>
            <!-- Section: profile -->
            
            <div class="center" style="display: none;">
            <button id="close" style="text-align: right">X</button>
            <form action="../InsertData"  method="POST" >
                <input type="text" name="cf_paz" value=${param.cf}>
                <center>
                    <b>Visita</b>
                    <br>
                    Data: <%= java.time.LocalDate.now() %>
                    Esito: <input type="text" name="esito_visita">
                    <br>
                    <b>Esame</b>
                    <br>
                    Data: <input type="date" name="data_visita">
                    <select name="tipo_esame">
                        <c:forEach var="t" items="${tipo_es.rows}">
                            <option value="${t.id}">${t.nome_esame}</option>
                        </c:forEach>>
                    </select>
                    Risultato: <input type="text" name="ris_esame">
                    <br>
                        <b>Ricetta</b>
                    <br>
                    Data: <%= java.time.LocalDate.now() %>
                    Farmaco: <input type="text" name="farmaco">
                    <br><br>
                    <input type="submit" value="Submit"> 
                </center>
            </form>
            </div>
          
         <!-- Section: visite -->
            <div class="anchor" id="visite"></div>
            <section class="home-section paddingtop-30 paddingbot-30">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-2 col-md-2"></div>
                        <div class="col-sm-8 col-md-8">
                            <div class="panel panel-info">
                                <div class="panel-heading" style="background-color:#3fbbc0;">
                                    <center><h3 class="panel-title" style="color:white;font-size:200%;">Visite</h3></center>
                                </div>  
                                <div class="panel-body">
                                    <center><table class="table table-hover table-bordered tabledata" style="width: 99%" id ="tablePagVisite" >
                                        <thead>
                                            <tr>
                                                <th scope="col">Data</th>
                                                <th scope="col">Medico</th>
                                                <th scope="col">Esito</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <c:forEach var="v" items="${medico.getPazienti().get(param.id).getVisite()}">
                                            <tr>
                                                <td>${v.getData_visita()}</td>
                                                <td>${v.getNomeMed()} ${v.getCognomeMed()}</td>
                                                <td>${v.getRisultato()}</td>
                                            </tr>
                                            </c:forEach>
                                        </tbody>
                                        </table></center>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!-- Section: visite -->
            <!-- Section: visita spec -->
            <div class="anchor" id="visite_spec"></div>
            <section class="home-section paddingtop-30 paddingbot-30">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-2 col-md-2"></div>
                        <div class="col-sm-8 col-md-8">
                            <div class="panel panel-info">
                                <div class="panel-heading" style="background-color:#3fbbc0;">
                                    <center><h3 class="panel-title" style="color:white;font-size:200%;">Visite Specialistiche</h3></center>
                                </div>  
                                <div class="panel-body">
                                    <center><table class="table table-hover table-bordered tabledata" style="width: 99%" id="tablePagVisiteSpec">
                                        <thead>
                                            <tr>
                                                <th scope="col">Data</th>
                                                <th scope="col">Medico</th>
                                                <th scope="col">Medico Visita</th>
                                                <th scope="col">Tipo Visita</th>
                                                <th scope="col">Esito Visita</th>
                                                <th scope="col">Ticket</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <c:forEach var="e" items="${medico.getPazienti().get(param.id).getVisite_spec()}">
                                                <tr>
                                                    <td>${e.getData_visita()}</td>
                                                    <td>${e.getNomeMed()} ${e.getCognomeMed()}</td>
                                                    <td>${e.getNomeMed_visita()} ${e.getCognomeMed_visita()}</td>
                                                    <td>${e.getNome_visita()}</td>
                                                    <td>${e.getRisultato()}</td>
                                                    <c:if test="${e.getTicket() == 0}"><td><span class="glyphicon glyphicon-remove" style="color:red" data-toggle="tooltip" data-placement="right" title="Non pagato"></span></td></c:if>
                                                    <c:if test="${e.getTicket() == 1}"><td><span class="glyphicon glyphicon-ok" style="color:green" data-toggle="tooltip" data-placement="right" title="Pagato"></td></c:if>
                                                </tr>
                                            </c:forEach>
                                        </tbody>
                                        </table></center>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
      <!-- Section: visita spec -->
      <!-- Section: esami -->
            <div class="anchor" id="esami"></div>
            <section class="home-section paddingtop-30 paddingbot-30">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-2 col-md-2"></div>
                        <div class="col-sm-8 col-md-8">
                            <div class="panel panel-info">
                                <div class="panel-heading" style="background-color:#3fbbc0;">
                                    <center><h3 class="panel-title" style="color:white;font-size:200%;">Esami</h3></center>
                                </div>  
                                <div class="panel-body">
                                    <center><table class="table table-hover table-bordered tabledata" style="width: 99%"id="tablePagEsami">
                                        <thead>
                                            <tr>
                                                <th scope="col">Data</th>
                                                <th scope="col">Medico</th>
                                                <th scope="col">Tipo Esame</th>
                                                <th scope="col">Esito Esame</th>
                                                <th scope="col">Ticket</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                         <c:forEach var="e" items="${medico.getPazienti().get(param.id).getEsami()}">
                                                <tr>
                                                    <td>${e.getData_esame()}</td>
                                                    <td>${e.getNomeMed()} ${e.getCognomeMed()}</td>
                                                    <td>${e.getNome_esame()}</td>
                                                    <td>${e.getRisultato()}</td>
                                                    <c:if test="${e.getTicket() == 0}"><td><span class="glyphicon glyphicon-remove" style="color:red" data-toggle="tooltip" data-placement="right" title="Non pagato"></span></td></c:if>
                                                    <c:if test="${e.getTicket() == 1}"><td><span class="glyphicon glyphicon-ok" style="color:green" data-toggle="tooltip" data-placement="right" title="Pagato"></td></c:if>
                                                </tr>
                                        </c:forEach>
                                        </tbody>
                                        </table></center>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
      <!-- Section: esami -->
      <!-- Section: ricette -->
            <div class="anchor" id="ricette"></div>
            <section class="home-section paddingtop-30 paddingbot-30">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-2 col-md-2"></div>
                        <div class="col-sm-8 col-md-8">
                            <div class="panel panel-info">
                                <div class="panel-heading" style="background-color:#3fbbc0;">
                                    <center><h3 class="panel-title" style="color:white;font-size:200%;">Ricette</h3></center>
                                </div>  
                                <div class="panel-body">
                                    <center><table class="table table-hover table-bordered tabledata" style="width: 99%" id="tablePagRicette">
                                        <thead>
                                            <tr>
                                                <th scope="col">Data</th>
                                                <th scope="col">Medico</th>
                                                <th scope="col">Farmaco</th>
                                                <th scope="col">QR</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <c:forEach var="r" items="${medico.getPazienti().get(param.id).getRicette()}">
                                                <tr>
                                                    <td>${r.getData_presc()}</td>
                                                    <td>${r.getNomeMed()} ${r.getCognomeMed()}</td>
                                                    <td>${r.getFarmaco()}</td>
                                                    <td>
                                                        <center>
                                                            <a href="../pdf_ricette/${r.getId()}.pdf" target="_blank">
                                                                <button type="submit" class="btn btn-info btn-sm"><span class="glyphicon glyphicon-eye-open"></span></button>
                                                            </a>
                                                            <a href="../pdf_ricette/${r.getId()}.pdf" id="download_ricetta" download="ricetta_${r.getId()}">
                                                                <button type="submit" class="btn btn-info btn-sm"><span class="glyphicon glyphicon-download-alt"></span></button>
                                                            </a>
                                                        </center>
                                                    </td>
                                                </tr>
                                            </c:forEach>
                                        </tbody>
                                        </table></center>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <a id="back-to-top" href="#" class="btn btn-info btn-lg back-to-top" role="button" title="Ritorna all'inizio della pagina" data-toggle="tooltip" data-placement="left">
                    <span class="glyphicon glyphicon-chevron-up"></span>
                </a>
            </section>            
        </div>
      <!-- Section: ricette -->
     <!-- Core JavaScript Files -->
     <script src="../js/jquery.min.js"></script>
     <script src="../js/bootstrap.min.js"></script>
     <script src="../js/jquery.easing.min.js"></script>
     <script src="../js/wow.min.js"></script>
     <script src="../js/jquery.scrollTo.js"></script>
     <script src="../js/jquery.appear.js"></script>
     <script src="../js/stellar.js"></script>
     <script src="../plugins/cubeportfolio/js/jquery.cubeportfolio.min.js"></script>
     <script src="../js/owl.carousel.min.js"></script>
     <script src="../js/nivo-lightbox.min.js"></script>
     <script src="../js/custom.js"></script>
     <script src="../js/datatables.js"></script>
     <script>
        $(document).ready(function () {
            $('.tabledata').DataTable( {
                "scrollX": true,
                "language": {
                    "sEmptyTable":     "Nessun dato presente nella tabella",
                    "sInfo":           "Pagina _PAGE_ di _PAGES_",
                    "sInfoEmpty":      "Vista da 0 a 0 di 0 elementi",
                    "sInfoFiltered":   "(filtrati da _MAX_ elementi totali)",
                    "sInfoPostFix":    "",
                    "sInfoThousands":  ".",
                    "sLengthMenu":     "Visualizza _MENU_ elementi",
                    "sLoadingRecords": "Caricamento...",
                    "sProcessing":     "Elaborazione...",
                    "sSearch":         "Cerca:",
                    "sZeroRecords":    "La ricerca non ha portato alcun risultato.",
                    "oPaginate": {
                        "sPrevious":   "Precedente",
                        "sNext":       "Successivo"
                    }
                },
                "order": [0, 'desc']
            } );
            $('.dataTables_length').addClass('bs-select');
            $(window).scroll(function () {
                if ($(this).scrollTop() > 50) {
                    $('#back-to-top').fadeIn();
                } else {
                    $('#back-to-top').fadeOut();
                }
            });
               // scroll body to 0px on click
            $('#back-to-top').click(function () {
                $('#back-to-top').tooltip('hide');
                $('body,html').animate({
                    scrollTop: 0
                }, 800);
                return false;
            });

            $('#back-to-top').tooltip('show');
        });
        
        $(document).ready(function(){
            

        });
    </script>
    <c:if test="${param.err == 3}">
      <script> window.alert("Inserimento esame non riuscito");</script>
    </c:if>
    <c:if test="${param.err == 5}">
      <script> window.alert("Inserimento della ricetta non riuscito");</script>
    </c:if>
    <c:if test="${param.err == 6}">
      <script> window.alert("Inserimento della visita non riuscito");</script>
    </c:if>
    <c:if test="${param.err == 7}">
      <script> window.alert("Inserimento della visita specialistica non riuscito");</script>
    </c:if>
    <c:if test="${param.err == 11}">
      <script> window.alert("Update della visita specialistica non riuscito");</script>
    </c:if>
     </body>
    </html>
</c:if>

<c:if test="${ssp != null}">
    <c:redirect url="homeSSP.jsp"/>
</c:if>
    
<c:if test="${paziente != null}">
    <c:redirect url="homePaziente.jsp"/>
</c:if>

<c:if test="${medico == null and pazeinte == null and ssp == null}">
    <c:redirect url="../index.jsp?dopo"/>
</c:if>
