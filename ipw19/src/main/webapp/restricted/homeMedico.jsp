<%-- 
    Document   : homeMedico
    Created on : 2 ott 2019, 11:29:05
    Author     : Andrea Cerretti
--%>
<!DOCTYPE html>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page errorPage="../error.jsp" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<c:if test="${medico  != null}">
    <html lang="it">
       <head>
            <link rel="shortcut icon" type="image/png" href="../img/ico.ico"/>
            <meta charset="utf-8">
            <meta name="viewport" content="width=device-width, initial-scale=1.0">
            <meta name="description" content="">
            <meta name="author" content="">
            <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

            <title>IPW2019 ${medico.getNome()} ${medico.getCognome()}</title>

            <!-- css -->
            <link href="../font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
            <link href="../plugins/cubeportfolio/css/cubeportfolio.min.css" rel="stylesheet" type="text/css">
            <link href="../css/bootstrap.min.css" rel="stylesheet" type="text/css">
            <link href="../css/nivo-lightbox.css" rel="stylesheet" />
            <link href="../css/nivo-lightbox-theme/default/default.css" rel="stylesheet" type="text/css" />
            <link href="../css/owl.carousel.css" rel="stylesheet" media="screen" />
            <link href="../css/owl.theme.css" rel="stylesheet" media="screen" />
            <link href="../css/animate.css" rel="stylesheet" />
            <link href="../css/style.css" rel="stylesheet">
            <link href="../css/datatables.min.css" rel="stylesheet">

            <!-- boxed bg -->
            <link id="bodybg" href="../bodybg/bg1.css" rel="stylesheet" type="text/css" />
            <!-- template skin -->
            <link id="t-colors" href="../color/default.css" rel="stylesheet">
            <style>
                .back-to-top {
                    cursor: pointer;
                    position: fixed;
                    bottom: 10%;
                    right: 5%;
                    display:none;
                }
                
                .anchor{
                    position: absolute;
                    margin-top: -10%;
                }
            </style>
        </head>
    <body id="page-top" data-spy="scroll" data-target=".navbar-custom">
        <div id="wrapper">
        <nav class="navbar navbar-custom navbar-fixed-top" role="navigation">
            <div class="top-area">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-6 col-md-6">
                            <p class="bold text-left">
                                <c:if test="${medico.getSesso() == 'M'}">  Benvenuto Dr.  </c:if>
                                <c:if test="${medico.getSesso() == 'F'}"> Benvenuta Dr.ssa  </c:if>
                                ${medico.getNome()} ${medico.getCognome()}
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="container navigation">
                <div class="navbar-header page-scroll">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-main-collapse">
                        <i class="fa fa-bars"></i>
                    </button>
                    <a class="navbar-brand" href="../index.jsp"> <img src="../img/logo.png" alt="" width="150" height="40" /></a>
                </div>

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse navbar-right navbar-main-collapse">
                    <ul class="nav navbar-nav">
                        <li><a href="#pazienti">Pazienti</a></li>
                        <li> <a href="${pageContext.request.contextPath}/Logout">Logout</a> </li>
                    </ul>
                </div>
                <!-- /.navbar-collapse -->
            </div>
          <!-- /.container -->
        </nav>
          <!-- Section: profile -->
        <section id="profilo" class="home-section paddingtop-110 paddingbot-20">
            <div class="container">
                <div class="row">
                    <div class="col-sm-2 col-md-2"></div>
                    <div class="col-sm-3 col-md-3">
                        <img src='../img_med/${medico.getCf()}/avataaars.png' class="img-responsive center-block"/>
                    </div>
                    <div class="col-sm-5 col-md-5">
                        <div class="panel panel-info">
                            <div class="panel-heading" style="background-color:#3fbbc0;">
                                <center><h3 class="panel-title" style="color:white;font-size:150%;">Informazioni personali</h3></center>
                            </div>  
                            <div class="panel-body" style="overflow-x: auto;">
                                <center><table style="width: 100%;">
                                    <tr>
                                        <th>Codice Fiscale: </th>
                                        <th>${medico.getCf()}</th>
                                    </tr>
                                    <tr>
                                        <th>Nome: </th>
                                        <th>${medico.getNome()}</th>
                                    </tr>
                                    <tr>
                                        <th>Cognome: </th>
                                        <th>${medico.getCognome()}</th>
                                    </tr>
                                    <tr>
                                        <th>Sesso: </th>
                                        <th>${medico.getSesso()}</th>
                                    </tr>
                                    <tr>
                                        <th>Data di Nascita: </th>
                                        <th>${medico.getData_nascita()}</th>
                                    </tr>
                                    <c:if test="${medico.getTipo_med() == 0}">
                                        <tr>
                                            <th>Codice Provincia: </th>
                                            <th>${medico.getCod_prov()}</th>
                                        </tr>
                                        <tr>
                                            <th>Città: </th>
                                            <th>${medico.getCitta()}</th>
                                        </tr>
                                    </c:if>
                                    <c:if test="${medico.getTipo_med() == 1}">
                                        <tr>
                                            <th>Specializzazione: </th>
                                            <th>${medico.getSpec()}</th>
                                        </tr>
                                    </c:if>
                                    <tr>
                                        <th>Email: </th>
                                        <th>${medico.getEmail()}</th>
                                    </tr>
                                    </table></center>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- Section: profile -->
            
        <!-- Section: pazienti -->
        <div class="anchor" id="pazienti"></div>
        <section class="home-section paddingtop-30 paddingbot-30">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-1 col-md-1 col-lg-1"></div>
                            <div class="col-sm-10 col-md-10 col-lg-10">
                                <div class="panel panel-info">
                                    <div class="panel-heading" style="background-color:#3fbbc0;">
                                        <center><h3 class="panel-title" style="color:white;font-size:200%;">Pazienti</h3></center>
                                    </div>  
                                    <div class="panel-body">
                                        <center><table class="table table-hover table-bordered" style="width: 99%" id ="tablePagPazienti" >
                                            <thead>
                                                <tr>
                                                    <th scope="col">Codice fiscale</th>
                                                    <th scope="col">Nome</th>
                                                    <th scope="col">Cognome</th>
                                                    <th scope="col">Ultima Visita</th>
                                                    <th scope="col">Ultima Ricetta</th>
                                                    <th scope="col">Info paziente</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <c:forEach var="i" begin="0" end="${medico.getPazienti().size() - 1}">
                                                    <tr>
                                                        <td>${medico.getPazienti().get(i).getCf()}</td>
                                                        <td>${medico.getPazienti().get(i).getNome()}</td>
                                                        <td>${medico.getPazienti().get(i).getCognome()}</td>
                                                        <td>${medico.getPazienti().get(i).getUltimaVisita()}</td>
                                                        <td>${medico.getPazienti().get(i).getUltimaRicetta()}</td>
                                                        <td>
                                                            <center><a href="paziente.jsp?id=<c:out value="${i}"/>"><button type="submit" class="btn btn-info btn-sm"><span class="glyphicon glyphicon-user"></span></button></a></center>                                               
                                                        </td>
                                                    </tr>
                                                </c:forEach>
                                            </tbody>
                                            </table></center>
                                    </div>
                                </div>
                            </div>
                        <div class="col-sm-2 col-md-2 col-lg-2"></div>
                    </div>
                </div>
                <a id="back-to-top" href="#" class="btn btn-info btn-lg back-to-top" role="button" title="Ritorna all'inizio della pagina" data-toggle="tooltip" data-placement="left"><span class="glyphicon glyphicon-chevron-up"></span></a>
            </section> 
            <!-- Section: pazienti -->
            </div>
        
            <!-- Core JavaScript Files -->
            <script src="../js/jquery.min.js"></script>
            <script src="../js/bootstrap.min.js"></script>
            <script src="../js/jquery.easing.min.js"></script>
            <script src="../js/wow.min.js"></script>
            <script src="../js/jquery.scrollTo.js"></script>
            <script src="../js/jquery.appear.js"></script>
            <script src="../js/stellar.js"></script>
            <script src="../plugins/cubeportfolio/js/jquery.cubeportfolio.min.js"></script>
            <script src="../js/owl.carousel.min.js"></script>
            <script src="../js/nivo-lightbox.min.js"></script>
            <script src="../js/custom.js"></script>
            <script src="../js/datatables.js"></script>
            <script>
                $(document).ready(function () {
                    $('#tablePagPazienti').DataTable( {
                        "scrollX": true,
                        "language": {
                            "sEmptyTable":     "Nessun dato presente nella tabella",
                            "sInfo":           "Pagina _PAGE_ di _PAGES_",
                            "sInfoEmpty":      "Vista da 0 a 0 di 0 elementi",
                            "sInfoFiltered":   "(filtrati da _MAX_ elementi totali)",
                            "sInfoPostFix":    "",
                            "sInfoThousands":  ".",
                            "sLengthMenu":     "Visualizza _MENU_ elementi",
                            "sLoadingRecords": "Caricamento...",
                            "sProcessing":     "Elaborazione...",
                            "sSearch":         "Cerca:",
                            "sZeroRecords":    "La ricerca non ha portato alcun risultato.",
                            "oPaginate": {
                                "sPrevious":   "Precedente",
                                "sNext":       "Successivo"
                            }
                        }
                    } );
                    $('.dataTables_length').addClass('bs-select');
                    $(window).scroll(function () {
                        if ($(this).scrollTop() > 50) {
                           $('#back-to-top').fadeIn();
                        } else {
                            $('#back-to-top').fadeOut();
                        }
                    });
                       // scroll body to 0px on click
                    $('#back-to-top').click(function () {
                        $('#back-to-top').tooltip('hide');
                        $('body,html').animate({
                            scrollTop: 0
                        }, 800);
                        return false;
                    });

                    $('#back-to-top').tooltip('show');
                });
           </script>
        </body>
    </html>
</c:if>

<c:if test="${ssp != null}">
    <c:redirect url="homeSSP.jsp"/>
</c:if>
    
<c:if test="${paziente != null}">
    <c:redirect url="homePaziente.jsp"/>
</c:if>

<c:if test="${medico == null and pazeinte == null and ssp == null}">
    <c:redirect url="../index.jsp?dopo"/>
</c:if>