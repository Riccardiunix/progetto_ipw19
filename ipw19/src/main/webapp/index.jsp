<!DOCTYPE html>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page errorPage="error.jsp"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<html lang="it">
    <%
        Cookie[] cookies = request.getCookies();
        if ((cookies != null) && (cookies.length > 0)) {
            for (Cookie cookie : cookies) {
                if (cookie.getName().equals("ipw19id")) ((HttpServletResponse) response).sendRedirect("Login");
            }
        }
    %>

<head>
  <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/cookieconsent@3/build/cookieconsent.min.css" />
  <link rel="shortcut icon" type="image/png" href="img/ico.ico"/>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="">
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

  <title>IPW2019 HOME</title>

  <!-- css -->
  <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
  <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
  <link rel="stylesheet" type="text/css" href="plugins/cubeportfolio/css/cubeportfolio.min.css">
  <link href="css/nivo-lightbox.css" rel="stylesheet" />
  <link href="css/nivo-lightbox-theme/default/default.css" rel="stylesheet" type="text/css" />
  <link href="css/owl.carousel.css" rel="stylesheet" media="screen" />
  <link href="css/owl.theme.css" rel="stylesheet" media="screen" />
  <link href="css/animate.css" rel="stylesheet" />
  <link href="css/style.css" rel="stylesheet">

  <!-- boxed bg -->
  <link id="bodybg" href="bodybg/bg1.css" rel="stylesheet" type="text/css" />
  <!-- template skin -->
  <link id="t-colors" href="color/default.css" rel="stylesheet">

</head>

<body id="page-top" data-spy="scroll" data-target=".navbar-custom">

  <div id="wrapper">

    <nav class="navbar navbar-custom navbar-fixed-top" role="navigation">
      <div class="container navigation">

        <div class="navbar-header page-scroll">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-main-collapse">
            <i class="fa fa-bars"></i>
          </button>
          <a class="navbar-brand" href="index.jsp">
            <img src="img/logo.png" alt="" width="150" height="40" />
          </a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse navbar-right navbar-main-collapse">
          <ul class="nav navbar-nav">
            <li><a href="#intro">Home</a></li>
            <li><a href="#motivi">Perchè Sceglierci</a></li>
            <li><a href="#servizi">Servizi</a></li>
            <li><a href="#medici">Medici</a></li>
            <li><a href="#login">Login</a></li>
          </ul>
        </div>
        <!-- /.navbar-collapse -->
      </div>
      <!-- /.container -->
    </nav>
    
    <div class="modal fade" id="forgotpass" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Effettua Visita</h5>
                </div>
                <div class="modal-body">
                    <form action="ForgotPassword" method="POST">
                        <div class="form-group">
                            <label for="exampleFormControlInput1">Email</label>
                            <label>Email</label>
                            <input type="email" name="email" id="email" class="form-control input-md" data-rule="email" data-msg="Please enter a valid email" required>
                        </div>
                        <div class="modal-footer">
                            <button type="reset" class="btn btn-info" data-dismiss="modal">Annulla</button>
                            <button type="submit" class="btn btn-primary">Inserisci</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <!-- Section: intro -->
    <section id="intro" class="intro">
      <div class="intro-content">
        <div class="container">
          <div class="row">
            <div class="col-lg-6">
              <div class="wow fadeInDown" data-wow-offset="0" data-wow-delay="0.1s">
                <h2 class="h-ultra">IPW 2019 </h2>
              </div>
              <div class="wow fadeInUp" data-wow-offset="0" data-wow-delay="0.1s">
                <h4 class="h-light">Migliora la tua vita con un click!</h4>
              </div>
              <div class="well well-trans">
                <div class="wow fadeInRight" data-wow-delay="0.1s">

                  <ul class="lead-list">
                    <li><span class="fa fa-check fa-2x icon-success"></span> <span class="list"><strong>Tempi d'attesa limitati</strong><br />La nostra ricca Equipe garantisce visite in tempi rapidi</span></li>
                    <li><span class="fa fa-check fa-2x icon-success"></span> <span class="list"><strong>Tanti medici a tua disposizione</strong><br />All' interno della nostra Equipe troverai il medico adatto a te</span></li>
                    <li><span class="fa fa-check fa-2x icon-success"></span> <span class="list"><strong>Interfaccia semplice e intuitiva</strong><br />Di facile utilizzo per medici e pazienti</span></li>
                  </ul>
                </div>
              </div>


            </div>
            <div class="col-lg-6">
                <div id="login" style="position:absolute;margin-top: -10%"></div>
                <div class="form-wrapper">
                  <div class="wow fadeInRight" data-wow-duration="2s" data-wow-delay="0.2s">

                    <div class="panel panel-skin">
                      <div class="panel-heading">
                        <h3 class="panel-title"><span class="fa fa-pencil-square-o"></span> LOGIN </h3>
                      </div>
                      <div class="panel-body">
                        <div id="sendmessage">Your message has been sent. Thank you!</div>
                        <div id="errormessage"></div>

                        <form action="Login" method="POST" role="form" class="contactForm lead">
                          <div class="row">
                                  <div class="col-xs-6 col-sm-6 col-md-6">
                                      <div class="form-group">
                                        <label>Email</label>
                                        <input type="email" name="email" id="email" class="form-control input-md" data-rule="email" data-msg="Please enter a valid email" required>
                                        <div class="validation"></div>
                                      </div>
                                  </div>
                                  <div class="col-xs-6 col-sm-6 col-md-6">
                                    <div class="form-group">
                                      <label>Password</label>
                                      <input type="password" name="password" id="password" class="form-control input-md"  data-rule="required" data-msg="Incorrect Password" required>
                                      <div class="validation"></div>
                                    </div>
                                </div>                 
                        </div>
                        <div class="form-check">
                            <input type="checkbox" class="form-check-input" name="ricordami">
                            <label class="form-check-label" for="exampleCheck1">Ricordami</label>
                            <label class="form-check-label" for="passwordDimenticata" style="display: block; text-align: right"><a href="#" data-toggle="modal" data-target="#forgotpass">Password dimenticata?</a></label>
                        </div>
                        <input type="submit" value="Submit" class="btn btn-skin btn-block btn-lg">
                      </form>
                    </div>
                  </div>
                </div>
            </div>
          </div>
        </div>
      </div>
    </section>

    <!-- /Section: intro -->

    <!-- Section: motivi -->
    <section id="motivi" class="home-section bg-gray paddingtop-80">
        <div class="container marginbot-50">
        <div class="row">
          <div class="col-lg-8 col-lg-offset-2">
            <div class="wow fadeInDown" data-wow-delay="0.1s">
              <div class="section-heading text-center">
                <h2 class="h-bold">Perchè sceglierci</h2>
                <p>Le funzionalità più importanti che offriamo</p>
              </div>
            </div>
            <div class="divider-short"></div>
          </div>
        </div>
      </div>
      <div class="container">
        <div class="row">
          <div class="col-sm-3 col-md-3">
            <div class="wow fadeInUp" data-wow-delay="0.2s">
              <div class="box text-center">

                <i class="fa fa-check fa-3x circled bg-skin"></i>
                <h4 class="h-bold">Resta in Contatto</h4>
                <p>
                  Resta sempre in contatto con il tuo medico di base e fai in modo che sappia gli esiti di tutti i tuoi esami per prescrivere le cure più adeguate alla tua persona.
                </p>
              </div>
            </div>
          </div>
          <div class="col-sm-3 col-md-3">
            <div class="wow fadeInUp" data-wow-delay="0.2s">
              <div class="box text-center">

                <i class="fa fa-list-alt fa-3x circled bg-skin"></i>
                <h4 class="h-bold">Non Scordarti Nulla</h4>
                <p>
                  Tieni sotto controllo tutte le tue prescizioni mediche e gli esami da fare per evitare di dimenticarteli.
                </p>
              </div>
            </div>
          </div>
          <div class="col-sm-3 col-md-3">
            <div class="wow fadeInUp" data-wow-delay="0.2s">
              <div class="box text-center">
                <i class="fa fa-user-md fa-3x circled bg-skin"></i>
                <h4 class="h-bold">Aiutato da Specialisti</h4>
                <p>
                  Trova lo specialista che ti serve e lui saprà già chi sei, ti prescriverà gli esami più adatti e ti sentirai come a casa.
                </p>
              </div>
            </div>
          </div>
          <div class="col-sm-3 col-md-3">
            <div class="wow fadeInUp" data-wow-delay="0.2s">
              <div class="box text-center">

                <i class="fa fa-hospital-o fa-3x circled bg-skin"></i>
                <h4 class="h-bold">Controlla gli Esiti</h4>
                <p>
                  Appena saranno disponibili gli esiti degli esami da te svolti, saprai subito i risultati e così come te anche i medici che ti seguono.
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>

    </section>
    <!-- /Section: motivi -->

    <!-- Section: servizi -->
    <section id="servizi" class="servizi home-section nopadding paddingtop-60">
      <div class="container marginbot-50">
        <div class="row">
          <div class="col-lg-8 col-lg-offset-2">
            <div class="wow fadeInDown" data-wow-delay="0.1s">
              <div class="section-heading text-center">
                <h2 class="h-bold">Servizi</h2>
                <p>I principali servizi che offriamo</p>
              </div>
            </div>
            <div class="divider-short"></div>
          </div>
        </div>
      </div>
      <div class="container">

        <div class="row">
          <div class="col-sm-6 col-md-6">
            <div class="wow fadeInUp" data-wow-delay="0.2s">
              <img src="img/dummy/img-1.jpg" class="img-responsive" alt="" />
            </div>
          </div>
          <div class="col-sm-3 col-md-3">

            <div class="wow fadeInRight" data-wow-delay="0.1s">
              <div class="service-box">
                <div class="service-icon">
                  <span class="fa fa-stethoscope fa-3x"></span>
                </div>
                <div class="service-desc">
                    <h5 class="h-light">Esami<br/> Radiologici</h5>
                 <p>
                    <ul>
                         <li>Rachide</li>
                         <li>Arti e Anca </li>
                         <li>Cranio temporomandibolare</li>
                     </ul>
                 </p>
                </div>
              </div>
            </div>

            <div class="wow fadeInRight" data-wow-delay="0.2s">
              <div class="service-box">
                <div class="service-icon">
                  <span class="fa fa-wheelchair fa-3x"></span>
                </div>
                <div class="service-desc">
                  <h5 class="h-light">Stratigrafia e Tomografia</h5>
                  <p>
                    <ul>
                        <li>Stratigrafia ATM</li>
                        <li>TAC</li>
                        <li>Tomografia Ottica</li>
                    </ul>
                 </p>
                </div>
              </div>
            </div>
            <div class="wow fadeInRight" data-wow-delay="0.3s">
              <div class="service-box">
                <div class="service-icon">
                  <span class="fa fa-plus-square fa-3x"></span>
                </div>
                <div class="service-desc">
                  <h5 class="h-light">Ecografia e Doppler</h5>
                  <p>
                    <ul>
                        <li>Ecoaddome Completo</li>
                        <li>Eco Color Doppler</li>
                        <li>Flussimetria Doppler</li>
                    </ul>
                </p>
                </div>
              </div>
            </div>


          </div>
          <div class="col-sm-3 col-md-3">

            <div class="wow fadeInRight" data-wow-delay="0.1s">
              <div class="service-box">
                <div class="service-icon">
                  <span class="fa fa-h-square fa-3x"></span>
                </div>
                <div class="service-desc">
                  <h5 class="h-light">Risonanza Magnetica</h5>
                  <p>
                    <ul>
                        <li>Colonna Vertebrale</li>
                        <li>Apparato Muscolo-Scheletrico</li>
                        <li>Atricolazioni</li>
                    </ul>
                  </p>
                </div>
              </div>
            </div>

            <div class="wow fadeInRight" data-wow-delay="0.2s">
              <div class="service-box">
                <div class="service-icon">
                  <span class="fa fa-filter fa-3x"></span>
                </div>
                <div class="service-desc">
                    <h5 class="h-light">Test <br/>Cutanei</h5>
                  <p>
                    <ul>
                        <li>Test Percutanei</li>
                        <li>Test Intracutanei</li>
                        <li>Prick e Patch Test</li>
                    </ul>
                </p>
                </div>
              </div>
            </div>
            <div class="wow fadeInRight" data-wow-delay="0.3s">
              <div class="service-box">
                <div class="service-icon">
                  <span class="fa fa-user-md fa-3x"></span>
                </div>
                <div class="service-desc">
                  <h5 class="h-light">Medicina <br/>Nucleare</h5>
                  <p>
                  <ul>
                      <li>Scintigrafia Ossea</li>
                      <li>Scintigrafia Renale</li>
                      <li>PET Cerebrale</li>
                  </ul>
                </p>
                </div>
              </div>
            </div>

          </div>

        </div>
      </div>
    </section>
    <!-- /Section: servizi -->


      <!-- Section: team -->
    <section id="medici" class="home-section bg-gray paddingbot-60">
      <div class="container marginbot-50">
        <div class="row">
          <div class="col-lg-8 col-lg-offset-2">
            <div class="wow fadeInDown" data-wow-delay="0.1s">
              <div class="section-heading text-center">
                <h2 class="h-bold">Medici</h2>
                <p>Questa è la nostra Equipe di medici di base e specialisti</p>
              </div>
            </div>
            <div class="divider-short"></div>
          </div>
        </div>
      </div>

      <div class="container">
        <div class="row">
          <div class="col-lg-12">

            <div id="filters-container" class="cbp-l-filters-alignLeft">
              <div data-filter="*" class="cbp-filter-item-active cbp-filter-item">All (
                <div class="cbp-filter-counter">6</div>)
              </div>
              <div data-filter=".base" class="cbp-filter-item">Medico di Base (
                <div class="cbp-filter-counter">5</div>)
              </div>
              <div data-filter=".specialista" class="cbp-filter-item">Medico Specialista(
                <div class="cbp-filter-counter">1</div>)
              </div>
            </div>

            <div id="grid-container" class="cbp-l-grid-team cbp cbp-caption-fadeIn cbp-animation-sequentially cbp-ready" style="height: 576px;">
              <ul class="cbp-wrapper" style="opacity: 1;">
                
                <li class="cbp-item base" style="width: 255px; height: 263px; transform: translate3d(0px, 0px, 0px);"><div class="cbp-item-wrapper"><div class="cbp-item-wrapper">
                  <a href="doctors/DVTGCL57B28A952G.html" class="cbp-caption cbp-singlePage">
                    <div class="cbp-caption-defaultWrap">
                        <center><img src="img_med/DVTGCL57B28A952G/avataaars.png" class="" width="90%"></center>
                    </div>
                    <div class="cbp-caption-activeWrap">
                      <div class="cbp-l-caption-alignCenter">
                        <div class="cbp-l-caption-body">
                          <div class="cbp-l-caption-text">VIEW PROFILE</div>
                        </div>
                      </div>
                    </div>
                  </a>
                  <a href="doctors/DVTGCL57B28A952G.html" class="cbp-singlePage cbp-l-grid-team-name">Graciliano De Vitto</a>
                  <div class="cbp-l-grid-team-position">Medico di Base</div>
                </div></div></li>
                
                <li class="cbp-item base" style="width: 255px; height: 263px; transform: translate3d(295px, 0px, 0px);"><div class="cbp-item-wrapper"><div class="cbp-item-wrapper">
                  <a href="doctors/GTNSBZ86S05A952W.html" class="cbp-caption cbp-singlePage">
                    <div class="cbp-caption-defaultWrap">
                        <center><img src="img_med/GTNSBZ86S05A952W/avataaars.png" class="" width="90%"></center>
                    </div>
                    <div class="cbp-caption-activeWrap">
                      <div class="cbp-l-caption-alignCenter">
                        <div class="cbp-l-caption-body">
                          <div class="cbp-l-caption-text">VIEW PROFILE</div>
                        </div>
                      </div>
                    </div>
                  </a>
                  <a href="doctors/GTNSBZ86S05A952W.html" class="cbp-singlePage cbp-l-grid-team-name">Sabazio Gaetani</a>
                  <div class="cbp-l-grid-team-position">Medico di Base</div>
                </div></div></li>
                
                <li class="cbp-item base" style="width: 255px; height: 263px; transform: translate3d(590px, 0px, 0px);"><div class="cbp-item-wrapper"><div class="cbp-item-wrapper">
                  <a href="doctors/CRNMRV91T65L378A.html" class="cbp-caption cbp-singlePage">
                    <div class="cbp-caption-defaultWrap">
                        <center><img src="img_med/CRNMRV91T65L378A/avataaars.png" class="" width="90%"></center>
                    </div>
                    <div class="cbp-caption-activeWrap">
                      <div class="cbp-l-caption-alignCenter">
                        <div class="cbp-l-caption-body">
                          <div class="cbp-l-caption-text">VIEW PROFILE</div>
                        </div>
                      </div>
                    </div>
                  </a>
                  <a href="doctors/CRNMRV91T65L378A.html" class="cbp-singlePage cbp-l-grid-team-name">Minervina Carinci</a>
                  <div class="cbp-l-grid-team-position">Medico di Base</div>
                </div></div></li>
                
                <li class="cbp-item base" style="width: 255px; height: 263px; transform: translate3d(885px, 0px, 0px);"><div class="cbp-item-wrapper"><div class="cbp-item-wrapper">
                  <a href="doctors/CRTLGU58E01L378A.html" class="cbp-caption cbp-singlePage">
                    <div class="cbp-caption-defaultWrap">
                        <center><img src="img_med/CRTLGU58E01L378A/avataaars.png" class="" width="90%"></center>
                    </div>
                    <div class="cbp-caption-activeWrap">
                      <div class="cbp-l-caption-alignCenter">
                        <div class="cbp-l-caption-body">
                          <div class="cbp-l-caption-text">VIEW PROFILE</div>
                        </div>
                      </div>
                    </div>
                  </a>
                  <a href="doctors/CRTLGU58E01L378A.html" class="cbp-singlePage cbp-l-grid-team-name">Luigi Cerutti</a>
                  <div class="cbp-l-grid-team-position">Medico di Base</div>
                </div></div></li>
                
                <li class="cbp-item base" style="width: 255px; height: 263px; transform: translate3d(0px, 313px, 0px);"><div class="cbp-item-wrapper"><div class="cbp-item-wrapper">
                  <a href="doctors/MNRLRN68M57H612G.html" class="cbp-caption cbp-singlePage">
                    <div class="cbp-caption-defaultWrap">
                        <center><img src="img_med/MNRLRN68M57H612G/avataaars.png" class="" width="90%"></center>
                    </div>
                    <div class="cbp-caption-activeWrap">
                      <div class="cbp-l-caption-alignCenter">
                        <div class="cbp-l-caption-body">
                          <div class="cbp-l-caption-text">VIEW PROFILE</div>
                        </div>
                      </div>
                    </div>
                  </a>
                  <a href="doctors/MNRLRN68M57H612G.html" class="cbp-singlePage cbp-l-grid-team-name">Lorena Minervino</a>
                  <div class="cbp-l-grid-team-position">Medico di Base</div>
                </div></div></li>
                
                
                <li class="cbp-item specialista" style="width: 255px; height: 263px; transform: translate3d(295px, 313px, 0px);"><div class="cbp-item-wrapper"><div class="cbp-item-wrapper">
                  <a href="doctors/TMMTNN78E22L378Z.html" class="cbp-caption cbp-singlePage">
                    <div class="cbp-caption-defaultWrap">
                        <center> <img src="img_med/TMMTNN78E22L378Z/avataaars.png" class="" width="90%"></center>
                    </div>
                    <div class="cbp-caption-activeWrap">
                      <div class="cbp-l-caption-alignCenter">
                        <div class="cbp-l-caption-body">
                          <div class="cbp-l-caption-text">VIEW PROFILE</div>
                        </div>
                      </div>
                    </div>
                  </a>
                  <a href="doctors/TMMTNN78E22L378Z.html" class="cbp-singlePage cbp-l-grid-team-name">Tommaso Trenini</a>
                  <div class="cbp-l-grid-team-position">Cardiologo</div>
                </div></div></li>
                
                <li class="cbp-item specialista" style="width: 255px; height: 263px; transform: translate3d(295px, 313px, 0px);"><div class="cbp-item-wrapper"><div class="cbp-item-wrapper">
                  <a href="doctors/GRDNDR62S12A952M.html" class="cbp-caption cbp-singlePage">
                    <div class="cbp-caption-defaultWrap">
                        <center> <img src="img_med/GRDNDR62S12A952M/avataaars.png" class="" width="90%"></center>
                    </div>
                    <div class="cbp-caption-activeWrap">
                      <div class="cbp-l-caption-alignCenter">
                        <div class="cbp-l-caption-body">
                          <div class="cbp-l-caption-text">VIEW PROFILE</div>
                        </div>
                      </div>
                    </div>
                  </a>
                  <a href="doctors/TMMTNN78E22L378Z.html" class="cbp-singlePage cbp-l-grid-team-name">Andrea Giardini</a>
                  <div class="cbp-l-grid-team-position">Radiologo</div>
                </div></div></li>
                  
              </ul>
            </div>
          </div>
        </div>
      </div>

    </section>
    <!-- /Section: team -->

    <footer>
      <div class="sub-footer">
        <div class="container">
          <div class="row">
            <div class="col-sm-6 col-md-6 col-lg-6">
              <div class="wow fadeInLeft" data-wow-delay="0.1s">
                <div class="text-left">
                  <p>&copy;Copyright 2019-2020- Ipw_2019</p>
                </div>
              </div>
            </div>
            <div class="col-sm-6 col-md-6 col-lg-6">
              <div class="wow fadeInRight" data-wow-delay="0.1s">
                <div class="text-right">
                  <div class="credits">
                      <p>Project By Bordon Riccardo, Cerretti Andrea and Poli Alessandro</p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </footer>

  </div>
  <a href="#" class="scrollup"><i class="fa fa-angle-up active"></i></a>

  <!-- Core JavaScript Files -->
  <script src="js/jquery.min.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <script src="js/jquery.easing.min.js"></script>
  <script src="js/wow.min.js"></script>
  <script src="js/jquery.scrollTo.js"></script>
  <script src="js/jquery.appear.js"></script>
  <script src="js/stellar.js"></script>
  <script src="plugins/cubeportfolio/js/jquery.cubeportfolio.min.js"></script>
  <script src="js/owl.carousel.min.js"></script>
  <script src="js/nivo-lightbox.min.js"></script>
  <script src="js/custom.js"></script>
  <script src='https://cdn.jsdelivr.net/npm/cookieconsent@3/build/cookieconsent.min.js' data-cfasync="false"></script>
  <script>
    window.cookieconsent.initialise({
        "palette": {
        "popup": {
          "background": "#237afc"
        },
        "button": {
          "background": "#fff",
          "text": "#237afc"
        }
      }
    });
  </script>
  <c:if test="${param.err == 0}">
      <script> window.alert("Utente non trovato")</script>
  </c:if>
      
  <c:if test="${param.err == 2}">
      <script> window.alert("Email non trovata")</script>
  </c:if>

</body>

</html>
