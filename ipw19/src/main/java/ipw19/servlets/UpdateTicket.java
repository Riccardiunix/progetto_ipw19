package ipw19.servlets;

import commons.dao.exceptions.DAOException;
import commons.dao.exceptions.DAOFactoryException;
import commons.dao.factories.DAOFactory;
import ipw19.dao.PazienteDAO;
import ipw19.entities.Paziente;
import ipw19.entities.Utenti;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author orion
 */
@WebServlet(urlPatterns = {"/UpdateTicket"})
public class UpdateTicket extends HttpServlet {
    
    private PazienteDAO pazienteDao;
    private Utenti utenti;
    
    @Override
    public void init() throws ServletException {
        DAOFactory daoFactory = (DAOFactory) super.getServletContext().getAttribute("daoFactory");
        if (daoFactory == null) throw new ServletException("Impossible to get dao factory for user storage system");
        try {
            pazienteDao = daoFactory.getDAO(PazienteDAO.class);
            utenti = daoFactory.getUtenti();
        } catch (DAOFactoryException ex) {
            throw new ServletException("Impossible to get dao factory for user storage system", ex);
        }
    }

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     * @throws commons.dao.exceptions.DAOException
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, DAOException {
        try{
            HttpSession session = request.getSession();
            Paziente paziente = (Paziente) session.getAttribute("paziente");
            boolean check = false;
            int tipo = Integer.parseInt(request.getParameter("tipo"));
            
            if (tipo == 0){
                int id_visita = Integer.parseInt(request.getParameter("id_visita"));
                check = pazienteDao.updateTicketVisita(paziente, id_visita);
                utenti.updateVisita(paziente.getCf());
            }
            else if (tipo == 1){
                int id_esame = Integer.parseInt(request.getParameter("id_esame"));
                check = pazienteDao.updateTicketEsame(paziente, id_esame);
                utenti.updateEsame(paziente.getCf());
            }
            
            if (check) response.sendRedirect("restricted/homePaziente.jsp");
            else response.sendRedirect("restricted/homePaziente.jsp?err=10");
        }
        catch(IOException | NumberFormatException e){
            response.sendRedirect("restricted/homePaziente.jsp?err=10");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (DAOException ex) {
            Logger.getLogger(UpdateTicket.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (DAOException ex) {
            Logger.getLogger(UpdateTicket.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
