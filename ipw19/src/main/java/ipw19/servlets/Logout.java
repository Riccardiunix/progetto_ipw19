package ipw19.servlets;

import commons.dao.exceptions.DAOFactoryException;
import commons.dao.factories.DAOFactory;
import ipw19.dao.CookieDAO;
import ipw19.entities.Medico;
import ipw19.entities.Paziente;
import ipw19.entities.ServizioSanitarioProvinciale;
import ipw19.entities.Utenti;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet(name = "Logout", urlPatterns = {"/Logout"})

public class Logout extends HttpServlet {
    
    private CookieDAO cookieDao;
    private Utenti utenti;
    
    @Override
    public void init() throws ServletException {
        DAOFactory daoFactory = (DAOFactory) super.getServletContext().getAttribute("daoFactory");
        if (daoFactory == null) throw new ServletException("Impossible to get dao factory for user storage system");
        try {
            cookieDao = daoFactory.getDAO(CookieDAO.class);
            utenti = daoFactory.getUtenti();
        } catch (DAOFactoryException ex) {
            throw new ServletException("Impossible to get dao factory for user storage system" + ex, ex);
        }
    }
    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();
        Medico med = (Medico) session.getAttribute("medico");
        if (med != null) utenti.removeMedico(med.getCf());
        else{
            Paziente paz = (Paziente) session.getAttribute("paziente");
            if (paz != null) utenti.removePaziente(paz.getCf());
            else{
                ServizioSanitarioProvinciale ssp = (ServizioSanitarioProvinciale) session.getAttribute("ssp");
                if (ssp != null) utenti.removeSSP(ssp.getCod_prov());
            }
        }
        session.invalidate();
        
        Cookie[] cookies = request.getCookies();
        if ((cookies != null) && (cookies.length > 0)) {
            for (Cookie cookie : cookies) {
                if (cookie.getName().equals("ipw19id")) cookieDao.removeCoockie(cookie.getValue());
                cookie.setMaxAge(0);
                response.addCookie(cookie);
            }
        }
        response.sendRedirect("index.jsp");
    }
    
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }
    
    @Override
     protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }
    
}
