/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ipw19.servlets;

import commons.dao.exceptions.DAOException;
import commons.dao.exceptions.DAOFactoryException;
import commons.dao.factories.DAOFactory;
import ipw19.dao.ServizioSanitarioProvincialeDAO;
import ipw19.entities.ServizioSanitarioProvinciale;
import ipw19.entities.Utenti;
import java.io.IOException;
import java.util.Date;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author orion
 */
@WebServlet(name = "UpdateEsame", urlPatterns = {"/UpdateEsame"})
public class UpdateEsame extends HttpServlet {
    
    private ServizioSanitarioProvincialeDAO sspDao;
    private Utenti utenti;

     @Override
    public void init() throws ServletException {
        DAOFactory daoFactory = (DAOFactory) super.getServletContext().getAttribute("daoFactory");
        if (daoFactory == null) throw new ServletException("Impossible to get dao factory for user storage system");
        try {
            sspDao = daoFactory.getDAO(ServizioSanitarioProvincialeDAO.class);
            utenti = daoFactory.getUtenti();
        } catch (DAOFactoryException ex) {
            throw new ServletException("Impossible to get dao factory for user storage system", ex);
        }
    }

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, DAOException {
        try{
            HttpSession session = request.getSession();
            ServizioSanitarioProvinciale ssp = (ServizioSanitarioProvinciale) session.getAttribute("ssp");
            int id_esame = Integer.parseInt(request.getParameter("id_esame"));
            String esito = request.getParameter("ris_esame");
            
            if (sspDao.updateEsame(ssp, id_esame, esito)) {
                utenti.update();
                sendMail(request, ssp.getEsami_da_fare().get(id_esame).getEmail());
                response.sendRedirect("restricted/homeSSP.jsp");
            }
            else  response.sendRedirect("restricted/homeSSP.jsp?err=8");
        }
        catch(IOException | NumberFormatException e){
            response.sendRedirect("restricted/homeSSP.jsp?err=8");
        }
    }
    
    private void sendMail(HttpServletRequest request, String email){
        final String host = "smtp.gmail.com";
        final String port = "465";
        final String username = "servizio.sanitario19@gmail.com";
        final String password = "hhtikdibgtctrqfb";
        Properties props = System.getProperties();

        props.setProperty("mail.smtp.host", host);
        props.setProperty("mail.smtp.port", port);
        props.setProperty("mail.smtp.socketFactory.port", port);
        props.setProperty("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
        props.setProperty("mail.smtp.auth", "true");
        props.setProperty("mail.smtp.starttls.enable", "true");
        props.setProperty("mail.debug", "true");

        Session session = Session.getInstance(props, new Authenticator() {
            @Override
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(username, password);
            }
        });

        Message msg = new MimeMessage(session);
        try {
            msg.setFrom(new InternetAddress(username));
            //msg.setRecipients(Message.RecipientType.TO, InternetAddress.parse(email, false));
            msg.setRecipients(Message.RecipientType.TO, InternetAddress.parse("pazienti.servizio.sanitario19@gmail.com", false));
            msg.setSubject("Un esame è stato aggiornato [" + email + "]");
            msg.setText("Visualizza l'esame su: Indirizzo web progetto");
            msg.setSentDate(new Date());

            Transport.send(msg);
        } catch (MessagingException me) {
            me.printStackTrace(System.err);
        }
    }


    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (DAOException ex) {
            Logger.getLogger(UpdateEsame.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (DAOException ex) {
            Logger.getLogger(UpdateEsame.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
