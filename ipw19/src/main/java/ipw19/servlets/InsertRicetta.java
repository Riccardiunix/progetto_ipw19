package ipw19.servlets;

import com.itextpdf.io.image.ImageData;
import com.itextpdf.io.image.ImageDataFactory;
import com.itextpdf.kernel.geom.PageSize;
import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfWriter;
import com.itextpdf.layout.Document;
import com.itextpdf.layout.element.Image;
import com.itextpdf.layout.element.Paragraph;
import commons.dao.exceptions.DAOException;
import commons.dao.exceptions.DAOFactoryException;
import commons.dao.factories.DAOFactory;
import ipw19.dao.MedicoDAO;
import ipw19.entities.Medico;
import ipw19.entities.Ricetta;
import ipw19.entities.Utenti;
import java.awt.Color;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.servlet.http.HttpSession;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFPalette;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.VerticalAlignment;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFColor;

/**
 *
 * @author orion
 */
@WebServlet(urlPatterns = {"/InsertRicetta"})
public class InsertRicetta extends HttpServlet {
    
    private MedicoDAO medicoDao;
    private Utenti utenti;
    
    @Override
    public void init() throws ServletException {
        DAOFactory daoFactory = (DAOFactory) super.getServletContext().getAttribute("daoFactory");
        if (daoFactory == null) throw new ServletException("Impossible to get dao factory for user storage system");
        try {
            medicoDao = daoFactory.getDAO(MedicoDAO.class);
            utenti = daoFactory.getUtenti();
        } catch (DAOFactoryException ex) {
            throw new ServletException("Impossible to get dao factory for user storage system", ex);
        }
    }

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     * @throws java.lang.ClassNotFoundException
     * @throws java.sql.SQLException
     * @throws java.lang.InterruptedException
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, ClassNotFoundException, SQLException, InterruptedException {
        int id = Integer.parseInt(request.getParameter("id"));
        String farmaco = request.getParameter("farmaco");
        try{
            HttpSession session = request.getSession();
            Medico medico = (Medico) session.getAttribute("medico");
            
            int id2 = medicoDao.insertRicetta(medico, id, farmaco);
            if (id2 != -1){
                sendMail(request, medico.getPazienti().get(id).getEmail());
                createPdf(request, id2);
                createXls(medicoDao.getRicette(request.getParameter("data"), medico.getCod_prov()), request.getParameter("data"), medico.getCod_prov());
                response.sendRedirect("restricted/paziente.jsp?id=" + id);
                utenti.updateEsame(medico.getPazienti().get(id).getCf());
            }
            else  response.sendRedirect("restricted/paziente.jsp?id=" + id + "&err=5");
        }
        catch(DAOException | IOException | NumberFormatException e){
            response.sendRedirect("restricted/paziente.jsp?id=" + id + "&err=5");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (ClassNotFoundException | SQLException | InterruptedException ex) {
            Logger.getLogger(InsertRicetta.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (ClassNotFoundException | SQLException | InterruptedException ex) {
            Logger.getLogger(InsertRicetta.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
    
    
    /**
     * Metodo che manda una mail al paziente per notificarlo di una nuova ricetta
     * 
     * @param request
     * @param esito_visita 
     */
    private void sendMail(HttpServletRequest request, String email_paz){
        final String host = "smtp.gmail.com";
        final String port = "465";
        final String username = "servizio.sanitario19@gmail.com";
        final String password = "hhtikdibgtctrqfb";
        
        Properties props = System.getProperties();
        props.setProperty("mail.smtp.host", host);
        props.setProperty("mail.smtp.port", port);
        props.setProperty("mail.smtp.socketFactory.port", port);
        props.setProperty("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
        props.setProperty("mail.smtp.auth", "true");
        props.setProperty("mail.smtp.starttls.enable", "true");
        props.setProperty("mail.debug", "true");

        Session session = Session.getInstance(props, new Authenticator() {
            @Override
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(username, password);
            }
        });

        Message msg = new MimeMessage(session);
        try {
            msg.setFrom(new InternetAddress(username));
            //msg.setRecipients(Message.RecipientType.TO, InternetAddress.parse(email_paz, false));
            msg.setRecipients(Message.RecipientType.TO, InternetAddress.parse("pazienti.servizio.sanitario19@gmail.com", false));
            msg.setSubject("Hai un nuova ricetta [" + email_paz + "]");
            msg.setText("Visualizza la nuova ricetta su: Indirizzo web progetto");
            msg.setSentDate(new Date());
            Transport.send(msg);
        } catch (MessagingException me) {
            me.printStackTrace(System.err);
        }
    }    
    
    /**
     * Metodo che crea il pdf da poter stampare
     * 
     * @param request
     * @param esito_visita 
     */
    private void createPdf(HttpServletRequest request, int id) throws FileNotFoundException, MalformedURLException{
        
        String routePath = InsertRicetta.class.getClassLoader().getResource(File.separator).getPath();
        PdfWriter writer = new PdfWriter(routePath + File.separator + ".." + File.separator + ".." + File.separator + ".." + File.separator + ".." + File.separator + "src" + File.separator + "main" + File.separator + "webapp" + File.separator + "pdf_ricette" + File.separator + id + ".pdf");
        PdfDocument pdfDoc = new PdfDocument(writer);              
        pdfDoc.addNewPage();                  
        Document document = new Document(pdfDoc);  
        
        // Add paragraphs
        String para1 = "Ricetta del paziente: " + request.getParameter("paziente");                       
        Paragraph paragraph1 = new Paragraph(para1);                              
        document.add(paragraph1);       

        
        ImageData data = ImageDataFactory.create(routePath + File.separator + ".." + File.separator + ".." + File.separator + ".." + File.separator + ".." + File.separator + "src" + File.separator + "main" + File.separator + "webapp" + File.separator + "qrcode_ricette" + File.separator + id + ".jpg");
        Image image = new Image(data);  
        image.scaleToFit(PageSize.A4.getWidth(), PageSize.A4.getHeight());
        document.add(image);
  
        document.close();  
    }
    
    /**
     * Metodo che crea un file xls con tutte le ricette 
     * create nello stesso giorno di quella appena fatta
     * 
     * @param ricette 
     */
    private void createXls(List<Ricetta> ricette, String data, String cod_prov){
        //Metodo che mi restituisce la collezione di dati da inserire nel foglio excel. 
        //L'esempio scrive una singola colonna.
        String routePath = InsertRicetta.class.getClassLoader().getResource(File.separator).getPath();
        File file = new File(routePath + File.separator + ".." + File.separator + ".." + File.separator + ".." + File.separator + ".." + File.separator + "src" + File.separator + "main" + File.separator + "webapp" + File.separator + "xls_ricette" + File.separator + cod_prov + File.separator + data + ".xls");
        HSSFWorkbook wb = new HSSFWorkbook();
        try {
            printWorkbook(ricette, wb, file);
        } 
        catch (Exception ex) {
            ex.printStackTrace();
        }
    }
    
    protected void printWorkbook(List<Ricetta> ricette, Workbook workbook, File outputFile) throws Exception{
        Sheet sheet= null;
        try {
            sheet= workbook.getSheetAt(0);//Recupero il foglio in cui scrivere (il primo del workbook).
        } catch (Exception iae) {
            //Se il file Excel è nuovo, non esiste nessufoglio. Devo crearne uno.
            sheet= workbook.createSheet();
        }try {
            int row = 0;
            
            printCell("Ora", Color.YELLOW,row,0,sheet);
            printCell("Farmaco", Color.YELLOW,row,1,sheet);
            printCell("Medico", Color.YELLOW,row,2,sheet);
            printCell("Paziente", Color.YELLOW,row,3,sheet);
            row++;
            for (Ricetta ricetta : ricette) {
                printCell(ricetta.getOra(), Color.WHITE,row,0,sheet);
                printCell(ricetta.getFarmaco(), Color.WHITE,row,1,sheet);
                printCell(ricetta.getNomeMed() + " " + ricetta.getCognomeMed(), Color.WHITE,row,2,sheet);
                printCell(ricetta.getCf_paz(), Color.WHITE,row,3,sheet);
                row++;
            }
        } finally{
            final FileOutputStream fos;
            fos = new FileOutputStream(outputFile);
            workbook.write(fos);
            fos.close();
        }
    }
    
    private Cell printCell(Object ca, Color bgColor, int row, int col, Sheet sheet) {
        Cell cell= printCell(ca, row, col, sheet);
        if (cell instanceof XSSFCell) {
            XSSFCell xssfCell= (XSSFCell) cell;
            XSSFCellStyle style = (XSSFCellStyle) sheet.getWorkbook().createCellStyle();
            style.setFillForegroundColor(new XSSFColor(bgColor));
            style.setFillPattern(FillPatternType.SOLID_FOREGROUND);
            xssfCell.setCellStyle(style);
        } 
        else if(cell instanceof HSSFCell) {
            HSSFCell hssfCell= (HSSFCell) cell;
            CellStyle style = sheet.getWorkbook().createCellStyle();
            HSSFPalette palette = ((HSSFSheet) sheet).getWorkbook().getCustomPalette();
            HSSFColor color = palette.findSimilarColor((byte) bgColor.getRed(), (byte) bgColor.getGreen(), (byte) bgColor.getBlue());
            style.setFillForegroundColor(color.getIndex());
            style.setFillPattern(FillPatternType.SOLID_FOREGROUND);
            hssfCell.setCellStyle(style);
        }
        return cell;
    }
    
    private Cell printCell(Object ca, int row, int col, Sheet sheet) {
        Row _row= sheet.getRow(row);
        //Provo a vedere se il foglio contiene già una riga con indice "row".
        if (_row== null) {
            _row= sheet.createRow(row);
            //Se il foglio non contiene la riga, la creo al volo
        }
        final Cell cell= _row.createCell(col);
        CellStyle cellStyle= sheet.getWorkbook().createCellStyle();
        cellStyle.setVerticalAlignment(VerticalAlignment.TOP);
        if (ca  instanceof Date) {
            //Le date, in Excel, in realtà sono dei numeri che equivalgono al numero di millisecondi 
            //trascorsidal 01/01/1970 (mi sembra sia questa la data). 
            //Comunque basta passare un oggetto Date e si arrangia POI a convertirlo nel numero corretto. 
            //L'importante è ricordarsi di impostare la cella di tipo CELL_TYPE_NUMERIC 
            //e utilizzare createDataFormat() per creare il "renderer" della cella
            cell.setCellValue((Date) ca);
            cellStyle.setDataFormat(sheet.getWorkbook().createDataFormat().getFormat("dd-mmm-yyyy"));
        } 
        else if(ca  instanceof String) {
            //Qui l'unica attenzione va agli "a capo". Se presenti meglio impostare il Wrap 
            //della cella a "true". Altrimenti rimane tutto su una singola linea...
            cell.setCellType(CellType.STRING);
            cell.setCellValue((String) ca);
            if (((String) ca).contains("\n")) {
                cellStyle.setWrapText(true);
            }
        } 
        else if(ca  instanceof Number) {
            //I Number(compresi gli interi) devono essere convertiti in double.
            cell.setCellType(CellType.NUMERIC);
            cell.setCellValue(((Number) ca).doubleValue());
        } 
        else if(ca  instanceof Boolean) {
            cell.setCellType(CellType.NUMERIC);
            cell.setCellValue((Boolean) ca);
        }
        cell.setCellStyle(cellStyle);
        return cell;
    }

}
