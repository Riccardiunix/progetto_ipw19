package ipw19.servlets;

import commons.dao.exceptions.DAOException;
import commons.dao.exceptions.DAOFactoryException;
import commons.dao.factories.DAOFactory;
import ipw19.dao.MedicoDAO;
import ipw19.entities.Medico;
import ipw19.entities.Utenti;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Date;
import java.util.Properties;
import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.servlet.http.HttpSession;

/**
 *
 * @author orion
 */
@WebServlet(name="UpdateVisitaSpec", urlPatterns = {"/UpdateVisitaSpec"})
public class UpdateVisitaSpec extends HttpServlet {
    
    private MedicoDAO medicoDao;
    private Utenti utenti;
    
    @Override
    public void init() throws ServletException {
        DAOFactory daoFactory = (DAOFactory) super.getServletContext().getAttribute("daoFactory");
        if (daoFactory == null) throw new ServletException("Impossible to get dao factory for user storage system");
        try {
            medicoDao = daoFactory.getDAO(MedicoDAO.class);
            utenti = daoFactory.getUtenti();
        } catch (DAOFactoryException ex) {
            throw new ServletException("Impossible to get dao factory for user storage system", ex);
        }
    }

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)  throws ServletException, IOException {
        int id = Integer.parseInt(request.getParameter("id"));
            int id_visita = Integer.parseInt(request.getParameter("id_visita"));
            String esito = request.getParameter("ris_visita"); 
       try{
            HttpSession session = request.getSession();
            Medico medico = (Medico) session.getAttribute("medico");

            if (medicoDao.updateVisitaSpec(medico, id, id_visita, esito)) {
                response.sendRedirect("restricted/paziente.jsp?id=" + id);
                utenti.updateVisita(medico.getPazienti().get(id).getCf());
                sendMail(request, medico.getPazienti().get(id).getEmail());
            }
            else response.sendRedirect("restricted/paziente.jsp?id=" + id + "&err=11");
        }
        catch(DAOException | IOException | NumberFormatException e){
            response.sendRedirect("restricted/paziente.jsp?id=" + id + "&err=11");
        }
    }
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
    
    /**
     * Metodo che manda una mail al paziente per notificarlo che un esame è stato aggiornato
     * 
     * @param request
     * @param esito_visita 
     */
    private void sendMail(HttpServletRequest request, String email){
        final String host = "smtp.gmail.com";
        final String port = "465";
        final String username = "servizio.sanitario19@gmail.com";
        final String password = "hhtikdibgtctrqfb";
        Properties props = System.getProperties();

        props.setProperty("mail.smtp.host", host);
        props.setProperty("mail.smtp.port", port);
        props.setProperty("mail.smtp.socketFactory.port", port);
        props.setProperty("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
        props.setProperty("mail.smtp.auth", "true");
        props.setProperty("mail.smtp.starttls.enable", "true");
        props.setProperty("mail.debug", "true");

        Session session = Session.getInstance(props, new Authenticator() {
            @Override
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(username, password);
            }
        });

        Message msg = new MimeMessage(session);
        try {
            msg.setFrom(new InternetAddress(username));
            //msg.setRecipients(Message.RecipientType.TO, InternetAddress.parse(email, false));
            msg.setRecipients(Message.RecipientType.TO, InternetAddress.parse("pazienti.servizio.sanitario19@gmail.com", false));
            msg.setSubject("Una visita specialistica è stata aggiornata [" + email + "]");
            msg.setText("Visualizza l'esito della visita specialistica su: Indirizzo web progetto");
            msg.setSentDate(new Date());

            Transport.send(msg);
        } catch (MessagingException me) {
            me.printStackTrace(System.err);
        }
    }

}
