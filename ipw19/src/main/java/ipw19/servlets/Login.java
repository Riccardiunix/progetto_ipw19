package ipw19.servlets;

import commons.dao.exceptions.DAOException;
import commons.dao.exceptions.DAOFactoryException;
import commons.dao.factories.DAOFactory;
import ipw19.dao.CookieDAO;
import ipw19.dao.MedicoDAO;
import ipw19.dao.PazienteDAO;
import ipw19.dao.ServizioSanitarioProvincialeDAO;
import ipw19.entities.CookieIPW;
import ipw19.entities.Medico;
import ipw19.entities.Paziente;
import ipw19.entities.ServizioSanitarioProvinciale;
import ipw19.entities.Utenti;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.Cookie;

/**
 *
 * @author orion
 */
@WebServlet(name = "Login", urlPatterns = {"/Login"})
public class Login extends HttpServlet {
    
    private PazienteDAO pazienteDao;
    private MedicoDAO medicoDao;
    private ServizioSanitarioProvincialeDAO sspDao;
    private CookieDAO cookieDao;
    private Utenti utenti;
    
    @Override
    public void init() throws ServletException {
        DAOFactory daoFactory = (DAOFactory) super.getServletContext().getAttribute("daoFactory");
        if (daoFactory == null) throw new ServletException("Impossible to get dao factory for user storage system");
        try {
            pazienteDao = daoFactory.getDAO(PazienteDAO.class);
            medicoDao = daoFactory.getDAO(MedicoDAO.class);
            sspDao = daoFactory.getDAO(ServizioSanitarioProvincialeDAO.class);
            cookieDao = daoFactory.getDAO(CookieDAO.class);
            utenti = daoFactory.getUtenti();
        } catch (DAOFactoryException ex) {
            throw new ServletException("Impossible to get dao factory for user storage system" + ex, ex);
        }
    }

    @SuppressWarnings("empty-statement")
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, ClassNotFoundException, SQLException, NoSuchAlgorithmException, DAOException {       
        String ipw19id = null;
        Cookie[] cookies = request.getCookies();
        if ((cookies != null) && (cookies.length > 0)) {
            for (Cookie cookie : cookies) {
                if (cookie.getName().equals("ipw19id")) ipw19id = cookie.getValue();
            }
        }
        
        String contextPath = getServletContext().getContextPath();
        if (!contextPath.endsWith("/")) contextPath += "/";
        
        if (ipw19id == null){
            Paziente paziente = pazienteDao.getByEmailAndPassword(request.getParameter("email"), request.getParameter("password"));
            if (paziente == null){
                Medico medico = medicoDao.getByEmailAndPassword(request.getParameter("email"), request.getParameter("password"));
                if (medico == null) {
                    ServizioSanitarioProvinciale ssp = sspDao.getByEmailAndPassword(request.getParameter("email"), request.getParameter("password"));
                    if (ssp == null) response.sendRedirect(response.encodeRedirectURL(contextPath + "index.jsp?err=0"));
                    else{
                        if(request.getParameter("ricordami") != null) createCookie(ssp.getCod_prov(), 2, response);
                        utenti.addSSP(ssp);
                        request.getSession().setAttribute("ssp", ssp);
                        response.sendRedirect(response.encodeRedirectURL(contextPath + "restricted/homeSSP.jsp"));
                    }
                }
                else{
                    if(request.getParameter("ricordami") != null) createCookie(medico.getCf(), 1, response);
                    utenti.addMedico(medico);
                    request.getSession().setAttribute("medico", medico);
                    response.sendRedirect(response.encodeRedirectURL(contextPath + "restricted/homeMedico.jsp"));
                }
            }
            else{
                if(request.getParameter("ricordami") != null) createCookie(paziente.getCf(), 0, response);
                utenti.addPaziente(paziente);
                request.getSession().setAttribute("paziente", paziente);
                response.sendRedirect(response.encodeRedirectURL(contextPath + "restricted/homePaziente.jsp"));
            }
        }
        else{
            CookieIPW cookie = cookieDao.getByPrimaryKey(ipw19id);
            if (cookie != null){
                switch (cookie.getTipo()) {
                    case 0:
                        Paziente paziente = pazienteDao.getByPrimaryKey(cookie.getPk());
                        request.getSession().setAttribute("paziente", paziente);
                        response.sendRedirect(response.encodeRedirectURL(contextPath + "restricted/homePaziente.jsp"));
                        break;
                    case 1:
                        Medico medico = medicoDao.getByPrimaryKey(cookie.getPk());
                        request.getSession().setAttribute("medico", medico);
                        response.sendRedirect(response.encodeRedirectURL(contextPath + "restricted/homeMedico.jsp"));
                        break;
                    case 2:
                        ServizioSanitarioProvinciale ssp = sspDao.getByPrimaryKey(cookie.getPk());
                        request.getSession().setAttribute("ssp", ssp);
                        response.sendRedirect(response.encodeRedirectURL(contextPath + "restricted/homeSSP.jsp"));
                        break;
                    default:
                        response.sendRedirect(response.encodeRedirectURL(contextPath + "index.jsp?err=0"));
                        break;
                }
            }
            else response.sendRedirect(response.encodeRedirectURL(contextPath + cookie + "_" + ipw19id));
        }
    }

        /**
         * Handles the HTTP <code>GET</code> method.
         *
         * @param request servlet request
         * @param response servlet response
         * @throws ServletException if a servlet-specific error occurs
         * @throws IOException if an I/O error occurs
         */
        @Override
        protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
            try {
                processRequest(request, response);
            } catch (ClassNotFoundException | SQLException | NoSuchAlgorithmException | DAOException ex) {
                Logger.getLogger(Login.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        /**
         * Handles the HTTP <code>POST</code> method.
         *
         * @param request servlet request
         * @param response servlet response
         * @throws ServletException if a servlet-specific error occurs
         * @throws IOException if an I/O error occurs
         */
        @Override
        protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
            try {
                processRequest(request, response);
                
            } catch (ClassNotFoundException | SQLException | NoSuchAlgorithmException | DAOException ex) {
                Logger.getLogger(Login.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        /**
         * Returns a short description of the servlet.
         *
         * @return a String containing servlet description
         */
        @Override
        public String getServletInfo() {
            return "Questo servlet si occupa del Login di medici  e pazienti";
        }

        public void createCookie(String pk, int tipo, HttpServletResponse response){
            String ipw19id = Long.toHexString(Double.doubleToLongBits(Math.random()));
            while(!cookieDao.addCookie(ipw19id, pk, tipo)) ipw19id = Long.toHexString(Double.doubleToLongBits(Math.random()));;
            Cookie cookie = new Cookie("ipw19id", ipw19id);
            cookie.setMaxAge(60*60*24); 
            response.addCookie(cookie);
        }
}
