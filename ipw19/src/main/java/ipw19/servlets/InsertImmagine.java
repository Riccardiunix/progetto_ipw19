package ipw19.servlets;

import ipw19.entities.Paziente;
import java.io.*;
import java.util.*;
 
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
 
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

/**
 *
 * @author orion
 */
@WebServlet(name = "InsertImmagine", urlPatterns = {"/InsertImmagine"})
public class InsertImmagine extends HttpServlet {
    
    private boolean isMultipart;
    private String filePath;
    private final int maxFileSize = 50 * 1024;
    private final int maxMemSize = 4 * 1024;
    private File file ;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();
        Paziente paziente = (Paziente) session.getAttribute("paziente");
        
        isMultipart = ServletFileUpload.isMultipartContent(request);
        response.setContentType("text/html");

        if( !isMultipart ) response.sendRedirect("homePaziente.jsp");

        DiskFileItemFactory factory = new DiskFileItemFactory();
        factory.setSizeThreshold(maxMemSize);
        String routePath = ReportTicket.class.getClassLoader().getResource(File.separator).getPath();
        String filePath = routePath + File.separator + ".." + File.separator + ".." + File.separator + ".." + File.separator + ".." + File.separator + "src" + File.separator + "main" + File.separator + "webapp" + File.separator;
        factory.setRepository(new File(filePath + "data" + File.separator));

        // Create a new file upload handler
        ServletFileUpload upload = new ServletFileUpload(factory);
        upload.setSizeMax( maxFileSize );

        try {
            List fileItems = upload.parseRequest(request);
            Iterator i = fileItems.iterator();
            i.hasNext ();
            FileItem fi = (FileItem)i.next();
            if ( !fi.isFormField () ) {
                file = new File( filePath + "img_paz/" + paziente.getCf() + "/avataaars.png");
                fi.write(file);
                Thread.sleep(2000);
            }
            response.sendRedirect("restricted/homePaziente.jsp");
        } catch(Exception ex) {
            response.sendRedirect("restricted/homePaziente.jsp?err=4");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
