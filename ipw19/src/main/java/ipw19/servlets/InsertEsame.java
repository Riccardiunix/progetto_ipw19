package ipw19.servlets;

import commons.dao.exceptions.DAOException;
import commons.dao.exceptions.DAOFactoryException;
import commons.dao.factories.DAOFactory;
import ipw19.dao.MedicoDAO;
import ipw19.entities.Medico;
import ipw19.entities.Utenti;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author orion
 */
@WebServlet(name = "InsertEsame", urlPatterns = {"/InsertEsame"})
public class InsertEsame extends HttpServlet {
    
    private MedicoDAO medicoDao;
    private Utenti utenti;
    
    @Override
    public void init() throws ServletException {
        DAOFactory daoFactory = (DAOFactory) super.getServletContext().getAttribute("daoFactory");
        if (daoFactory == null) throw new ServletException("Impossible to get dao factory for user storage system");
        try {
            medicoDao = daoFactory.getDAO(MedicoDAO.class);
            utenti = daoFactory.getUtenti();
        } catch (DAOFactoryException ex) {
            throw new ServletException("Impossible to get dao factory for user storage system", ex);
        }
    }

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)  throws ServletException, IOException {
        int id = Integer.parseInt(request.getParameter("id"));
        int tipo_esame = Integer.parseInt(request.getParameter("tipo_esame"));
        String data_esame = request.getParameter("data_esame");
        try{
            HttpSession session = request.getSession();
            Medico medico = (Medico) session.getAttribute("medico");        

            if (medicoDao.insertEsame(medico, id, tipo_esame, data_esame)){
                utenti.updateEsame(medico.getPazienti().get(id).getCf());
                response.sendRedirect("restricted/paziente.jsp?id=" + id);
            }
            else response.sendRedirect("restricted/paziente.jsp?id=" + id + "&err=3");
        }
        catch(DAOException | IOException | NumberFormatException e){
            response.sendRedirect("restricted/paziente.jsp?id=" + id + "&err=3");
        }
    } 

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
