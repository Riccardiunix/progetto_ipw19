package ipw19.servlets;

import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfWriter;
import com.itextpdf.layout.Document;
import com.itextpdf.layout.element.Paragraph;
import ipw19.entities.Esame;
import ipw19.entities.VisiteSpec;
import ipw19.entities.Paziente;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author andry
 */
@WebServlet(name = "ReportTicket", urlPatterns = {"/ReportTicket"})
public class ReportTicket extends HttpServlet {
    
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     * @throws java.lang.InterruptedException
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, InterruptedException {
        HttpSession session = request.getSession();

        Paziente paziente = (Paziente) session.getAttribute("paziente");
        List<VisiteSpec> visiteSpec = paziente.getVisite_spec();
        List<Esame> esami = paziente.getEsami();
        
        createPdf(visiteSpec, esami, request);
        Thread.sleep(2000);
        response.sendRedirect("pdf_ticket/" + request.getParameter("cf_paz") + ".pdf");
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (InterruptedException ex) {
            Logger.getLogger(ReportTicket.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (InterruptedException ex) {
            Logger.getLogger(ReportTicket.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
    
    /**
     * Metodo che crea il pdf da poter stampare
     * 
     * @param request
     * @param esito_visita 
     */
    private void createPdf(List<VisiteSpec> visiteSpec, List<Esame> esami, HttpServletRequest request) throws FileNotFoundException{
        // Creating a PdfWriter, a PdfDocument and a Document   
        String routePath = ReportTicket.class.getClassLoader().getResource(File.separator).getPath();
        PdfWriter writer = new PdfWriter(routePath + File.separator + ".." + File.separator + ".." + File.separator + ".." + File.separator + ".." + File.separator + "src" + File.separator + "main" + File.separator + "webapp" + File.separator + "pdf_ticket" + File.separator + request.getParameter("cf_paz") + ".pdf");
        PdfDocument pdfDoc = new PdfDocument(writer);   
        pdfDoc.addNewPage();                  
        // Add paragraphs
        try (Document document = new Document(pdfDoc)) {
            // Add paragraphs
            if(visiteSpec.isEmpty() & esami.isEmpty()) document.add(new Paragraph("Il paziente non ha nessun esame e visita specialistica"));
            else if(visiteSpec.isEmpty()){
                String para = "";
                for (Esame esame:esami){
                    if (esame.getTicket() == 1){        //Ticket pagato
                        para += "- Data: " + esame.getData_esame()+ "; Nome esame: " + esame.getNome_esame() + "; Risultato: " + esame.getRisultato() + "; Ticket: 11 euro\n";
                    }
                }
                
                if (para.length() > 0){
                    document.add(new Paragraph("Paziente: " + request.getParameter("paziente")));
                    document.add(new Paragraph("\n\nEsami pagati:\n" + para));
                }
                else document.add(new Paragraph("Nessun esame pagato\n"));
                
                document.add(new Paragraph("Il paziente non ha nessuna visita specialistica"));
            }
            else if(esami.isEmpty()){
                document.add(new Paragraph("Il paziente non ha nessun esame\n"));
                
                String para = "";
                for (VisiteSpec visitaSpec:visiteSpec){
                    if (visitaSpec.getTicket() == 1){        //Ticket pagato
                        para += "- Data: " + visitaSpec.getData_visita()+ "; Nome visita: " + visitaSpec.getNome_visita() + "; Risultato: " + visitaSpec.getRisultato() + "; Ticket: 50 euro\n";
                    }
                }
                
                if (para.length() > 0){
                    document.add(new Paragraph("Paziente: " + request.getParameter("paziente")));
                    document.add(new Paragraph("\n\nVisite pagate:\n" + para));
                }
                else document.add(new Paragraph("Nessuna vista pagato"));
            }
            else{
                String para = "";
                for (Esame esame:esami){
                    if (esame.getTicket() == 1){        //Ticket pagato
                        para += "- Data: " + esame.getData_esame()+ "; Nome esame: " + esame.getNome_esame() + "; Risultato: " + esame.getRisultato() + "; Ticket: 11 euro\n";
                    }
                }
                
                if (para.length() > 0){
                    document.add(new Paragraph("Paziente: " + request.getParameter("paziente")));
                    document.add(new Paragraph("\n\nEsami pagati:\n" + para));
                }
                else document.add(new Paragraph("Nessun esame pagato\n"));
                
                para = "";
                for (VisiteSpec visitaSpec:visiteSpec){
                    if (visitaSpec.getTicket() == 1){        //Ticket pagato
                        para += "- Data: " + visitaSpec.getData_visita()+ "; Nome visita: " + visitaSpec.getNome_visita() + "; Risultato: " + visitaSpec.getRisultato() + "; Ticket: 50 euro\n";
                    }
                }
                
                if (para.length() > 0){
                    document.add(new Paragraph("\n\nVisite pagate:\n" + para));
                }
                else document.add(new Paragraph("Nessuna vista pagato\n"));
            }
        }
        
    }

}
