package ipw19.entities;

import java.util.List;

/**
 *
 * @author orion
 */
public class ServizioSanitarioProvinciale {
    String email;
    String password;
    String salt;
    String cod_prov;
    List<Esame> esami;
    List<Esame> esami_da_fare;
    private List<String> date_ricette;

    public List<Esame> getEsami_da_fare() {
        return esami_da_fare;
    }

    public void setEsami_da_fare(List<Esame> esami_da_fare) {
        this.esami_da_fare = esami_da_fare;
    }
    
    public String getSalt() {
        return salt;
    }

    public void setSalt(String salt) {
        this.salt = salt;
    }
    
    public List<Esame> getEsami() {
        return esami;
    }

    public void setEsami(List<Esame> esami) {
        this.esami = esami;
    }
    
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getCod_prov() {
        return cod_prov;
    }

    public void setCod_prov(String cod_prov) {
        this.cod_prov = cod_prov;
    }
    
    public List<String> getDate_ricette() {
        return date_ricette;
    }

    public void setDate_ricette(List<String> date_ricette) {
        this.date_ricette = date_ricette;
    }
    
}
