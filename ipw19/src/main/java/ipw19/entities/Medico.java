/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ipw19.entities;

import java.util.Date;
import java.util.List;

/**
 *
 * @author orion
 */
public class Medico {
    private String cf;
    private String nome;
    private String cognome;
    private String email;
    private String password;
    private String salt;
    private Date data_nascita;
    private String sesso;
    private int tipo_med;
    private String cod_prov;
    private String citta;
    private String spec;
    private String id_spec;
    private List<Paziente> pazienti;
    private List<List<String>> tipi_esami;
    private List<List<String>> tipi_visite;
    private List<List<List<String>>> visite_da_fare;

    public String getId_spec() {
        return id_spec;
    }

    public void setId_spec(String id_spec) {
        this.id_spec = id_spec;
    }

    public List<List<List<String>>> getVisite_da_fare() {
        return visite_da_fare;
    }

    public void setVisite_da_fare(List<List<List<String>>> visite_da_fare) {
        this.visite_da_fare = visite_da_fare;
    }
    
    public List getTipi_esami() {
        return tipi_esami;
    }

    public void setTipi_esami(List tipi_esami) {
        this.tipi_esami = tipi_esami;
    }
    
    public List getTipi_visite() {
        return tipi_visite;
    }

    public void setTipi_visite(List tipi_visite) {
        this.tipi_visite = tipi_visite;
    }
    
    
    public String getSpec() {
        return spec;
    }

    public void setSpec(String spec) {
        this.spec = spec;
    }

    public String getCod_prov() {
        return cod_prov;
    }

    public void setCod_prov(String cod_prov) {
        this.cod_prov = cod_prov;
    }

    public String getCitta() {
        return citta;
    }

    public void setCitta(String citta) {
        this.citta = citta;
    }
    
    public List<Paziente> getPazienti() {
        return pazienti;
    }

    public void setPazienti(List<Paziente> pazienti) {
        this.pazienti = pazienti;
    }

    public String getCf() {
        return cf;
    }

    public void setCf(String cf) {
        this.cf = cf;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCognome() {
        return cognome;
    }

    public void setCognome(String cognome) {
        this.cognome = cognome;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getSalt() {
        return salt;
    }

    public void setSalt(String salt) {
        this.salt = salt;
    }

    public Date getData_nascita() {
        return data_nascita;
    }

    public void setData_nascita(Date data_nascita) {
        this.data_nascita = data_nascita;
    }

    public String getSesso() {
        return sesso;
    }

    public void setSesso(String sesso) {
        this.sesso = sesso;
    }

    public int getTipo_med() {
        return tipo_med;
    }

    public void setTipo_med(int tipo_med) {
        this.tipo_med = tipo_med;
    }
    
}
