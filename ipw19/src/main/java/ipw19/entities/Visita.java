/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ipw19.entities;

import java.util.Date;

/**
 *
 * @author orion
 */
public class Visita {
    private String cf_med;
    private String cf_paz;
    private String nomeMed;
    private String cognomeMed;
    private Date data_visita;
    private String risultato;

    public String getNomeMed() {
        return nomeMed;
    }

    public void setNomeMed(String nomeMed) {
        this.nomeMed = nomeMed;
    }

    public String getCognomeMed() {
        return cognomeMed;
    }

    public void setCognomeMed(String cognomeMed) {
        this.cognomeMed = cognomeMed;
    }

    public String getCf_med() {
        return cf_med;
    }

    public void setCf_med(String cf_med) {
        this.cf_med = cf_med;
    }

    public String getCf_paz() {
        return cf_paz;
    }

    public void setCf_paz(String cf_paz) {
        this.cf_paz = cf_paz;
    }

    public Date getData_visita() {
        return data_visita;
    }

    public void setData_visita(Date data_visita) {
        this.data_visita = data_visita;
    }

    public String getRisultato() {
        return risultato;
    }

    public void setRisultato(String risultato) {
        this.risultato = risultato;
    }

}
