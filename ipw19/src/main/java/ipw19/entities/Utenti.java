/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ipw19.entities;

import commons.dao.exceptions.DAOException;
import ipw19.dao.jdbc.JDBCMedicoDAO;
import ipw19.dao.jdbc.JDBCPazienteDAO;
import ipw19.dao.jdbc.JDBCServizioSanitarioProvincialeDAO;
import java.sql.Connection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

/**
 *
 * @author orion
 */
public class Utenti{
    private HashMap<String, Paziente> pazienti;
    private HashMap<String, ServizioSanitarioProvinciale> ssps;
    private HashMap<String, Medico> medici;
    private Connection CON;

    public Utenti(Connection CON) {
        pazienti = new HashMap<String, Paziente>();
        ssps = new HashMap<String, ServizioSanitarioProvinciale>();
        medici = new HashMap<String, Medico>();
        this.CON = CON;
    }
    
    public void addMedico(Medico medico){
        medici.put(medico.getCf(), medico);
    }
    
    public void addPaziente(Paziente paziente){
        pazienti.put(paziente.getCf(), paziente);
    }
    
    public void addSSP(ServizioSanitarioProvinciale ssp){
        ssps.put(ssp.cod_prov, ssp);
    }
    
    public void removeMedico(String cf_med){
        medici.remove(cf_med);
    }
    
    public void removePaziente(String cf_paz){
        pazienti.remove(cf_paz);
    }
    
    public void removeSSP(String cod_prov){
        ssps.remove(cod_prov);
    }
    
    public void updateVisita(String cf_paz) throws DAOException{
        if (pazienti.get(cf_paz) != null) new JDBCPazienteDAO(CON).update((Paziente) pazienti.get(cf_paz));
        
        Set set = medici.entrySet();
        Iterator iterator = set.iterator();
        while(iterator.hasNext()) {
           Map.Entry mentry = (Map.Entry)iterator.next();
           new JDBCMedicoDAO(CON).update((Medico) mentry.getValue());
        }
    }
    
    public void updateEsame(String cf_paz) throws DAOException{
        updateVisita(cf_paz);
        
        Set set = ssps.entrySet();
        Iterator iterator = set.iterator();
        while(iterator.hasNext()) {
           Map.Entry mentry = (Map.Entry)iterator.next();
           new JDBCServizioSanitarioProvincialeDAO(CON).update((ServizioSanitarioProvinciale) mentry.getValue());
        }
    }
    
    public void updateRicetta(String cf_paz) throws DAOException{
        updateEsame(cf_paz);
    }
    
    public void update() throws DAOException{
        Set set = pazienti.entrySet();
        Iterator iterator = set.iterator();
        while(iterator.hasNext()) {
           Map.Entry mentry = (Map.Entry)iterator.next();
           new JDBCPazienteDAO(CON).update((Paziente) mentry.getValue());
        }
        
        set = medici.entrySet();
        iterator = set.iterator();
        while(iterator.hasNext()) {
           Map.Entry mentry = (Map.Entry)iterator.next();
           new JDBCMedicoDAO(CON).update((Medico) mentry.getValue());
        }
        
        set = ssps.entrySet();
        iterator = set.iterator();
        while(iterator.hasNext()) {
           Map.Entry mentry = (Map.Entry)iterator.next();
           new JDBCServizioSanitarioProvincialeDAO(CON).update((ServizioSanitarioProvinciale) mentry.getValue());
        }
    }
}
