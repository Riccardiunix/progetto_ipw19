/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ipw19.entities;

import java.util.Date;
import java.util.List;

/**
 *
 * @author orion
 */
public class Paziente {
    private String cf;
    private String nome;
    private String cognome;
    private Date data_nascita;
    private String luogo_nascita;
    private String sesso;
    private String cf_med;
    private String nome_med;
    private String cognome_med;
    private String cod_prov;
    private String email;
    private String password;
    private String salt;
    private List<Visita> visite;
    private List<VisiteSpec> visite_spec;
    private List<Esame> esami;
    private String ultimaVisita;
    private String ultimaRicetta;
    
    public List<Esame> getEsami() {
        return esami;
    }

    public void setEsami(List<Esame> esami) {
        this.esami = esami;
    }
    private List<Ricetta> ricette;
    private List<List<String>> mediciDisp;
    
    public String getCf_med() {
        return cf_med;
    }

    public void setCf_med(String cf_med) {
        this.cf_med = cf_med;
    }

    public List<List<String>> getMediciDisp() {
        return mediciDisp;
    }

    public void setMediciDisp(List<List<String>> mediciDisp) {
        this.mediciDisp = mediciDisp;
    }

    public String getNome_med() {
        return nome_med;
    }

    public void setNome_med(String nome_med) {
        this.nome_med = nome_med;
    }

    public String getCognome_med() {
        return cognome_med;
    }

    public void setCognome_med(String cognome_med) {
        this.cognome_med = cognome_med;
    }
    
    public List<Ricetta> getRicette() {
        return ricette;
    }

    public void setRicette(List<Ricetta> ricette) {
        this.ricette = ricette;
    }

    public String getCf() {
        return cf;
    }
    
    public void setCf(String cf) {
        this.cf = cf;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCognome() {
        return cognome;
    }

    public void setCognome(String cognome) {
        this.cognome = cognome;
    }

    public Date getData_nascita() {
        return data_nascita;
    }

    public void setData_nascita(Date data_nascita) {
        this.data_nascita = data_nascita;
    }

    public String getLuogo_nascita() {
        return luogo_nascita;
    }

    public void setLuogo_nascita(String luogo_nascita) {
        this.luogo_nascita = luogo_nascita;
    }

    public String getSesso() {
        return sesso;
    }

    public void setSesso(String sesso) {
        this.sesso = sesso;
    }

    public String getCod_prov() {
        return cod_prov;
    }

    public void setCod_prov(String cod_prov) {
        this.cod_prov = cod_prov;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getSalt() {
        return salt;
    }

    public void setSalt(String salt) {
        this.salt = salt;
    }
    
    public void setVisite(List<Visita> visite){
        this.visite = visite;
    }
    
    public List<Visita> getVisite(){
        return visite;
    }

    public List<VisiteSpec> getVisite_spec() {
        return visite_spec;
    }

    public void setVisite_spec(List<VisiteSpec> visite_spec) {
        this.visite_spec = visite_spec;
    }

    public String getUltimaVisita() {
        return ultimaVisita;
    }

    public void setUltimaVisita(String ultimaVisita) {
        this.ultimaVisita = ultimaVisita;
    }

    public String getUltimaRicetta() {
        return ultimaRicetta;
    }

    public void setUltimaRicetta(String ultimaRicetta) {
        this.ultimaRicetta = ultimaRicetta;
    }
}