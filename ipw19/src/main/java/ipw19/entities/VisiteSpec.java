/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ipw19.entities;

import java.util.Date;

/**
 *
 * @author orion
 */
public class VisiteSpec {
    private String cf_med;
    private String cf_paz;
    private String nomeMed;
    private String cognomeMed;
    private String cf_med_visita;
    private String nomeMed_visita;
    private String cognomeMed_visita;
    private Date data_visita;
    private int tipo_visita;
    private String nome_visita;
    private String risultato;
    private int ticket;

    public String getCf_med_visita() {
        return cf_med_visita;
    }

    public void setCf_med_visita(String cf_med_visita) {
        this.cf_med_visita = cf_med_visita;
    }

    public String getNomeMed_visita() {
        return nomeMed_visita;
    }

    public void setNomeMed_visita(String nomeMed_visita) {
        this.nomeMed_visita = nomeMed_visita;
    }

    public String getCognomeMed_visita() {
        return cognomeMed_visita;
    }

    public void setCognomeMed_visita(String cognomeMed_visita) {
        this.cognomeMed_visita = cognomeMed_visita;
    }
    
    public String getCf_med() {
        return cf_med;
    }

    public void setCf_med(String cf_med) {
        this.cf_med = cf_med;
    }

    public String getCf_paz() {
        return cf_paz;
    }

    public void setCf_paz(String cf_paz) {
        this.cf_paz = cf_paz;
    }

    public String getNomeMed() {
        return nomeMed;
    }

    public void setNomeMed(String nomeMed) {
        this.nomeMed = nomeMed;
    }

    public String getCognomeMed() {
        return cognomeMed;
    }

    public void setCognomeMed(String cognomeMed) {
        this.cognomeMed = cognomeMed;
    }

    public Date getData_visita() {
        return data_visita;
    }

    public void setData_visita(Date data_visita) {
        this.data_visita = data_visita;
    }

    public int getTipo_visita() {
        return tipo_visita;
    }

    public void setTipo_visita(int tipo_visita) {
        this.tipo_visita = tipo_visita;
    }

    public String getNome_visita() {
        return nome_visita;
    }

    public void setNome_visita(String nome_visita) {
        this.nome_visita = nome_visita;
    }

    public String getRisultato() {
        return risultato;
    }

    public void setRisultato(String risultato) {
        this.risultato = risultato;
    }

    public int getTicket() {
        return ticket;
    }

    public void setTicket(int ticket) {
        this.ticket = ticket;
    }
    
    
}
