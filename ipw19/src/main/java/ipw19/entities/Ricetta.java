package ipw19.entities;

import java.util.Date;

/**
 *
 * @author orion
 */
public class Ricetta {
    private int id;
    private String cf_med;
    private String cf_paz;
    private Date data_presc;
    private String ora;
    private String farmaco;
    private String nomeMed;
    private String cognomeMed;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCf_med() {
        return cf_med;
    }

    public void setCf_med(String cf_med) {
        this.cf_med = cf_med;
    }

    public String getCf_paz() {
        return cf_paz;
    }

    public void setCf_paz(String cf_paz) {
        this.cf_paz = cf_paz;
    }

    public Date getData_presc() {
        return data_presc;
    }

    public void setData_presc(Date data_presc) {
        this.data_presc = data_presc;
    }
    
    public String getOra() {
        return ora;
    }

    public void setOra(String ora) {
        this.ora = ora;
    }

    public String getFarmaco() {
        return farmaco;
    }

    public void setFarmaco(String farmaco) {
        this.farmaco = farmaco;
    }

    public String getNomeMed() {
        return nomeMed;
    }

    public void setNomeMed(String nomeMed) {
        this.nomeMed = nomeMed;
    }

    public String getCognomeMed() {
        return cognomeMed;
    }

    public void setCognomeMed(String cognomeMed) {
        this.cognomeMed = cognomeMed;
    }
}
