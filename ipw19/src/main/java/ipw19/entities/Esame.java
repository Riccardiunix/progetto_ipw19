/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ipw19.entities;

import java.util.Date;

/**
 *
 * @author orion
 */
public class Esame {
    private String cf_med;
    private String cf_paz;
    private String nomeMed;
    private String cognomeMed;
    private String nomePaz;
    private String cognomePaz;
    private Date data_esame;
    private int tipo_esame;
    private String nome_esame;
    private String risultato;
    private String email;
    private int ticket;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
    
    public String getNomePaz() {
        return nomePaz;
    }

    public void setNomePaz(String nomePaz) {
        this.nomePaz = nomePaz;
    }

    public String getCognomePaz() {
        return cognomePaz;
    }

    public void setCognomePaz(String cognomePaz) {
        this.cognomePaz = cognomePaz;
    }
    
    public String getCf_med() {
        return cf_med;
    }

    public void setCf_med(String cf_med) {
        this.cf_med = cf_med;
    }

    public String getCf_paz() {
        return cf_paz;
    }

    public void setCf_paz(String cf_paz) {
        this.cf_paz = cf_paz;
    }

    public String getNomeMed() {
        return nomeMed;
    }

    public void setNomeMed(String nomeMed) {
        this.nomeMed = nomeMed;
    }

    public String getCognomeMed() {
        return cognomeMed;
    }

    public void setCognomeMed(String cognomeMed) {
        this.cognomeMed = cognomeMed;
    }

    public Date getData_esame() {
        return data_esame;
    }

    public void setData_esame(Date data_esame) {
        this.data_esame = data_esame;
    }

    public int getTipo_esame() {
        return tipo_esame;
    }

    public void setTipo_esame(int tipo_esame) {
        this.tipo_esame = tipo_esame;
    }

    public String getNome_esame() {
        return nome_esame;
    }

    public void setNome_esame(String nome_esame) {
        this.nome_esame = nome_esame;
    }

    public String getRisultato() {
        return risultato;
    }

    public void setRisultato(String risultato) {
        this.risultato = risultato;
    }

    public int getTicket() {
        return ticket;
    }

    public void setTicket(int ticket) {
        this.ticket = ticket;
    }
    
    
}
