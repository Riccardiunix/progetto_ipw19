/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ipw19.entities;

/**
 *
 * @author orion
 */
public class CookieIPW {
    String ipw19id;
    String pk;
    int tipo;

    public String getIpw19id() {
        return ipw19id;
    }

    public void setIpw19id(String ipw19id) {
        this.ipw19id = ipw19id;
    }

    public String getPk() {
        return pk;
    }

    public void setPk(String pk) {
        this.pk = pk;
    }

    public int getTipo() {
        return tipo;
    }

    public void setTipo(int tipo) {
        this.tipo = tipo;
    }
    
}
