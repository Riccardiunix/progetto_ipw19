package ipw19.dao.jdbc;

import commons.dao.exceptions.DAOException;
import commons.dao.jdbc.JDBCDAO;
import ipw19.dao.ServizioSanitarioProvincialeDAO;
import ipw19.entities.Esame;
import ipw19.entities.ServizioSanitarioProvinciale;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author orion
 */
public class JDBCServizioSanitarioProvincialeDAO extends JDBCDAO<ServizioSanitarioProvinciale, String> implements ServizioSanitarioProvincialeDAO{
    
    public JDBCServizioSanitarioProvincialeDAO(Connection con) {
        super(con);
    }
    
    public String generateHash(String passalt) throws NoSuchAlgorithmException {
        MessageDigest digest = MessageDigest.getInstance("MD5");
        digest.update(passalt.getBytes(), 0, passalt.length());
        return new BigInteger(1, digest.digest()).toString(16);
    }

    @Override
    public Long getCount() throws DAOException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public ServizioSanitarioProvinciale getByPrimaryKey(String primaryKey) throws DAOException {
        if (primaryKey == null)  throw new DAOException("primaryKey is null");
        try (PreparedStatement stm = CON.prepareStatement("SELECT * FROM ssp WHERE cod_prov = ?")) {
            stm.setString(1, primaryKey);
            try (ResultSet rs = stm.executeQuery()) {
                if (rs.next()){
                    ServizioSanitarioProvinciale ssp = new ServizioSanitarioProvinciale();
                    ssp.setCod_prov(rs.getString("cod_prov"));                    
                    ssp.setEmail(rs.getString("email"));
                    ssp.setPassword(rs.getString("password"));
                    ssp.setSalt(rs.getString("salt"));
                    
                    try (PreparedStatement esameStm = CON.prepareStatement("SELECT * FROM medici, medici_base, esame, tipo_esame, pazienti WHERE medici.cf = medici_base.cf AND medici_base.cod_prov = ? AND medici.cf = esame.cf_med AND esame.tipo_esame=tipo_esame.id AND esame.cf_paz = pazienti.cf ORDER BY data_esame DESC")) {
                        esameStm.setString(1, ssp.getCod_prov());
                        ResultSet rs_esame = esameStm.executeQuery();
                        List<Esame> esami = new ArrayList<>();
                        List<Esame> esami_da_fare = new ArrayList<>();
                        while(rs_esame.next()){
                            Esame esame = new Esame();
                            esame.setCf_med(rs_esame.getString("cf_med"));
                            esame.setNomeMed(rs_esame.getString("medici.nome"));
                            esame.setCognomeMed(rs_esame.getString("medici.cognome"));
                            esame.setCf_paz(rs_esame.getString("cf_paz"));
                            esame.setNomePaz(rs_esame.getString("pazienti.nome"));
                            esame.setCognomePaz(rs_esame.getString("pazienti.cognome"));
                            esame.setData_esame(rs_esame.getDate("data_esame"));
                            esame.setTipo_esame(rs_esame.getInt("tipo_esame"));
                            esame.setNome_esame(rs_esame.getString("nome_esame"));
                            esame.setRisultato(rs_esame.getString("risultato"));
                            esame.setTicket(rs_esame.getInt("ticket"));
                            esame.setEmail(rs_esame.getString("pazienti.email"));
                            
                            if (esame.getRisultato() == null) esami_da_fare.add(esame);
                            esami.add(esame);
                        }
                        ssp.setEsami(esami);
                        ssp.setEsami_da_fare(esami_da_fare);
                    }
                    
                    try (PreparedStatement ricetteStm = CON.prepareStatement("SELECT data_presc FROM ricette, pazienti WHERE pazienti.cf = ricette.cf_paz AND pazienti.cod_prov = ? GROUP BY data_presc ORDER BY data_presc DESC")) {
                        ricetteStm.setString(1, ssp.getCod_prov());
                        ResultSet rs_ricette = ricetteStm.executeQuery();
                        List<String> date_ricette = new ArrayList<>();
                        while(rs_ricette.next()) date_ricette.add(rs_ricette.getString("data_presc"));
                        ssp.setDate_ricette(date_ricette);
                    }
                    return ssp;
                }
            }
        } catch (SQLException ex) {
            Logger.getLogger(JDBCServizioSanitarioProvincialeDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    @Override
    public List<ServizioSanitarioProvinciale> getAll() throws DAOException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public ServizioSanitarioProvinciale getByEmailAndPassword(String email, String password) throws DAOException {
        if ((email == null) || (password == null)) throw new DAOException("Email and password are mandatory fields", new NullPointerException("email or password are null"));

        try (PreparedStatement stm = CON.prepareStatement("SELECT cod_prov, password, salt FROM ssp WHERE email = ?")) {
            stm.setString(1, email);
            try (ResultSet rs = stm.executeQuery()) {
                if (rs.next()){
                    while (!rs.getString("password").equals(generateHash(password + rs.getString("salt")))) {
                        if (!rs.next()) return null;
                    }
                    return getByPrimaryKey(rs.getString("cod_prov"));
                }
                return null;
            } catch (NoSuchAlgorithmException ex) {
                 Logger.getLogger(JDBCPazienteDAO.class.getName()).log(Level.SEVERE, null, ex);
             }
        } catch (SQLException ex) {
            throw new DAOException("Impossible to get the list of users", ex);
        }
        return null;
    }

    @Override
    public ServizioSanitarioProvinciale getByEmailAndHash(String email, String password) throws DAOException {
        if ((email == null) || (password == null)) throw new DAOException("Email and password are mandatory fields", new NullPointerException("email or password are null"));

        try (PreparedStatement stm = CON.prepareStatement("SELECT cod_prov FROM ssp WHERE email = ? AND password = ?")) {
            stm.setString(1, email);
            stm.setString(2, password);
            ResultSet rs = stm.executeQuery();
            if (rs.next()){
                return getByPrimaryKey(rs.getString("cod_prov"));
            }
            return null;
        } catch (SQLException ex) {
            throw new DAOException("Impossible to get the list of users", ex);
        }
    }

    @Override
     public boolean updateEsame(ServizioSanitarioProvinciale ssp, int id_esame, String esito) throws DAOException {
         if ((ssp == null) || (id_esame == -1) || (esito.length() == 0)) return false;
        
        try(PreparedStatement stm = CON.prepareStatement("UPDATE esame SET risultato = ? WHERE cf_med = ? AND  cf_paz = ? AND data_esame = ? AND tipo_esame = ?")){
            stm.setString(1, esito);
            stm.setString(2, ssp.getEsami_da_fare().get(id_esame).getCf_med());
            stm.setString(3, ssp.getEsami_da_fare().get(id_esame).getCf_paz());
            stm.setString(4, ssp.getEsami_da_fare().get(id_esame).getData_esame().toString());
            stm.setInt(5, ssp.getEsami_da_fare().get(id_esame).getTipo_esame());
            stm.executeUpdate();
            
            //update(ssp);
            return true;
        } catch (SQLException ex) {
            throw new DAOException("Impossible to update ESAME", ex);
        }
    }

    @Override
    public boolean forgotPassword(String email, String password) throws DAOException, NoSuchAlgorithmException {
        try (PreparedStatement stm = CON.prepareStatement("SELECT cod_prov, salt FROM ssp WHERE email = ?")) {
            stm.setString(1, email);
            try (ResultSet rs = stm.executeQuery()) {
                if (rs.next()){
                    try(PreparedStatement stm1 = CON.prepareStatement("UPDATE ssp SET password = ? WHERE cod_prov = ?")){
                        stm1.setString(1, generateHash(password + rs.getString("salt")));
                        stm1.setString(2, rs.getString("cod_prov"));
                        stm1.executeUpdate();
                        return true;
                    }
                }
            }
        } catch (SQLException ex) {
            Logger.getLogger(JDBCMedicoDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }

    @Override
    public void update(ServizioSanitarioProvinciale ssp) throws DAOException {
        if (ssp == null)  throw new DAOException("SSP is null");
        try (PreparedStatement stm = CON.prepareStatement("SELECT * FROM ssp WHERE cod_prov = ?")) {
            stm.setString(1, ssp.getCod_prov());
            try (ResultSet rs = stm.executeQuery()) {
                if (rs.next()){
                    ssp.setPassword(rs.getString("password"));
                    
                    try (PreparedStatement esameStm = CON.prepareStatement("SELECT * FROM medici, medici_base, esame, tipo_esame, pazienti WHERE medici.cf = medici_base.cf AND medici_base.cod_prov = ? AND medici.cf = esame.cf_med AND esame.tipo_esame=tipo_esame.id AND esame.cf_paz = pazienti.cf ORDER BY data_esame DESC")) {
                        esameStm.setString(1, ssp.getCod_prov());
                        ResultSet rs_esame = esameStm.executeQuery();
                        List<Esame> esami = new ArrayList<>();
                        List<Esame> esami_da_fare = new ArrayList<>();
                        while(rs_esame.next()){
                            Esame esame = new Esame();
                            esame.setCf_med(rs_esame.getString("cf_med"));
                            esame.setNomeMed(rs_esame.getString("medici.nome"));
                            esame.setCognomeMed(rs_esame.getString("medici.cognome"));
                            esame.setCf_paz(rs_esame.getString("cf_paz"));
                            esame.setNomePaz(rs_esame.getString("pazienti.nome"));
                            esame.setCognomePaz(rs_esame.getString("pazienti.cognome"));
                            esame.setData_esame(rs_esame.getDate("data_esame"));
                            esame.setTipo_esame(rs_esame.getInt("tipo_esame"));
                            esame.setNome_esame(rs_esame.getString("nome_esame"));
                            esame.setRisultato(rs_esame.getString("risultato"));
                            esame.setTicket(rs_esame.getInt("ticket"));
                            esame.setEmail(rs_esame.getString("pazienti.email"));
                            
                            if (rs_esame.getString("risultato") == null)  esami_da_fare.add(esame);
                            esami.add(esame);
                        }
                        ssp.setEsami(esami);
                        ssp.setEsami_da_fare(esami_da_fare);
                    }
                    
                    try (PreparedStatement ricetteStm = CON.prepareStatement("SELECT data_presc FROM ricette, pazienti WHERE pazienti.cf = ricette.cf_paz AND pazienti.cod_prov = ? GROUP BY data_presc ORDER BY data_presc DESC")) {
                        ricetteStm.setString(1, ssp.getCod_prov());
                        ResultSet rs_ricette = ricetteStm.executeQuery();
                        List<String> date_ricette = new ArrayList<>();
                        while(rs_ricette.next()){
                            date_ricette.add(rs_ricette.getString("data_presc"));
                        }
                        ssp.setDate_ricette(date_ricette);
                    }
                }
            }
        } catch (SQLException ex) {
            Logger.getLogger(JDBCServizioSanitarioProvincialeDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
}
