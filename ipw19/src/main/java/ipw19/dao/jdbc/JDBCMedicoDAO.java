package ipw19.dao.jdbc;

import commons.dao.exceptions.DAOException;
import commons.dao.jdbc.JDBCDAO;
import ipw19.dao.MedicoDAO;
import ipw19.entities.Medico;
import ipw19.entities.Paziente;
import ipw19.entities.Ricetta;
import net.glxn.qrgen.QRCode;
import net.glxn.qrgen.image.ImageType;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author orion
 */
public class JDBCMedicoDAO extends JDBCDAO<Medico, String> implements MedicoDAO {
    
    public JDBCMedicoDAO(Connection con) {
        super(con);
    }
    
    public String generateHash(String passalt) throws NoSuchAlgorithmException {
        MessageDigest digest = MessageDigest.getInstance("MD5");
        digest.update(passalt.getBytes(), 0, passalt.length());
        return new BigInteger(1, digest.digest()).toString(16);
    }
    
    @Override
    public Long getCount() throws DAOException {
        try (Statement stmt = CON.createStatement()) {
            ResultSet counter = stmt.executeQuery("SELECT COUNT(*) FROM medici");
            if (counter.next()) return counter.getLong(1);
        }
        catch (SQLException ex) { throw new DAOException("Impossible to count MEDICI", ex);}
        return 0L;
    }

    @Override
    public Medico getByPrimaryKey(String primaryKey) throws DAOException {
        if (primaryKey == null)  throw new DAOException("MEDICO primaryKey is null");
        try (PreparedStatement stm = CON.prepareStatement("SELECT * FROM medici WHERE medici.cf = ?")) {
            stm.setString(1, primaryKey);
            try (ResultSet rs = stm.executeQuery()) {
                if (rs.next()){
                    Medico medico = new Medico();
                    medico.setCf(rs.getString("cf"));
                    medico.setNome(rs.getString("nome"));
                    medico.setCognome(rs.getString("cognome"));
                    medico.setTipo_med(rs.getInt("tipo_med"));
                    medico.setData_nascita(rs.getDate("data_nascita"));
                    medico.setSesso(rs.getString("sesso"));
                    medico.setEmail(rs.getString("email"));
                    medico.setPassword(rs.getString("password"));
                    medico.setSalt(rs.getString("salt"));

                    try(PreparedStatement stm_te = CON.prepareStatement("SELECT * FROM tipo_visite, specializ WHERE id_spec = specializ")){
                        ResultSet rs_te = stm_te.executeQuery();
                        List<List> tipi_visita = new ArrayList<>();
                        while(rs_te.next()){
                            List tipo_visita = new ArrayList();
                            tipo_visita.add(rs_te.getString("id"));
                            tipo_visita.add(rs_te.getString("nome_visita"));
                            tipo_visita.add(rs_te.getString("specializ"));
                            tipo_visita.add(rs_te.getString("nome"));
                            tipi_visita.add(tipo_visita);
                        }
                        medico.setTipi_visite(tipi_visita);
                    }
                    
                    try(PreparedStatement stm_te = CON.prepareStatement("SELECT * FROM tipo_esame")){
                        ResultSet rs_te = stm_te.executeQuery();
                        List<List> tipi_esame = new ArrayList<>();
                        while(rs_te.next()){
                            List tipo_esame = new ArrayList();
                            tipo_esame.add(rs_te.getString("id"));
                            tipo_esame.add(rs_te.getString("nome_esame"));
                            tipi_esame.add(tipo_esame);
                        }
                        medico.setTipi_esami(tipi_esame);
                    }

                    if (medico.getTipo_med() == 0){
                        try(PreparedStatement stm_mb = CON.prepareStatement("SELECT * FROM medici_base WHERE medici_base.cf = ?")){
                            stm_mb.setString(1, medico.getCf());
                            ResultSet rs_mb = stm_mb.executeQuery();
                            rs_mb.next();
                            medico.setCitta(rs_mb.getString("citta"));
                            medico.setCod_prov(rs_mb.getString("cod_prov"));
                        }

                        try (PreparedStatement pazientiStm = CON.prepareStatement("SELECT cf FROM pazienti WHERE medico_base =  ?")) {
                            pazientiStm.setString(1, medico.getCf());
                            ResultSet rs_pazienti = pazientiStm.executeQuery();
                            List<Paziente> pazienti = new ArrayList<>();
                            while(rs_pazienti.next()) {
                                Paziente paziente = new JDBCPazienteDAO(CON).getByPrimaryKey(rs_pazienti.getString("cf"));
                                pazienti.add(paziente);
                            }
                            medico.setPazienti(pazienti);
                        }
                    }

                    else if (medico.getTipo_med() == 1){
                        try(PreparedStatement stm_ms = CON.prepareStatement("SELECT nome, spec FROM medici_spec, specializ WHERE cf = ? AND spec = id_spec")){
                            stm_ms.setString(1, medico.getCf());
                            ResultSet rs_ms = stm_ms.executeQuery();
                            rs_ms.next();
                            medico.setId_spec(rs_ms.getString("spec"));
                            medico.setSpec(rs_ms.getString("nome"));
                        }

                        List<Paziente> pazienti = new JDBCPazienteDAO(CON).getAll();
                        medico.setPazienti(pazienti);

                        List<List<List<String>>> visite_da_fare = new ArrayList<>();
                        for (int i=0; i<medico.getPazienti().size(); i++){
                            boolean find = false;
                            List<List<String>> visite_da_fare_paz = new ArrayList<>();
                            try(PreparedStatement stm_ms = CON.prepareStatement("SELECT data_visita, nome_visita, cf_med, tipo_visita FROM visita_spec, tipo_visite, specializ WHERE tipo_visite.id = visita_spec.tipo_visita AND cf_paz = ? AND specializ.id_spec = tipo_visite.specializ AND tipo_visite.specializ = ? AND risultato IS NULL")){
                                stm_ms.setString(1, medico.getPazienti().get(i).getCf());
                                stm_ms.setInt(2, Integer.parseInt(medico.getId_spec()));
                                ResultSet rs_ms = stm_ms.executeQuery();
                                while(rs_ms.next()){
                                    find = true;
                                    List<String> visita_da_fare = new ArrayList<>();
                                    visita_da_fare.add(rs_ms.getString("data_visita"));
                                    visita_da_fare.add(rs_ms.getString("nome_visita"));
                                    visita_da_fare.add(rs_ms.getString("cf_med"));
                                    visita_da_fare.add(rs_ms.getString("tipo_visita"));
                                    visite_da_fare_paz.add(visita_da_fare);
                                }
                            }
                            if (!find) {
                               List<String> visita_da_fare = new ArrayList<>();
                               visite_da_fare_paz.add(visita_da_fare);
                            }
                            visite_da_fare.add(visite_da_fare_paz);
                        }
                        medico.setVisite_da_fare(visite_da_fare);
                    }
                    return medico;
                }
            }
            return null;
        } 
        catch (SQLException ex) {
            throw new DAOException("Impossible to get the MEDICO for the passed primary key ", ex);
        }
    }

    @Override
    public List<Medico> getAll() throws DAOException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Medico getByEmailAndPassword(String email, String password) throws DAOException {
        if ((email == null) || (password == null)) throw new DAOException("Email and password are mandatory fields", new NullPointerException("email or password are null"));

        try (PreparedStatement stm = CON.prepareStatement("SELECT cf, password, salt FROM medici WHERE email = ?")) {
            stm.setString(1, email);
            try (ResultSet rs = stm.executeQuery()) {
                if (rs.next()){
                    while (!rs.getString("password").equals(generateHash(password + rs.getString("salt")))) {
                        if (!rs.next()) return null;
                    }
                    return getByPrimaryKey(rs.getString("cf"));
                }
                return null;
            }
            catch (NoSuchAlgorithmException ex) {Logger.getLogger(JDBCMedicoDAO.class.getName()).log(Level.SEVERE, null, ex);}
            return null;
        }
        catch (SQLException ex) {
            throw new DAOException("Impossible to get the list of MEDICI", ex);
        }
    }

    @Override
    public Medico getByEmailAndHash(String email, String password) throws DAOException {
        if ((email == null) || (password == null)) throw new DAOException("Email and password are mandatory fields", new NullPointerException("email or password are null"));

        try (PreparedStatement stm = CON.prepareStatement("SELECT cf FROM medici WHERE email = ? AND password = ?")) {
            stm.setString(1, email);
            stm.setString(2, password);
            ResultSet rs = stm.executeQuery();
            if (rs.next()) return getByPrimaryKey(rs.getString("cf"));
            return null;
        }
        catch (SQLException ex) {
            throw new DAOException("Impossible to get the list of MEDICI", ex);
        }
    }
    
    @Override
    public List<Ricetta> getRicette(String date, String cod_prov) throws DAOException {
        if ((date == null)) throw new DAOException("Date is a mandatory field", new NullPointerException("date is null"));
        
        try (PreparedStatement ricettaStm = CON.prepareStatement("SELECT * FROM medici, ricette, medici_base WHERE ricette.data_presc = ? AND ricette.cf_med = medici.cf AND medici_base.cf = medici.cf AND medici_base.cod_prov = ?")) {
            ricettaStm.setString(1, date);
            ricettaStm.setString(2, cod_prov);
            ResultSet rs_ricetta = ricettaStm.executeQuery();
            List<Ricetta> ricette = new ArrayList<>();
            while(rs_ricetta.next()){
                Ricetta ricetta = new Ricetta();
                ricetta.setId(rs_ricetta.getInt("id"));
                ricetta.setCf_med(rs_ricetta.getString("cf_med"));
                ricetta.setNomeMed(rs_ricetta.getString("nome"));
                ricetta.setCognomeMed(rs_ricetta.getString("cognome"));
                ricetta.setCf_paz(rs_ricetta.getString("cf_paz"));
                ricetta.setData_presc(rs_ricetta.getDate("data_presc"));
                ricetta.setOra(rs_ricetta.getString("ora"));
                ricetta.setFarmaco(rs_ricetta.getString("farmaco"));
                ricette.add(ricetta);
            }
            return ricette;
        }
        catch (SQLException ex) {
            throw new DAOException("Impossible to get the list of ", ex);
        }
    }

    @Override
    public void update(Medico medico) throws DAOException {
        if (medico == null)  throw new DAOException("MEDICO is null");
        try (PreparedStatement stm = CON.prepareStatement("SELECT * FROM medici WHERE medici.cf = ?")) {
            stm.setString(1, medico.getCf());
            try (ResultSet rs = stm.executeQuery()) {
                if (rs.next()){
                    medico.setPassword(rs.getString("password"));

                    try(PreparedStatement stm_te = CON.prepareStatement("SELECT * FROM tipo_visite, specializ WHERE id_spec = specializ")){
                        ResultSet rs_te = stm_te.executeQuery();
                        List<List> tipi_visita = new ArrayList<>();
                        while(rs_te.next()){
                            List tipo_visita = new ArrayList();
                            tipo_visita.add(rs_te.getString("id"));
                            tipo_visita.add(rs_te.getString("nome_visita"));
                            tipo_visita.add(rs_te.getString("specializ"));
                            tipo_visita.add(rs_te.getString("nome"));
                            tipi_visita.add(tipo_visita);
                        }
                        medico.setTipi_visite(tipi_visita);
                    }
                    
                    try(PreparedStatement stm_te = CON.prepareStatement("SELECT * FROM tipo_esame")){
                        ResultSet rs_te = stm_te.executeQuery();
                        List<List> tipi_esame = new ArrayList<>();
                        while(rs_te.next()){
                            List tipo_esame = new ArrayList();
                            tipo_esame.add(rs_te.getString("id"));
                            tipo_esame.add(rs_te.getString("nome_esame"));
                            tipi_esame.add(tipo_esame);
                        }
                        medico.setTipi_esami(tipi_esame);
                    }

                    if (medico.getTipo_med() == 0){
                        try(PreparedStatement stm_mb = CON.prepareStatement("SELECT * FROM medici_base WHERE medici_base.cf = ?")){
                            stm_mb.setString(1, medico.getCf());
                            ResultSet rs_mb = stm_mb.executeQuery();
                            rs_mb.next();
                            medico.setCitta(rs_mb.getString("citta"));
                            medico.setCod_prov(rs_mb.getString("cod_prov"));
                        }

                        try (PreparedStatement pazientiStm = CON.prepareStatement("SELECT cf FROM pazienti WHERE medico_base =  ?")) {
                            pazientiStm.setString(1, medico.getCf());
                            ResultSet rs_pazienti = pazientiStm.executeQuery();
                            List<Paziente> pazienti = new ArrayList<>();
                            while(rs_pazienti.next()) {
                                Paziente paziente = new JDBCPazienteDAO(CON).getByPrimaryKey(rs_pazienti.getString("cf"));
                                pazienti.add(paziente);
                            }
                            medico.setPazienti(pazienti);
                        }
                    }

                    else if (medico.getTipo_med() == 1){
                        try(PreparedStatement stm_ms = CON.prepareStatement("SELECT nome, spec FROM medici_spec, specializ WHERE cf = ? AND spec = id_spec")){
                            stm_ms.setString(1, medico.getCf());
                            ResultSet rs_ms = stm_ms.executeQuery();
                            rs_ms.next();
                            medico.setId_spec(rs_ms.getString("spec"));
                            medico.setSpec(rs_ms.getString("nome"));
                        }

                        List<Paziente> pazienti = new JDBCPazienteDAO(CON).getAll();
                        medico.setPazienti(pazienti);


                        List<List<List<String>>> visite_da_fare = new ArrayList<>();
                        for (int i=0; i<medico.getPazienti().size(); i++){
                            boolean find = false;
                            List<List<String>> visite_da_fare_paz = new ArrayList<>();
                            try(PreparedStatement stm_ms = CON.prepareStatement("SELECT data_visita, nome_visita, cf_med, tipo_visita FROM visita_spec, tipo_visite, specializ WHERE tipo_visite.id = visita_spec.tipo_visita AND cf_paz = ? AND specializ.id_spec = tipo_visite.specializ AND tipo_visite.specializ = ? AND risultato IS NULL")){
                                stm_ms.setString(1, medico.getPazienti().get(i).getCf());
                                stm_ms.setInt(2, Integer.parseInt(medico.getId_spec()));
                                ResultSet rs_ms = stm_ms.executeQuery();
                                while(rs_ms.next()){
                                    find = true;
                                    List<String> visita_da_fare = new ArrayList<>();
                                    visita_da_fare.add(rs_ms.getString("data_visita"));
                                    visita_da_fare.add(rs_ms.getString("nome_visita"));
                                    visita_da_fare.add(rs_ms.getString("cf_med"));
                                    visita_da_fare.add(rs_ms.getString("tipo_visita"));
                                    visite_da_fare_paz.add(visita_da_fare);
                                }
                            }
                            if (!find) {
                               List<String> visita_da_fare = new ArrayList<>();
                               visite_da_fare_paz.add(visita_da_fare);
                            }
                            visite_da_fare.add(visite_da_fare_paz);
                        }
                        medico.setVisite_da_fare(visite_da_fare);
                    }
                }
            }
        } catch (SQLException ex) {
            Logger.getLogger(JDBCMedicoDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public boolean insertVisita(Medico medico, int id_paz, String esito) throws DAOException {
        if ((medico == null) || (esito.length() == 0)) return false;
        
        try(PreparedStatement stm = CON.prepareStatement("INSERT INTO visita (cf_med, cf_paz, data_visita, risultato) VALUES (?, ?, ?, ?)")){
            stm.setString(1, medico.getCf());
            stm.setString(2, medico.getPazienti().get(id_paz).getCf());
            stm.setString(3, java.time.LocalDate.now().toString());
            stm.setString(4, esito);
            stm.executeUpdate();
            
            //new JDBCPazienteDAO(CON).update(medico.getPazienti().get(id_paz));
            return true;
        } catch (SQLException ex) {
            throw new DAOException("Impossible to insert VISITA", ex);
        } 
    }

    @Override
    public boolean insertVisitaSpec(Medico medico, int id_paz, int tipo_visita, String data_visita) throws DAOException {
        if ((medico == null) || (data_visita == null) || (tipo_visita == -1)) return false;
        
        try(PreparedStatement stm = CON.prepareStatement("INSERT INTO visita_spec (cf_med, cf_paz, data_visita, tipo_visita, ticket) VALUES (?, ?, ?, ?, 0)")){
            stm.setString(1, medico.getCf());
            stm.setString(2, medico.getPazienti().get(id_paz).getCf());
            stm.setString(3, data_visita);
            stm.setInt(4, tipo_visita);
            stm.executeUpdate();
            
            //new JDBCPazienteDAO(CON).update(medico.getPazienti().get(id_paz));
            return true;
        } catch (SQLException ex) {
            throw new DAOException("Impossible to insert VISITA" + ex, ex);
        } 
    }

    @Override
    public int insertRicetta(Medico medico, int id_paz, String farmaco) throws DAOException {
        if ((medico == null) || (farmaco.length() == 0)) return -1;
        
        String cf_med = medico.getCf();
        String cf_paz = medico.getPazienti().get(id_paz).getCf();
        String data = java.time.LocalDate.now().toString();
        DateFormat df = new SimpleDateFormat("HH:mm:ss");
        String ora = df.format(new Date());
        try(PreparedStatement stm = CON.prepareStatement("INSERT INTO ricette (cf_med, cf_paz, data_presc, ora, farmaco) VALUES (?, ?, ?, ?, ?)")){
            stm.setString(1, cf_med);
            stm.setString(2, cf_paz);
            stm.setString(3, data);
            stm.setString(4, ora);
            stm.setString(5, farmaco);
            stm.executeUpdate();
            
            try(PreparedStatement stm_id = CON.prepareStatement("SELECT id FROM ricette WHERE cf_med =? AND cf_paz = ? AND data_presc = ? AND ora = ? AND farmaco =  ?")){
                stm_id.setString(1, cf_med);
                stm_id.setString(2, cf_paz);
                stm_id.setString(3, data);
                stm_id.setString(4, ora);
                stm_id.setString(5, farmaco);
                ResultSet rs = stm_id.executeQuery();
                rs.next();
                int id = Integer.parseInt(rs.getString("id"));
                //Generate QR code
                ByteArrayOutputStream qrcode = QRCode.from(cf_med+";"+cf_paz+";"+data+";"+ora+";"+id+";"+farmaco).to(ImageType.JPG).stream();
                FileOutputStream fout = null;
                String routePath = JDBCMedicoDAO.class.getClassLoader().getResource(File.separator).getPath();
                try{
                    fout = new FileOutputStream(new File(routePath + File.separator + ".." + File.separator + ".." + File.separator + ".." + File.separator + ".." + File.separator + "src" + File.separator + "main" + File.separator + "webapp" + File.separator + "qrcode_ricette" + File.separator + id + ".jpg"));
                }
                catch(Exception e){}

                if (fout != null){
                    fout.write(qrcode.toByteArray());
                    fout.flush();
                    fout.close();
                    Thread.sleep(1000);
                }

                new JDBCPazienteDAO(CON).update(medico.getPazienti().get(id_paz));
                return id;
            } catch (IOException | InterruptedException ex) {
                Logger.getLogger(JDBCMedicoDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
        } catch (SQLException ex) {
            throw new DAOException("Impossible to insert VISITA", ex);
        } 
        return -1;
    }

    @Override
    public boolean updateVisitaSpec(Medico medico, int id_paz, int id_esame, String esito) throws DAOException {
        if ((medico == null) || (id_esame == -1) || (esito.length() == 0)) return false;
        
        try(PreparedStatement stm = CON.prepareStatement("UPDATE visita_spec SET risultato = ?, cf_med_visita = ? WHERE cf_med = ? AND  cf_paz = ? AND data_visita = ? AND tipo_visita = ?")){
            stm.setString(1, esito);
            stm.setString(2, medico.getCf());
            stm.setString(3, medico.getVisite_da_fare().get(id_paz).get(id_esame).get(2));
            stm.setString(4, medico.getPazienti().get(id_paz).getCf());
            stm.setString(5, medico.getVisite_da_fare().get(id_paz).get(id_esame).get(0));
            stm.setString(6, medico.getVisite_da_fare().get(id_paz).get(id_esame).get(3));
            stm.executeUpdate();
            
            //new JDBCPazienteDAO(CON).update(medico.getPazienti().get(id_paz));
            return true;
        } catch (SQLException ex) {
            throw new DAOException("Impossible to update ESAME", ex);
        } 
    }

    @Override
    public boolean insertEsame(Medico medico, int id_paz, int tipo_esame, String data_esame) throws DAOException {
        if ((medico == null) || (data_esame == null) || (tipo_esame == -1)) return false;
        
        try(PreparedStatement stm = CON.prepareStatement("INSERT INTO esame (cf_med, cf_paz, data_esame, tipo_esame, ticket) VALUES (?, ?, ?, ?, 0)")){
            stm.setString(1, medico.getCf());
            stm.setString(2, medico.getPazienti().get(id_paz).getCf());
            stm.setString(3, data_esame);
            stm.setInt(4, tipo_esame);
            stm.executeUpdate();
            
            //new JDBCPazienteDAO(CON).update(medico.getPazienti().get(id_paz));
            return true;
        } catch (SQLException ex) {
            throw new DAOException("Impossible to insert ESAME", ex);
        } 
    }

    @Override
    public boolean forgotPassword(String email, String password) throws DAOException, NoSuchAlgorithmException {
        try (PreparedStatement stm = CON.prepareStatement("SELECT cf, salt FROM medici WHERE email = ?")) {
            stm.setString(1, email);
            try (ResultSet rs = stm.executeQuery()) {
                if (rs.next()){
                    try(PreparedStatement stm1 = CON.prepareStatement("UPDATE medici SET password = ? WHERE cf = ?")){
                        stm1.setString(1, generateHash(password + rs.getString("salt")));
                        stm1.setString(2, rs.getString("cf"));
                        stm1.executeUpdate();
                        return true;
                    }
                }
            }
        } catch (SQLException ex) {
            Logger.getLogger(JDBCMedicoDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }
}
