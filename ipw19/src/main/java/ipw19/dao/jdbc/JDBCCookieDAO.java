/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ipw19.dao.jdbc;

import commons.dao.exceptions.DAOException;
import commons.dao.jdbc.JDBCDAO;
import ipw19.dao.CookieDAO;
import ipw19.entities.CookieIPW;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author orion
 */
public class JDBCCookieDAO extends JDBCDAO<CookieIPW, String> implements CookieDAO{
    
    public JDBCCookieDAO(Connection con) {
        super(con);
    }

    @Override
    public Long getCount() throws DAOException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public CookieIPW getByPrimaryKey(String primaryKey) throws DAOException {
        if (primaryKey == null) return null;
        
        try(PreparedStatement stm = CON.prepareStatement("SELECT * FROM cookies WHERE ipw19id = ?")){
            stm.setString(1, primaryKey);
            ResultSet rs = stm.executeQuery();
            if (rs.next()){
                CookieIPW cookie = new CookieIPW();
                cookie.setIpw19id(primaryKey);
                cookie.setPk(rs.getString("pk"));
                cookie.setTipo(Integer.parseInt(rs.getString("tipo")));
                return cookie;
            }
        } catch (SQLException ex) {
            throw new DAOException("Impossible to get COOKIES", ex);
        }
        return null;
    }

    @Override
    public List<CookieIPW> getAll() throws DAOException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean addCookie(String ipw19id, String pk, int tipo) {
        if ((ipw19id == null) || (pk == null)) return false;
        
        try(PreparedStatement stm = CON.prepareStatement("INSERT INTO cookies (ipw19id, pk, tipo) VALUES (?, ?, ?)")){
            stm.setString(1, ipw19id);
            stm.setString(2, pk);
            stm.setInt(3, tipo);
            stm.executeUpdate();
            
            return true;
        } catch (SQLException ex) {
            try {
                throw new DAOException("Impossible to insert COOKIES", ex);
            } catch (DAOException ex1) {
                Logger.getLogger(JDBCCookieDAO.class.getName()).log(Level.SEVERE, null, ex1);
            }
        } 
        return false;
    }

    @Override
    public boolean removeCoockie(String ipw19id) {
        if (ipw19id == null) return false;
        
        try(PreparedStatement stm = CON.prepareStatement("DELETE FROM cookies WHERE ipw19id = ?")){
            stm.setString(1, ipw19id);
            stm.executeUpdate();
            
            return true;
        } catch (SQLException ex) {
            try {
                throw new DAOException("Impossible to remove COOKIES", ex);
            } catch (DAOException ex1) {
                Logger.getLogger(JDBCCookieDAO.class.getName()).log(Level.SEVERE, null, ex1);
            }
        } 
        return false;
    }
}
