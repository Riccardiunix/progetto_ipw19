package ipw19.dao.jdbc;

import commons.dao.exceptions.DAOException;
import commons.dao.jdbc.JDBCDAO;
import ipw19.dao.PazienteDAO;
import ipw19.entities.Esame;
import ipw19.entities.VisiteSpec;
import ipw19.entities.Paziente;
import ipw19.entities.Ricetta;
import ipw19.entities.Visita;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author orion
 */
public class JDBCPazienteDAO extends JDBCDAO<Paziente, String> implements PazienteDAO{
    
    public JDBCPazienteDAO(Connection con) {
        super(con);
    }
    
    public String generateHash(String passalt) throws NoSuchAlgorithmException {
        MessageDigest digest = MessageDigest.getInstance("MD5");
        digest.update(passalt.getBytes(), 0, passalt.length());
        return new BigInteger(1, digest.digest()).toString(16);
    }

    @Override
    public Long getCount() throws DAOException {
        try (Statement stmt = CON.createStatement()) {
            ResultSet counter = stmt.executeQuery("SELECT COUNT(*) FROM pazienti");
            if (counter.next())  return counter.getLong(1);

        } catch (SQLException ex) {
            throw new DAOException("Impossible to count pazienti", ex);
        }

        return 0L;
    }

    @Override
    public Paziente getByPrimaryKey(String primaryKey) throws DAOException {
        if (primaryKey == null)  throw new DAOException("primaryKey is null");
        try (PreparedStatement stm = CON.prepareStatement("SELECT * FROM pazienti WHERE cf = ?")) {
            stm.setString(1, primaryKey);
            try (ResultSet rs = stm.executeQuery()) {
                if (rs.next()){
                    Paziente paziente = new Paziente();
                    paziente.setCf(rs.getString("cf"));
                    paziente.setNome(rs.getString("nome"));
                    paziente.setCognome(rs.getString("cognome"));
                    paziente.setData_nascita(rs.getDate("data_nascita"));
                    paziente.setLuogo_nascita(rs.getString("luogo_nascita"));
                    paziente.setSesso(rs.getString("sesso"));
                    paziente.setCod_prov(rs.getString("cod_prov"));
                    paziente.setEmail(rs.getString("email"));
                    paziente.setPassword(rs.getString("password"));
                    paziente.setSalt(rs.getString("salt"));
                    paziente.setCf_med(rs.getString("medico_base"));


                    try(PreparedStatement stm_mbn = CON.prepareStatement("SELECT nome, cognome FROM medici WHERE medici.cf = ?")){
                        stm_mbn.setString(1, paziente.getCf_med());
                        ResultSet rs_mbn = stm_mbn.executeQuery();
                        rs_mbn.next();
                            paziente.setNome_med(rs_mbn.getString("nome"));
                            paziente.setCognome_med(rs_mbn.getString("cognome"));
                    }

                    try(PreparedStatement stm_mbs = CON.prepareStatement("SELECT * FROM medici, medici_base WHERE medici.cf = medici_base.cf AND medici_base.cod_prov = ? AND medici_base.cf != ?")){
                        stm_mbs.setString(1, paziente.getCod_prov());
                        stm_mbs.setString(2, paziente.getCf_med());

                        ResultSet rs_mbs = stm_mbs.executeQuery();
                        List<List<String>> mediciDisp = new ArrayList<>();
                        while(rs_mbs.next()){
                            List<String> medico = new ArrayList<>();
                            medico.add(rs_mbs.getString("cf"));
                            medico.add(rs_mbs.getString("nome"));
                            medico.add(rs_mbs.getString("cognome"));
                            mediciDisp.add(medico);
                        }
                        paziente.setMediciDisp(mediciDisp);
                    }

                    try (PreparedStatement visiteStm = CON.prepareStatement("SELECT * FROM visita, medici WHERE cf_paz = ? AND medici.cf = visita.cf_med ORDER BY data_visita DESC")) {
                        visiteStm.setString(1, paziente.getCf());
                        ResultSet rs_visite = visiteStm.executeQuery();
                        List<Visita> visite = new ArrayList<>();
                        while(rs_visite.next()){
                            if (paziente.getUltimaVisita() == null) paziente.setUltimaVisita(rs_visite.getString("data_visita"));
                            Visita visita = new Visita();
                            visita.setCf_med(rs_visite.getString("cf_med"));
                            visita.setNomeMed(rs_visite.getString("nome"));
                            visita.setCognomeMed(rs_visite.getString("cognome"));
                            visita.setCf_paz(rs_visite.getString("cf_paz"));
                            visita.setData_visita(rs_visite.getDate("data_visita"));
                            visita.setRisultato(rs_visite.getString("risultato"));
                            visite.add(visita);
                        }
                        paziente.setVisite(visite);
                    }

                     try (PreparedStatement visite_specStm = CON.prepareStatement("SELECT * FROM medici, visita_spec, tipo_visite WHERE visita_spec.cf_paz = ? AND medici.cf = visita_spec.cf_med AND visita_spec.tipo_visita=tipo_visite.id ORDER BY data_visita DESC")) {
                        visite_specStm.setString(1, paziente.getCf());
                        ResultSet rs_visitia_spec = visite_specStm.executeQuery();
                        List<VisiteSpec> visite_spec = new ArrayList<>();
                        while(rs_visitia_spec.next()){
                            try(PreparedStatement nome_medicoStm = CON.prepareStatement("SELECT * FROM medici WHERE cf = ?")){
                                nome_medicoStm.setString(1, rs_visitia_spec.getString("cf_med_visita"));
                                ResultSet nome_medico = nome_medicoStm.executeQuery();
                                VisiteSpec visita_spec = new VisiteSpec();
                                visita_spec.setCf_med(rs_visitia_spec.getString("cf_med"));
                                visita_spec.setNomeMed(rs_visitia_spec.getString("nome"));
                                visita_spec.setCognomeMed(rs_visitia_spec.getString("cognome"));
                                visita_spec.setCf_paz(rs_visitia_spec.getString("cf_paz"));
                                visita_spec.setCf_med_visita(rs_visitia_spec.getString("cf_med_visita"));
                                visita_spec.setNomeMed_visita("");
                                visita_spec.setCognomeMed_visita("");
                                if (nome_medico.next()){
                                    visita_spec.setNomeMed_visita(nome_medico.getString("nome"));
                                    visita_spec.setCognomeMed_visita(nome_medico.getString("cognome"));
                                }
                                visita_spec.setData_visita(rs_visitia_spec.getDate("data_visita"));
                                visita_spec.setTipo_visita(rs_visitia_spec.getInt("tipo_visita"));
                                visita_spec.setNome_visita(rs_visitia_spec.getString("nome_visita"));
                                visita_spec.setRisultato(rs_visitia_spec.getString("risultato"));
                                visita_spec.setTicket(rs_visitia_spec.getInt("ticket"));
                                visite_spec.add(visita_spec);
                            }
                        }
                        paziente.setVisite_spec(visite_spec);
                    }
                     
                     try (PreparedStatement esameStm = CON.prepareStatement("SELECT * FROM medici, esame, tipo_esame WHERE esame.cf_paz = ? AND medici.cf = esame.cf_med AND esame.tipo_esame=tipo_esame.id ORDER BY data_esame DESC")) {
                        esameStm.setString(1, paziente.getCf());
                        ResultSet rs_esame = esameStm.executeQuery();
                        List<Esame> esami = new ArrayList<>();
                        while(rs_esame.next()){
                            Esame esame = new Esame();
                            esame.setCf_med(rs_esame.getString("cf_med"));
                            esame.setNomeMed(rs_esame.getString("nome"));
                            esame.setCognomeMed(rs_esame.getString("cognome"));
                            esame.setCf_paz(rs_esame.getString("cf_paz"));
                            esame.setData_esame(rs_esame.getDate("data_esame"));
                            esame.setTipo_esame(rs_esame.getInt("tipo_esame"));
                            esame.setNome_esame(rs_esame.getString("nome_esame"));
                            esame.setRisultato(rs_esame.getString("risultato"));
                            esame.setTicket(rs_esame.getInt("ticket"));
                            esami.add(esame);
                        }
                        paziente.setEsami(esami);
                    }

                     try (PreparedStatement ricettaStm = CON.prepareStatement("SELECT * FROM medici, ricette WHERE ricette.cf_paz = ? AND ricette.cf_med = medici.cf ORDER BY ricette.data_presc DESC")) {
                        ricettaStm.setString(1, paziente.getCf());
                        ResultSet rs_ricetta = ricettaStm.executeQuery();
                        List<Ricetta> ricette = new ArrayList<>();
                        while(rs_ricetta.next()){
                            if (paziente.getUltimaRicetta() == null) paziente.setUltimaRicetta(rs_ricetta.getString("data_presc"));
                            Ricetta ricetta = new Ricetta();
                            ricetta.setId(rs_ricetta.getInt("id"));
                            ricetta.setCf_med(rs_ricetta.getString("cf_med"));
                            ricetta.setNomeMed(rs_ricetta.getString("nome"));
                            ricetta.setCognomeMed(rs_ricetta.getString("cognome"));
                            ricetta.setCf_paz(rs_ricetta.getString("cf_paz"));
                            ricetta.setData_presc(rs_ricetta.getDate("data_presc"));
                            ricetta.setFarmaco(rs_ricetta.getString("farmaco"));
                            ricette.add(ricetta);
                        }
                        paziente.setRicette(ricette);
                    }
                    return paziente;
                }
            }
            return null;
        } catch (SQLException ex) {
            throw new DAOException("Impossible to get the PAZIENTE for the passed primary key", ex);
        }
    }

    @Override
    public List<Paziente> getAll() throws DAOException {
        List<Paziente> pazienti = new ArrayList<>();
        try (Statement stm = CON.createStatement()) {
            try (ResultSet rs = stm.executeQuery("SELECT cf FROM pazienti")) {
                while (rs.next()) pazienti.add(getByPrimaryKey(rs.getString("cf")));
            }
        } catch (SQLException ex) {
            throw new DAOException("Impossible to get the list of  all PAZIENTI", ex);
        }
        return pazienti;
    }

    @Override
    public Paziente getByEmailAndPassword(String email, String password) throws DAOException {
         if ((email == null) || (password == null)) throw new DAOException("Email and password are mandatory fields", new NullPointerException("email or password are null"));

        try (PreparedStatement stm = CON.prepareStatement("SELECT cf, password, salt FROM pazienti WHERE email = ?")) {
            stm.setString(1, email);
            try (ResultSet rs = stm.executeQuery()) {
                if (rs.next()){
                    while (!rs.getString("password").equals(generateHash(password + rs.getString("salt")))) {
                        if (!rs.next()) return null;
                    }
                    return getByPrimaryKey(rs.getString("cf"));
                }
                return null;
            } catch (NoSuchAlgorithmException ex) {
                 Logger.getLogger(JDBCPazienteDAO.class.getName()).log(Level.SEVERE, null, ex);
             }
        } catch (SQLException ex) {
            throw new DAOException("Impossible to get the list of users", ex);
        }
        return null;
    }

    @Override
    public Paziente getByEmailAndHash(String email, String password) throws DAOException {
        if ((email == null) || (password == null)) throw new DAOException("Email and password are mandatory fields", new NullPointerException("email or password are null"));

        try (PreparedStatement stm = CON.prepareStatement("SELECT cf FROM pazienti WHERE email = ? AND password = ?")) {
            stm.setString(1, email);
            stm.setString(2, password);
            ResultSet rs = stm.executeQuery();
            if (rs.next()) return getByPrimaryKey(rs.getString("cf"));
            return null;
        } catch (SQLException ex) {
            throw new DAOException("Impossible to get the list of users", ex);
        }
    }
    
    public void update(Paziente paziente) throws DAOException{
        if (paziente == null)  throw new DAOException("Paziente is null");
        try (PreparedStatement stm = CON.prepareStatement("SELECT * FROM pazienti WHERE cf = ?")) {
            stm.setString(1, paziente.getCf());
            try (ResultSet rs = stm.executeQuery()) {
                if (rs.next()){
                    paziente.setPassword(rs.getString("password"));
                    paziente.setCf_med(rs.getString("medico_base"));

                    try(PreparedStatement stm_mbn = CON.prepareStatement("SELECT nome, cognome FROM medici WHERE medici.cf = ?")){
                        stm_mbn.setString(1, paziente.getCf_med());
                        ResultSet rs_mbn = stm_mbn.executeQuery();
                        rs_mbn.next();
                        paziente.setNome_med(rs_mbn.getString("nome"));
                        paziente.setCognome_med(rs_mbn.getString("cognome"));
                    }

                    try(PreparedStatement stm_mbs = CON.prepareStatement("SELECT * FROM medici, medici_base WHERE medici.cf = medici_base.cf AND medici_base.cod_prov = ? AND medici_base.cf != ?")){
                        stm_mbs.setString(1, paziente.getCod_prov());
                        stm_mbs.setString(2, paziente.getCf_med());

                        ResultSet rs_mbs = stm_mbs.executeQuery();
                        List<List<String>> mediciDisp = new ArrayList<>();
                        while(rs_mbs.next()){
                            List<String> medico = new ArrayList<>();
                            medico.add(rs_mbs.getString("cf"));
                            medico.add(rs_mbs.getString("nome"));
                            medico.add(rs_mbs.getString("cognome"));
                            mediciDisp.add(medico);
                        }
                        paziente.setMediciDisp(mediciDisp);
                    }

                    try (PreparedStatement visiteStm = CON.prepareStatement("SELECT * FROM visita, medici WHERE cf_paz = ? AND medici.cf = visita.cf_med ORDER BY data_visita DESC")) {
                        visiteStm.setString(1, paziente.getCf());
                        ResultSet rs_visite = visiteStm.executeQuery();
                        List<Visita> visite = new ArrayList<>();
                        while(rs_visite.next()){
                            if (paziente.getUltimaVisita() == null) paziente.setUltimaVisita(rs_visite.getString("data_visita"));
                            Visita visita = new Visita();
                            visita.setCf_med(rs_visite.getString("cf_med"));
                            visita.setNomeMed(rs_visite.getString("nome"));
                            visita.setCognomeMed(rs_visite.getString("cognome"));
                            visita.setCf_paz(rs_visite.getString("cf_paz"));
                            visita.setData_visita(rs_visite.getDate("data_visita"));
                            visita.setRisultato(rs_visite.getString("risultato"));
                            visite.add(visita);
                        }
                        paziente.setVisite(visite);
                    }

                     try (PreparedStatement visite_specStm = CON.prepareStatement("SELECT * FROM medici, visita_spec, tipo_visite WHERE visita_spec.cf_paz = ? AND medici.cf = visita_spec.cf_med AND visita_spec.tipo_visita=tipo_visite.id ORDER BY data_visita DESC")) {
                        visite_specStm.setString(1, paziente.getCf());
                        ResultSet rs_visitia_spec = visite_specStm.executeQuery();
                        List<VisiteSpec> visite_spec = new ArrayList<>();
                        while(rs_visitia_spec.next()){
                            try(PreparedStatement nome_medicoStm = CON.prepareStatement("SELECT * FROM medici WHERE cf = ?")){
                                nome_medicoStm.setString(1, rs_visitia_spec.getString("cf_med_visita"));
                                ResultSet nome_medico = nome_medicoStm.executeQuery();
                                VisiteSpec visita_spec = new VisiteSpec();
                                visita_spec.setCf_med(rs_visitia_spec.getString("cf_med"));
                                visita_spec.setNomeMed(rs_visitia_spec.getString("nome"));
                                visita_spec.setCognomeMed(rs_visitia_spec.getString("cognome"));
                                visita_spec.setCf_paz(rs_visitia_spec.getString("cf_paz"));
                                visita_spec.setCf_med_visita(rs_visitia_spec.getString("cf_med_visita"));
                                visita_spec.setNomeMed_visita("");
                                visita_spec.setCognomeMed_visita("");
                                if (nome_medico.next()){
                                    visita_spec.setNomeMed_visita(nome_medico.getString("nome"));
                                    visita_spec.setCognomeMed_visita(nome_medico.getString("cognome"));
                                }
                                visita_spec.setData_visita(rs_visitia_spec.getDate("data_visita"));
                                visita_spec.setTipo_visita(rs_visitia_spec.getInt("tipo_visita"));
                                visita_spec.setNome_visita(rs_visitia_spec.getString("nome_visita"));
                                visita_spec.setRisultato(rs_visitia_spec.getString("risultato"));
                                visita_spec.setTicket(rs_visitia_spec.getInt("ticket"));
                                visite_spec.add(visita_spec);
                            }
                        }
                        paziente.setVisite_spec(visite_spec);
                    }
                     
                     try (PreparedStatement esameStm = CON.prepareStatement("SELECT * FROM medici, esame, tipo_esame WHERE esame.cf_paz = ? AND medici.cf = esame.cf_med AND esame.tipo_esame=tipo_esame.id ORDER BY data_esame DESC")) {
                        esameStm.setString(1, paziente.getCf());
                        ResultSet rs_esame = esameStm.executeQuery();
                        List<Esame> esami = new ArrayList<>();
                        while(rs_esame.next()){
                            Esame esame = new Esame();
                            esame.setCf_med(rs_esame.getString("cf_med"));
                            esame.setNomeMed(rs_esame.getString("nome"));
                            esame.setCognomeMed(rs_esame.getString("cognome"));
                            esame.setCf_paz(rs_esame.getString("cf_paz"));
                            esame.setData_esame(rs_esame.getDate("data_esame"));
                            esame.setTipo_esame(rs_esame.getInt("tipo_esame"));
                            esame.setNome_esame(rs_esame.getString("nome_esame"));
                            esame.setRisultato(rs_esame.getString("risultato"));
                            esame.setTicket(rs_esame.getInt("ticket"));
                            esami.add(esame);
                        }
                        paziente.setEsami(esami);
                    }

                     try (PreparedStatement ricettaStm = CON.prepareStatement("SELECT * FROM medici, ricette WHERE ricette.cf_paz = ? AND ricette.cf_med = medici.cf ORDER BY ricette.data_presc DESC")) {
                        ricettaStm.setString(1, paziente.getCf());
                        ResultSet rs_ricetta = ricettaStm.executeQuery();
                        List<Ricetta> ricette = new ArrayList<>();
                        while(rs_ricetta.next()){
                            if (paziente.getUltimaRicetta() == null) paziente.setUltimaRicetta(rs_ricetta.getString("data_presc"));
                            Ricetta ricetta = new Ricetta();
                            ricetta.setId(rs_ricetta.getInt("id"));
                            ricetta.setCf_med(rs_ricetta.getString("cf_med"));
                            ricetta.setNomeMed(rs_ricetta.getString("nome"));
                            ricetta.setCognomeMed(rs_ricetta.getString("cognome"));
                            ricetta.setCf_paz(rs_ricetta.getString("cf_paz"));
                            ricetta.setData_presc(rs_ricetta.getDate("data_presc"));
                            ricetta.setFarmaco(rs_ricetta.getString("farmaco"));
                            ricette.add(ricetta);
                        }
                        paziente.setRicette(ricette);
                    }
                }
            }
        } catch (SQLException ex) {
            throw new DAOException("error "+ex, ex);
        }
    }

    @Override
    public boolean updateTicketVisita(Paziente paziente, int id_visita) {
        if (paziente == null) return false;
        
        try(PreparedStatement stm = CON.prepareStatement("UPDATE visita_spec SET ticket = 1 WHERE cf_med = ? AND  cf_paz = ? AND data_visita = ? AND tipo_visita = ?")){
            stm.setString(1, paziente.getVisite_spec().get(id_visita).getCf_med());
            stm.setString(2, paziente.getCf());
            stm.setString(3, paziente.getVisite_spec().get(id_visita).getData_visita().toString());
            stm.setInt(4, paziente.getVisite_spec().get(id_visita).getTipo_visita());
            stm.executeUpdate();
            
            //update(paziente);
            return true;
        } catch (Exception ex) {
            try {
                throw new DAOException("Impossible to UPDATE ticket", ex);
            } catch (DAOException ex1) {
                Logger.getLogger(JDBCPazienteDAO.class.getName()).log(Level.SEVERE, null, ex1);
            }
        }
        return false;
    }

    @Override
    public boolean updateMedicoBase(Paziente paziente, String cf_med) {
        if ((paziente == null) || (cf_med.length() == 0) || (cf_med.equals("-1"))) return false;
        
        try(PreparedStatement stm = CON.prepareStatement("UPDATE pazienti SET medico_base = ? WHERE cf = ?")){
            stm.setString(1, cf_med);
            stm.setString(2, paziente.getCf());
            stm.executeUpdate();
            
            //update(paziente);
            return true;
        } catch (Exception ex) {
            try {
                throw new DAOException("Impossible to UPDATE ticket", ex);
            } catch (DAOException ex1) {
                Logger.getLogger(JDBCPazienteDAO.class.getName()).log(Level.SEVERE, null, ex1);
            }
        }
        return false;
    }
    
    @Override
    public boolean updateTicketEsame(Paziente paziente, int id_esame) {
        if (paziente == null) return false;
        
        try(PreparedStatement stm = CON.prepareStatement("UPDATE esame SET ticket = 1 WHERE cf_med = ? AND  cf_paz = ? AND data_esame = ? AND tipo_esame = ?")){
            stm.setString(1, paziente.getEsami().get(id_esame).getCf_med());
            stm.setString(2, paziente.getCf());
            stm.setString(3, paziente.getEsami().get(id_esame).getData_esame().toString());
            stm.setInt(4, paziente.getEsami().get(id_esame).getTipo_esame());
            stm.executeUpdate();
            
            //update(paziente);
            return true;
        } catch (Exception ex) {
            try {
                throw new DAOException("Impossible to UPDATE ticket", ex);
            } catch (DAOException ex1) {
                Logger.getLogger(JDBCPazienteDAO.class.getName()).log(Level.SEVERE, null, ex1);
            }
        }
        return false;
    }

    @Override
    public void changePassword(Paziente paziente, String new_pass) throws DAOException {
        if (paziente != null || new_pass != null){
        
            try(PreparedStatement stm = CON.prepareStatement("UPDATE pazienti SET password = ? WHERE cf = ?")){
                stm.setString(1, generateHash(new_pass + paziente.getSalt()));
                stm.setString(2, paziente.getCf());
                stm.executeUpdate();

                update(paziente);
            } catch (Exception ex) {
                try {
                    throw new DAOException("Impossible to UPDATE ticket", ex);
                } catch (DAOException ex1) {
                    Logger.getLogger(JDBCPazienteDAO.class.getName()).log(Level.SEVERE, null, ex1);
                }
            }
        }
    }

    @Override
   public boolean forgotPassword(String email, String password) throws DAOException, NoSuchAlgorithmException {
        try (PreparedStatement stm = CON.prepareStatement("SELECT cf, salt FROM pazienti WHERE email = ?")) {
            stm.setString(1, email);
            try (ResultSet rs = stm.executeQuery()) {
                if (rs.next()){
                    try(PreparedStatement stm1 = CON.prepareStatement("UPDATE pazienti SET password = ? WHERE cf = ?")){
                        stm1.setString(1, generateHash(password + rs.getString("salt")));
                        stm1.setString(2, rs.getString("cf"));
                        stm1.executeUpdate();
                        return true;
                    }
                }
            }
        } catch (SQLException ex) {
            Logger.getLogger(JDBCMedicoDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }
}
