/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ipw19.dao;

import commons.dao.DAO;
import ipw19.entities.CookieIPW;

/**
 *
 * @author orion
 */
public interface CookieDAO extends  DAO<CookieIPW, String>{
    
    public boolean addCookie(String ipw19id, String pk, int tipo);
    
    public boolean removeCoockie(String ipw19id);
}
