/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ipw19.dao;

import commons.dao.DAO;
import commons.dao.exceptions.DAOException;
import ipw19.entities.Medico;
import ipw19.entities.Ricetta;
import java.security.NoSuchAlgorithmException;
import java.util.List;

/**
 *
 * @author orion
 */
public interface MedicoDAO extends DAO<Medico, String>{
    
    public Medico getByEmailAndPassword(String email, String password) throws DAOException;

    public Medico getByEmailAndHash(String email, String password) throws DAOException;
    
    public List<Ricetta> getRicette(String date, String cod_prov) throws DAOException;
    
    public void update(Medico medico) throws DAOException;
    
    public boolean insertVisita(Medico medico, int id_paz, String esito) throws DAOException;
    
    public boolean insertVisitaSpec(Medico medico, int id_paz, int tipo_esame, String data_visita) throws DAOException;
    
    public boolean insertEsame(Medico medico, int id_paz, int tipo_esame, String data_esame) throws DAOException;
    
    public int insertRicetta(Medico medico, int id_paz, String farmaco) throws DAOException;
    
    public boolean updateVisitaSpec(Medico medico, int id_paz, int id_esame, String esito)  throws DAOException;
    
    public boolean forgotPassword(String email, String password) throws DAOException, NoSuchAlgorithmException;
}