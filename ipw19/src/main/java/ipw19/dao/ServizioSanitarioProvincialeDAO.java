/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ipw19.dao;

import commons.dao.DAO;
import commons.dao.exceptions.DAOException;
import ipw19.entities.ServizioSanitarioProvinciale;
import java.security.NoSuchAlgorithmException;

/**
 *
 * @author orion
 */
public interface ServizioSanitarioProvincialeDAO extends DAO<ServizioSanitarioProvinciale, String>{
    
     public ServizioSanitarioProvinciale getByEmailAndPassword(String email, String password) throws DAOException;
     
     public boolean updateEsame(ServizioSanitarioProvinciale ssp, int id_esame, String esito)  throws DAOException;
     
     public ServizioSanitarioProvinciale getByEmailAndHash(String email, String password) throws DAOException;
     
     public void update(ServizioSanitarioProvinciale ssp) throws DAOException;
     
     public boolean forgotPassword(String email, String password) throws DAOException, NoSuchAlgorithmException;
}
