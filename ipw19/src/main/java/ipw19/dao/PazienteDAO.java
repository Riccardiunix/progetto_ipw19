/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ipw19.dao;

import commons.dao.DAO;
import commons.dao.exceptions.DAOException;
import ipw19.entities.Paziente;
import java.security.NoSuchAlgorithmException;

/**
 *
 * @author orion
 */
public interface PazienteDAO extends DAO<Paziente, String>{
    
    public Paziente getByEmailAndPassword(String email, String password) throws DAOException;

    public Paziente getByEmailAndHash(String email, String password) throws DAOException;
    
    public boolean updateTicketVisita(Paziente paziente, int id_visita) throws DAOException;
    
    public boolean updateTicketEsame(Paziente paziente, int id_esame) throws DAOException;
    
    public boolean updateMedicoBase(Paziente paziente, String cf_med) throws DAOException;
    
    public void update(Paziente paziente) throws DAOException;
    
    public void changePassword(Paziente paziente, String new_pass) throws DAOException;
    
    public boolean forgotPassword(String email, String password) throws DAOException, NoSuchAlgorithmException;
}
