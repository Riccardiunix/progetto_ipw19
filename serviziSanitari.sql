-- MariaDB dump 10.17  Distrib 10.4.11-MariaDB, for Linux (x86_64)
--
-- Host: localhost    Database: serviziSanitari
-- ------------------------------------------------------
-- Server version	10.4.11-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `citta`
--

DROP TABLE IF EXISTS `citta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `citta` (
  `citta` varchar(45) NOT NULL,
  `cod_prov` char(2) NOT NULL,
  `regione` varchar(45) DEFAULT NULL,
  `provincia` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`citta`,`cod_prov`),
  KEY `citta_ibfk_1` (`cod_prov`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `citta`
--

LOCK TABLES `citta` WRITE;
/*!40000 ALTER TABLE `citta` DISABLE KEYS */;
INSERT INTO `citta` VALUES ('Ala','TN','Trentino','Trento'),('Arco','TN','Trentino','Trento'),('Bolzano','BZ','Trentino','Bolzano'),('Brunico','BZ','Trentino','Bolzano'),('Folgaria','TN','Trentino','Trento'),('Merano','BZ','Trentino','Bolzano'),('Rovereto','TN','Trentino','Trento'),('Trento','TN','Trentino','Trento'),('Vipiteno','BZ','Trentino','Bolzano');
/*!40000 ALTER TABLE `citta` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cookies`
--

DROP TABLE IF EXISTS `cookies`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cookies` (
  `ipw19id` char(16) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `pk` varchar(16) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tipo` int(11) DEFAULT NULL,
  PRIMARY KEY (`ipw19id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cookies`
--

LOCK TABLES `cookies` WRITE;
/*!40000 ALTER TABLE `cookies` DISABLE KEYS */;
INSERT INTO `cookies` VALUES ('3fb8a108b68d5720','BZ',2);
/*!40000 ALTER TABLE `cookies` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `esame`
--

DROP TABLE IF EXISTS `esame`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `esame` (
  `cf_paz` char(16) NOT NULL,
  `cf_med` char(16) NOT NULL,
  `data_esame` date NOT NULL,
  `risultato` varchar(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tipo_esame` int(11) NOT NULL,
  `ticket` int(11) DEFAULT NULL,
  PRIMARY KEY (`cf_paz`,`cf_med`,`data_esame`,`tipo_esame`),
  KEY `esame_FK` (`tipo_esame`),
  KEY `esame_FK_2` (`cf_med`),
  CONSTRAINT `esame_FK` FOREIGN KEY (`tipo_esame`) REFERENCES `tipo_esame` (`id`),
  CONSTRAINT `esame_FK_1` FOREIGN KEY (`cf_paz`) REFERENCES `pazienti` (`cf`),
  CONSTRAINT `esame_FK_2` FOREIGN KEY (`cf_med`) REFERENCES `medici` (`cf`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `esame`
--

LOCK TABLES `esame` WRITE;
/*!40000 ALTER TABLE `esame` DISABLE KEYS */;
INSERT INTO `esame` VALUES ('BGNGLN29P18B220R','DVTGCL57B28A952G','2020-02-02',NULL,2,0),('BNVLNU98L57M067J','DVTGCL57B28A952G','2020-01-30','Negativo',1,1),('BNVLNU98L57M067J','DVTGCL57B28A952G','2020-01-30','La mammografia non ha svelato alcun problema',3,1),('BNVLNU98L57M067J','DVTGCL57B28A952G','2020-01-31',NULL,5,0),('BNVLNU98L57M067J','DVTGCL57B28A952G','2020-02-02','Massa rimossa si consigliano ulteriori accetamenti',1,1),('BNVLNU98L57M067J','DVTGCL57B28A952G','2020-02-02','Nessun Danno',2,0),('CCCGLI13L46A372X','CRTLGU58E01L378A','2019-11-16','Negativo',3,1),('CHRCRR77D43H612N','CRNMRV91T65L378A','2020-01-29','Trovata ciste benigna. Si consiglia la rimozione della stessa',1,1),('CHRCRR77D43H612N','CRNMRV91T65L378A','2020-01-29','Nulla di anormale, si consiglia di effettuare un nuovo controllo tra 6 mesi',5,1),('CHRCRR77D43H612N','CRNMRV91T65L378A','2020-01-31','Positivo, svolgere ulteriori accertamenti',3,1),('CMMLSS96M66M067W','GTNSBZ86S05A952W','2020-01-31','Possibile anomalia nel tratto rettale. Probabile sviluppo di massa tumorale',5,0),('CPRBRN88P04A952Q','GTNSBZ86S05A952W','2020-01-29','Rinvenute cellule neoplastiche, si consiglia TAC',4,0),('CRDSMN28T14L378D','CRNMRV91T65L378A','2020-01-28','Nulla di grave rinvenuto. Se il dolore persiste si  consiglia assunzione di Oki',5,0),('CSLZOE78C59M067D','GTNSBZ86S05A952W','2020-01-29','Tutto in regola',1,1),('FZZLNE93C43D651D','MNRLRN68M57H612G','2020-01-29','Nessuna anomalia',4,1),('LCTFST89B18A372T','CRTLGU58E01L378A','2020-01-29','La risonanza magnetica ha rilevato una compressione delle vertebre lombari',2,0),('MZZBRC33H52B220T','DVTGCL57B28A952G','2020-01-30','Sta male',5,0),('STRFBA49L14D651V','CRNMRV91T65L378A','2020-01-31','Nessuna frattura è stata rilevata',2,0);
/*!40000 ALTER TABLE `esame` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `medici`
--

DROP TABLE IF EXISTS `medici`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `medici` (
  `cf` char(16) NOT NULL,
  `nome` varchar(50) NOT NULL,
  `cognome` varchar(50) NOT NULL,
  `email` varchar(256) NOT NULL,
  `password` char(32) NOT NULL,
  `salt` varchar(255) NOT NULL,
  `data_nascita` date NOT NULL,
  `sesso` char(1) DEFAULT NULL,
  `tipo_med` int(11) DEFAULT NULL,
  PRIMARY KEY (`cf`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `medici`
--

LOCK TABLES `medici` WRITE;
/*!40000 ALTER TABLE `medici` DISABLE KEYS */;
INSERT INTO `medici` VALUES ('CRNMRV91T65L378A','Minervina','Carinci','minervina.carinci@gmail.com','bd64c893f2f3a7fcc3db671ce2cbb7d7','cbomocpvjuqheihxfcsklhtoutteaqcytfxbftddsxiovzjtotugkrbftuilpfilapbtbwmoijkagaltktbawejqrbrilgjudajrwtukrfzispzeqjwmloqnpownyqxgfrmjossmjiyykpqpmeczzvdfxqmjotzcemsnlcbhqaz','1991-12-25','F',0),('CRTLGU58E01L378A','Luigi','Cerutti','luigi.cerutti@gmail.com','4bc29f467fb1cb040cf2919df7e32996','zexk9yx6r0y9n9wfosn3o4frnnhpxswwa4c9ita6bw9irsepyerkpuv1i074qutvg4ep8vku7q1p0db3iovdmmlckzvcvcx9sq90i20mxyrcp396impc8s5bx4y9cn14bdlywv9y1q9apvtkeudech8cvijb9pwrgdqxd8m1s8gvhywb7al9zwzyqxtkfqo06du9obhrs4py9yah68v1esjsmpusbn2waxjyb1763x5v2wdh1kbyt9k','1958-05-01','M',0),('DVTGCL57B28A952G','Graciliano','De Vitto','graciliano.de.vitto@gmail.com','f735271dec054189eb8a7f673f40a410','ucskdudncmlrstuiuhjttafiuffdgkmrgeridnbvfpnfowdfqckevxmzeyqzpzfnlpzchckokcyvdp','1957-02-28','M',0),('GRDNDR62S12A952M','Andrea','Giardini','andrea.giardini@gmail.com','2ebbaaf57c2295925099aa85018b18ec','wqvcbzusydyixjrruvmucudskbgozhyywlubmesxyzkdvjdbugczkkoqfftuugocqnvtyvfkhntxeyvnsxuxshijkpvrooznhsawndxswpsxrcwjmxlraaweoljwjubknyxnwuuoduolakkrlzkxxxnfuqfmzljzvbqmlbrfbzkhjdeyhmlfidfnyfteubthwvjruqdbtrrzfrjbngpuatudsdagnpjmgnicxfybyvplsqhhshbfngb','1962-11-12','M',1),('GTNSBZ86S05A952W','Sabazio','Gaetani','sabazio.gaetani@gmail.com','ba6ab8b6b2bfaec9c893db3d650af21b','ihxrrzobryrhngihtliidhfoipasvvobvokqmvpalbveubfxibxyyhzlqloylpenuhiececkseqbxpsrjufqvdttbjkvkxnbisi','1986-11-05','M',0),('MNRLRN68M57H612G','Lorena','Minervino','lorena.minervino@gmail.com','76f69270e09225ea09521ad3ee6f980b','ybiffukfwwtsvbinxpcbhpjbvttneczucpogzysrkcogqhsnrwgurbierbicfgqeqkblquhwqgbialetjlzaywozliqgjuuuuakwcryewbfqqxafhnzazvmhaccesjibwiticdpfaqbzioqwnnmpuiiqskohtbudhkskrtjbzrtouofozjnyjfavjvhfptzy','1968-08-17','F',0),('TMMTNN78E22L378Z','Tommaso','Trenini','tommaso.trenini@gmail.com','7c73844fc267ebba5baeeb72e2c2cab0','bxsbtfwafubnzoviqzpzuskhnadltnrrxkyiabtzrursflzinhksmunztzivzliind','1978-05-22','M',1);
/*!40000 ALTER TABLE `medici` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `medici_base`
--

DROP TABLE IF EXISTS `medici_base`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `medici_base` (
  `cf` char(16) NOT NULL,
  `cod_prov` char(2) NOT NULL,
  `citta` varchar(45) NOT NULL,
  PRIMARY KEY (`cf`),
  KEY `medici_base_ibfk_2` (`cod_prov`),
  CONSTRAINT `medici_base_ibfk_1` FOREIGN KEY (`cf`) REFERENCES `medici` (`cf`),
  CONSTRAINT `medici_base_ibfk_2` FOREIGN KEY (`cod_prov`) REFERENCES `citta` (`cod_prov`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `medici_base`
--

LOCK TABLES `medici_base` WRITE;
/*!40000 ALTER TABLE `medici_base` DISABLE KEYS */;
INSERT INTO `medici_base` VALUES ('CRNMRV91T65L378A','TN','Trento'),('CRTLGU58E01L378A','TN','Trento'),('DVTGCL57B28A952G','BZ','Bolzano'),('GTNSBZ86S05A952W','BZ','Bolzano'),('MNRLRN68M57H612G','TN','Rovereto');
/*!40000 ALTER TABLE `medici_base` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `medici_spec`
--

DROP TABLE IF EXISTS `medici_spec`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `medici_spec` (
  `cf` char(16) DEFAULT NULL,
  `spec` int(11) DEFAULT NULL,
  KEY `cf` (`cf`),
  KEY `medici_spec_FK` (`spec`),
  CONSTRAINT `medici_spec_FK` FOREIGN KEY (`spec`) REFERENCES `specializ` (`id_spec`),
  CONSTRAINT `medici_spec_ibfk_1` FOREIGN KEY (`cf`) REFERENCES `medici` (`cf`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `medici_spec`
--

LOCK TABLES `medici_spec` WRITE;
/*!40000 ALTER TABLE `medici_spec` DISABLE KEYS */;
INSERT INTO `medici_spec` VALUES ('TMMTNN78E22L378Z',3),('GRDNDR62S12A952M',2);
/*!40000 ALTER TABLE `medici_spec` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pazienti`
--

DROP TABLE IF EXISTS `pazienti`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pazienti` (
  `cf` char(16) NOT NULL,
  `nome` varchar(25) DEFAULT NULL,
  `cognome` varchar(25) DEFAULT NULL,
  `data_nascita` date DEFAULT NULL,
  `luogo_nascita` varchar(25) DEFAULT NULL,
  `sesso` char(1) DEFAULT NULL,
  `medico_base` char(16) DEFAULT NULL,
  `cod_prov` char(2) DEFAULT NULL,
  `email` varchar(256) DEFAULT NULL,
  `password` char(32) DEFAULT NULL,
  `salt` varchar(255) NOT NULL,
  PRIMARY KEY (`cf`),
  KEY `medico_base` (`medico_base`),
  KEY `cod_prov` (`cod_prov`),
  CONSTRAINT `pazienti_ibfk_1` FOREIGN KEY (`medico_base`) REFERENCES `medici_base` (`cf`),
  CONSTRAINT `pazienti_ibfk_2` FOREIGN KEY (`cod_prov`) REFERENCES `citta` (`cod_prov`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pazienti`
--

LOCK TABLES `pazienti` WRITE;
/*!40000 ALTER TABLE `pazienti` DISABLE KEYS */;
INSERT INTO `pazienti` VALUES ('BGNGLN29P18B220R','Giuliano','Biagioni','1929-09-18','Brunico','M','DVTGCL57B28A952G','BZ','giuliano.biagioni@gmail.com','7dc44531e1793ac0124fc3502e1e7933','txbhbmslnqyepjjuxtnfgdkcbqonsuzmgfufbfrpefydfuqdfhqbqvzpltccrprbqpqgtwherzhhurfrhpascakhnhiydikhmyehgxxmyjmtnqoodmnjvequpwmbxjpmqhivqyitfdebmfeunxcqwuxmcujpabqzynbr'),('BLTDNL68E22L378M','Daniele','Belotti','1968-05-22','Trento','M','CRTLGU58E01L378A','TN','daniele.belotti@gmail.com','362731d502df001db2f7052e7332b042','vaaodscehwwgscsfjkavkuzjdfhitsguqgmtycrwswsbrcptiodwofofrdmyhzkaeogylflktuxxhzuytdwgoyfturkifexedniegvnkeyzssrxlcedbmiukkvxemkcgcxnkwyxsoukdxxjizlnlpuiwncrhqaifveiimcguzjikpntyhlnlyyfvohkpjq'),('BNCGRG16S01A372X','Giorgio','Bianconi','1916-11-01','Arco','M','CRTLGU58E01L378A','TN','giorgio.bianconi@gmail.com','324342f3dd8ad1d4480bc2156f67d1f5','twogmmydaebhhorkisrlfdtzwiwrjbaqknumjxegsagmmzfqpznwmkexciswshkfgushxbbvsmkvtitupnbamvbbdhbvbiclysovedtaqnihinakgzggkfslawimrfrmzpxumokfisgzsqbfrnyqqmiwkxqggktifkyohrarnbuesms'),('BNVLNU98L57M067J','Luna','Bonaventura','1998-07-17','Vipiteno','F','DVTGCL57B28A952G','BZ','luna.bonaventura@gmail.com','22cee644dba3111300d2d48042bedd86','hfcfyfklvvwnpquhwxirlsstdlidcnhladiqftphjcexweoufafycoty'),('CCCGLI13L46A372X','Giulia','Cuccaro','2013-07-06','Arco','F','CRTLGU58E01L378A','TN','giulia.cuccaro@gmail.com','86528e81c2737bfa391a5e5f4dd073b0','hwpaixxuigbthzwbruwixcdcmgkjjibywlyldealkymqtnrvdollbqgsnwxfdghkktsgbhmvkyxyfxkibvculjokusygiurnnuvkrfihgtkcpwpstaerctmxdlemqhwvytvyiwfqgxwcdifwmxgulrjjyqopbdvwsqjajuvobriwzoqaqolhpjyfsowuywmquzvuuqeggdqcznjvxsvnxkmgojwxkkzcmnzzsnbo'),('CHRCRR77D43H612N','Chiara',' Rodia','1977-04-03','Rovereto','F','CRNMRV91T65L378A','TN','chiara.rodia@gmail.com','b464b72a3d7fb0a91467f09849a745c3','mljnrnfabeqvkeslunlhgnqqjnamorjmqgvrkrjsctqkjrniaunglac'),('CLMSLV14M11L378O','Silvio','Columbo','1914-08-11','Trento','M','CRNMRV91T65L378A','TN','silvio.columbo@gmail.com','2643473d49a963a4b64867bdd96b7137','kqcdbltxqqnnyvpanporuwgohcjfbacufjwqvkspgbpkquandbephpqzszvwvnettxvomxqvcbiydbftycbzetetvchmvwdxmahseoukhtzzsdxwwtkvofdpjtltbzsgengtmurhteklizctnluqcjkkcjxoeclqademukhuhhtpxssgihiizihkpjqvfoeyjccaxrufqcnrvmnerrkm'),('CMMLSS96M66M067W','Alessio','Commisso','1996-08-26','Vipiteno','M','GTNSBZ86S05A952W','BZ','alessio.commisso@gmail.com','4c1c703b5c368936c131c7b66cee84c0','enrifkxrtyagjunfgpgxipsgdrfogbccscmoakuzmbfvlvpnygiyhaavkaxijhsinwlnwmsmuzecottefhcctompnwinhgjsycupibddtduikurbzcnndevwtxatxlkyiuggcerkixpt'),('CPRBRN88P04A952Q','Bruno','Caporale','1988-09-04','Bolzano','M','GTNSBZ86S05A952W','BZ','bruno.caporale@gmail.com','f10173d086d4fa35e6917965ccf54f30','eiehqnpfdialnzopnmqsityvvpczzljmtqtoszpzqbagadimwjrbxgjgsswghjcqpbhmxpqhnbtmaqzrsyauemvemsffnotyizjwqo'),('CRDSMN28T14L378D','Simone','Carducci','1928-12-14','Trento','M','CRNMRV91T65L378A','TN','simone.carducci@gmail.com','a3cac0e6cb3c39614b7ca8cef2a37b99','pixtdoyilftcttdexausfsgtsxeeyvkzxjvsplaxxchwxankjzymgeryqnsdhvxvyoarwwsqkscgpkeimgnsypowtzrqpdepipuxbatcvacfppljcjwyeuehlfat'),('CRLCHR07A49A952X','Chiara','Carlino','2007-01-09','Bolzano','F','GTNSBZ86S05A952W','BZ','chiara.carlino@gmail.com','2f9cdb68f2b7cc7c809795f6484b9246','eshurywwuspjrbfucalumwwaktdkwnagjqieiwvgqzltnzgsmcjnwb'),('CRRGLI17P08F132M','Giulio','Carricato','1917-09-08','Merano','M','DVTGCL57B28A952G','BZ','giulio.carricato@gmail.com','56d27a14600188961962c8d3fd1b91b4','soqizivmp'),('CSLZOE78C59M067D','Zoe','Casella','1978-03-19','Vipiteno','F','GTNSBZ86S05A952W','BZ','zoe.casella@gmail.com','b0193e88718e9fca0113ba311a788f89','ykwtrsevfqgafffqcvtejozizlggyokhwuhgnxetuvdpjdrtpjgxjkqypkei'),('CTRLCU95C17L378F','Luca','Cotrone','1995-03-17','Trento','M','CRTLGU58E01L378A','TN','luca.cotrone@gmail.com','a0966ef149bd0d39ebc88d1512b49d75','msbhuthnpvxfoogluupqexgcanhjjehgmvznlcydzvqenepjakbxlrhrftyajbthxoycifcagljfwksotbsoixkkeohwbmlprkxhsvznhvpybpgw'),('FRLMRZ92C14B220W','Marzio','Farella','1992-03-14','Brunico','M','GTNSBZ86S05A952W','BZ','marzio.farella@gmail.com','8864534902c14dd474173d34d5663f83','zasosjoseafdffuuplvqxjvulyzelqgsmvwqavohqmidbizejvuyurfjzenzdcuwgvqbkdbtmvzppggsmdfrjewnabyq'),('FRTSTT69B45A952L','Isotta','Fratangelo','1969-02-05','Bolzano','F','GTNSBZ86S05A952W','BZ','isotta.fratangelo@gmail.com','49c745e4fd3efd8a834e2a74c6b5ea6e','oqngpewuqywbxmuqfhbrvgbnlbaxjczlgmdpvisgxtwohqqtdtbmlvcbtprxdkozegukcucarwazogoaxlxwxfffzixabuqrbtzxfajgudgteypbosvdlgamcyvbpavntnjktpeizmfyxmekwihkneizcxdviynhsrvokerrzdbnxncbavvrwjmifdrhbbeimbrrmnzstpqiwmkkhczy'),('FZZLNE93C43D651D','Elena','Fazzi','1993-03-03','Folgaria','F','MNRLRN68M57H612G','TN','elena.fazzi@gmail.com','8ad3f89ae7abb80d76cb63742eeff335','eratlvdaanfponyoxntwttwvijhqqxtfimsbsioezltrkutdwnfmkpfoehfxwuwfrrbbdhrypzsddurloacjfxwtmgzzpxkyodwiwcurzlwzkfqtxahmhtpqiqbzkuvobtkixwvehkcaccilecnlgqjzeukbwcnmfpvfsygtiieqyqszpjjup'),('GHLLND39A23H612K','Leonida','Ghilarducci','1939-01-23','Rovereto','M','CRNMRV91T65L378A','TN','leonida.ghilarducci@gmail.com','e8542ccb52a53708a737452464add783','libimvsgrmuzklurqhcnxldrklzzmhpvcevogqepfxlsvkrolsovlrhedoukylnsdaqouuqpatjssfhkjuinotdetlwwjfkemzgbfgiuuievzapogypukzytquzhnjdcrjezzdqrjvuasqxbyz'),('GLSMHL43C51F132J','Michela','Galasso','1948-03-11','Merano','F','GTNSBZ86S05A952W','BZ','michela.galasso@gmail.com','a48a5aad8730a55e0861ecd050c9bd9f','hucbenigkvzmxhlhmvuukypkdowczqoauhnitckprmnzalsjxqepuuwbcivarynnzmjnqsefaksooyunxgqikxhukgccvjnbacbruspawahtdppuufwiexfuvixgythozgqfjpjjyngljlrmnonkuam'),('GNNSLA55D13A952E','Ausilio','Gianni','1955-04-13','Bolzano','M','GTNSBZ86S05A952W','BZ','ausilio.gianni@gmail.com','b5d5ab1953a53a77a8c4d106f947fdc8','fsbohloczjgslrribjkxkzqlibzvpqvzzrsqqxyllketjspemcbprxcvoiiephzwbwpgmuappnztztfjnpipmznerlhktj'),('GRMLBT21P42L378M','Elisabetta','Grimaldi','1921-09-02','Trento','F','CRTLGU58E01L378A','TN','elisabetta.grimaldi@gmail.com','17024d3728b5f52d680a2e49b4e06b00','umtptliikulovhxppcztrqmcqvnupeddrlygfankdqespvjofkhcbmuxoucsszmtoltchrpekyftkkrykhjpytdreibmrxfsqhlsipxmcpepmhtpclgtdorlyqkvjbnzgpxendqfetxcewcgxicbsictvtakdymyyeaxthkhrdawecpkizewfpwjiysscxhjtnnegymhntroykuqhnhvbxvnjdofvxbufkfpgttckugqcacjsuksucucixw'),('GRNNCL06M18F132J','Nicola','Guarneri','2006-08-18','Merano','M','DVTGCL57B28A952G','BZ','nicola.guarneri@gmail.com','cfa78959e3f49bf39a2f2a44f88a0aaf','gzqiaxpfghvmqpbqpflzoliqryiuepxtvillkgofzfhtfsgdautbuluragfbgxdiriefuyklvjtrrfqtkkytsnecdmrsvpwzsrdkbvvnkhknioanmljnauwryvvixtnexmwqsdppnjvnu'),('LCTFST89B18A372T','Fausto','Locati','1989-02-18','Arco','M','CRTLGU58E01L378A','TN','fausto.locati@gmail.com','8d2b0a639c03ae61b10a4cd18d52e7be','zmjhnsnlcfjfhuzctkxgcnekfmhvvxipnueacenirg'),('LNELRZ58H52D651L','Lucrezia','Lena','1958-06-12','Folgaria','F','CRTLGU58E01L378A','TN','lucrezia.lena@gmail.com','ab37f6fc7902e6172391a42c4decbeb6','gjgpmvsgstoxbpdsgzqvczdaidtzpinmjxnoajoxhkcvrfgzcecqkxbkmyypoirokxludhihchcvailifwfafhrtxoffpvocngwzhtdlhdovydbogvmnmblbeqzccctrbduznysxfalwyokrtthpptxzbjgubfpbyvtncytxlleuyxdkmhzcnwiljhn'),('LNRDMZ14E18L378S','Domezio','Leonardis','2014-05-18','Trento','M','CRNMRV91T65L378A','TN','domezio.leonardis@gmail.com','f816e6787e6a27e82f94bf1514a5248b','ysxviodgwgtndpyxyvqatdzaorxrsamihnsdwoemcilmhcysmnifxgxjnttoxdlhbpcbzsrfuxhamisxpkwrxzmjrnkyigxhwqajrln'),('LTALLN36T52H612N','Liliana','Alioto','1936-12-12','Rovereto','F','MNRLRN68M57H612G','TN','liliana.alioto@gmail.com','f991c5ec7740ee9c796ed7aa45d939fe','iixgcbfxjyheononrylkichhuejrruigrofzldzpezujhcecnjtmglikhbsunicsyqpvdmdfduqbnvduremsz'),('LVRLGO58C67B220B','Olga','Lovera','1958-03-27','Brunico','F','DVTGCL57B28A952G','BZ','olga.lovera@gmial.com','1c9dce8f861311a0e8351bfc240e8225','ulgefqjhxuthmbgafyhirgnctvleruaiydf'),('MGRVNT94S44A116C','Valentina',' Maugeri','1994-11-04','Ala','F','CRNMRV91T65L378A','TN','valentina.maugeri@gmail.com','ff25ff25bebfe570457deaaf527cb891','oprbmmofqzbbtofxwdbbnhihthzbbxfzqzyukgjiqeaujbkrqtebuzjrnxtevjclhcupkfbimbwnjgpacwmldxmkoahtwwafnirlafqcnylsqjebancodkxpeavhgxjsxtsblfsovwkxidkcieahqxkmehvpohzpbhxubydsouwcjyfegzszvvboumncllqqeicizopjitcrmeietwrlqbuqjluvuzienxbwsxiruaue'),('MNCRNI68H57L378F','Irene','Mancuso','1968-06-17','Trento','F','CRNMRV91T65L378A','TN','irene.mancuso@gmail.com','e85e12085c4153bf476db21c72e4e87b','nldkroywdvvviccwlxtnpdbsxfhbdclmvyhylbnihagnpvykjsuapmqpwytvjmqtkvityamkppjsogyudevjfpihcimfnuqeqjdfjvdkcsoqcetwgifzffwf'),('MRBGLI67C04D651I','Giulio','Mirabella','1967-03-04','Folgaria','M','CRTLGU58E01L378A','TN','giulio.mirabella@gmail.com','8103b487e8910a5f64a0a9ea15852dcf','udomzmrpsmgzyevfybaqboobxlxpoqvcgtmmnmugqkcenqhztmlvcmvecigaxchzxazwrtlfhxtccdcko'),('MRLFRC09P56F132W','Federica','Morello','2009-09-16','Merano','F','DVTGCL57B28A952G','BZ','federica.morello@gmail.com','2690119d8aac9d98465666bb808fa160','npvhjfehiuzhzqjkikwpckeqvtcpcnkbuiqjtrmkneurhdiumeyrktxbrzhtizkzjubzukzraucnsqoxiazozleefzz'),('MRSMRA15C49H612A','Maria','Marsiglia','2015-03-09','Rovereto','F','MNRLRN68M57H612G','TN','maria.marsiglia@gmail.com','244254bda1a60a4cd6e94c907adf577d','jdwhyxbdyyjvifjihzwvignusrkglwwfn'),('MZZBFC45D18M067D','Bonifacio','Mazzetti','1945-04-18','Vipiteno','M','DVTGCL57B28A952G','BZ','bonifacio.mazzetti@gmail.com','920c4b4db1a37b99795526b4fd3ec8df','iwlxwumvdzosuhxbpyrphscrdntzcgujgcnrbjuhssyvvasfykovbfndobhjgutnbtwtcwpuvkwguwvcxarlxyufjurbqytsnujthernpqoyoqfelrdvlgtburqnxphbtxkrfvysefeiviznvsrodvikeadlolkmxeedtjtmuvrqijmfvlgnkcnscgctxx'),('MZZBRC33H52B220T','Beatrice','Mazzotta','1933-06-12','Brunico','F','DVTGCL57B28A952G','BZ','beatrice.mazzotta@gmail.com','162ca1ac2e900fba20f90fdaa73ea4ae','dpzbogkslmimocufhnxjtugzflibubkdbvkshtbffcafyuanayekalcpd'),('MZZGTN98B02H612F','Agostino','Mazzaro','2019-02-02','Rovereto','M','MNRLRN68M57H612G','TN','agostino.mazzaro@gmail.com','de1052f6410383ef9d9d0a5bc4ef5a04','rufxqcvupdgoocxypwtjzjvrxbfjyhjtqqguywyurggathtbocqctjwbidotcdkiiduhcolveawiboggecd'),('NGLSRT36S24B220Q','Spartaco','Angelotti','1936-11-24','Brunico','M','DVTGCL57B28A952G','BZ','spartaco.angelotti@gmail.com','7859c816eb97d4106c24666195df018b','hcdqagzgoyxnjuphhatqwzbhmgsqaawdbintjiqevurlhuxpoiebdpprmxylcdeosnzcaqpwcfegmcncwbiwahsjhtudhdgdkhestfmakwmdefmlofyuspbmuexqqaiqmxfixiayrqwmxlvuidrmwimiiidkebxbaojtaiflizawsfpeetqnfhdubmdglfuemthhyd'),('PLSNDR01S01A952E','Andrea','Peluso','2001-11-01','Bolzano','M','GTNSBZ86S05A952W','BZ','andrea.peluso@gmail.com','2a48f2856b13238f315f12137c2f1456','jvgforqhdshodfgmudzgcfcuyeibjhisrpezfoerqpvmcgwwzjvxgrfdkjqozcydzrejzguiexytkhrovdeudl'),('PRRSNT74D53L378N','Simonetta','Pierro','1974-04-13','Trento','F','MNRLRN68M57H612G','TN','simonetta.pierro@gmail.com','74eada8c272d058ee240200ab4b7ba43','uqekuhyoumfsvhqdoniqyizudophycifcqpukwongibpctjukerxlxnlqnweehsbkaojykxsygojtgzwoklthvvogyqtuwtrqgboaiangqvwwfulbqiioagjlsigranadlgkllbqqsnksfdajwruqakbijfwlbnoxsse'),('PRSLLD88E23H612T','Leopoldo','Presutti','1988-05-23','Rovereto','M','MNRLRN68M57H612G','TN','leopoldo.presutti@gmail.com','e04592c7844361f5807178bdf9ae09fc','eqfotrxgnavixvj'),('RCCCRN74M61F132S','Caterina','Ricco','1974-08-21','Merano','F','GTNSBZ86S05A952W','BZ','caterina.ricco@gmail.com','7a1f77d9f080446bc7d8ed79fc9296e0','hnyhfgdhazghoepukdagsmpffjozkaqfmfqcyanwlbzqcwyrjqthsedmiemgdjfqcrinetbmaxctesnancrorpjegganqcefqdrdhcnilwkfwaieonexbjysxspjyrmizqzrbwceiczbgqtzuomauqhozkijmyfbuxxkjrthtfuzhxygsruasklbheefnavzwucccopziecvivoicxddvrasgkktmclslisfideybukpu'),('RGNPTR23L03H612X','Pietro ','Ragno','1923-07-03','Rovereto','M','MNRLRN68M57H612G','TN','pietro.ragno@gmail.com','9129865f3fdccbaba9de05c256e1b749','ianhlsjghzwflgtdgfrhhhjilzjnecemllnpxiqazwyxycspknyxznclbjzmhlpsgcdylthqllvrglrogadltbraebhqyvjvcanwyetorapjyfqappvsarojgxjmpewyislgpkoalganictwzrevyzoysfkioexrjtguunzdatsncwmhxqszfgzemobbaxdxakhstigdttamarnkcgwyyoulvvmzixnn'),('RLIGLM02E28A116F','Girolamo','Riolo','2002-05-28','Ala','M','MNRLRN68M57H612G','TN','girolamo.riolo@gmail.com','d48056ed945c4c1a2c67fa286283a204','xddruvzpjbhsjnwihbxqbbkbkwkxcwkbsmwvajedwznucifjqdpfxfwxgxjiplzryfoxwlfbxmbmmiueakrzkiakxucputgutnhfeyekzfxpgxahvqxkxuaxzmcstvlopgiysjxita'),('SCHRSL12B68A372N','Rossella','Schirra','2012-02-28','Arco','F','CRTLGU58E01L378A','TN','rossellaschirra@gmail.com','5a431f367a611baee9609868c2b7f4d4','azwmyywnrfykazrslgjohufwgvtzshheywvbehglczdvadkyxwypspwiybaoxexyerqcyajhoqkaz'),('SLLMRA72A41A116','Mario','Sollami','1972-01-01','Ala','M','CRNMRV91T65L378A','TN','mario.sollami@gmail.com','c07a3f7736bc68da42929b83b45f5013','znrfrkrhizhcjhqovpwynjsugribluzwtcrvozikvspuiptrmarmbrafsuailxodpifuwxqivqjidnqgfltzqchqxmumszbzpqsqatjqhcdimqeyrkfybceohlbfiunsyodvjavjefxxtjcvtzwplcqylaoxnnnezz'),('SRRLVC04A71A116L','Ludovica','Sarra','2004-01-31','Ala','F','MNRLRN68M57H612G','TN','ludovica.sarra@gmail.com','c29991590a57668ad26e42d014368f30','hutroljoyzplcdrwnobyixgfwazcusvuxo'),('STRFBA49L14D651V','Fabio','Storti','1949-07-14','Folgaria','M','CRNMRV91T65L378A','TN','fabio.storti@gmail.com','8e8c3d4d431f6db6df8fd211ec132da2','avpidrsagvndizyvmfoihpkizrihmqckihgdnnyaqqzzowdvnyguptzimcpofqynvrputotxfyzsgxdxdmqbwrlehofdfleocvyalgzgkutjswdwgoyrsxujp'),('VLLGLR21S59L378K','Gloria','Valli','1921-11-19','Trento','F','MNRLRN68M57H612G','TN','gloria.valli@gmail.com','3de446cff1c5bbf143f16c08d0e9fef2','poqwokapphzfivizbowtpuorwijgdzwrcxwjudesofjnnehpsjccxntsevzhsauffoekeuxnqncwutq'),('VLURNZ90B10H612U','Renzo','Vuolo','1990-02-10','Rovereto','M','MNRLRN68M57H612G','TN','renzo.vuolo@gmail.com','6665823114961eeb99769a5ba1eefce3','poqwokapphzfivizbowtpuorwijgdzwrcxwjudesofjnnehpsjccxntsevzhsauffoekeuxnqncwutq'),('VNNFBN15L47F132A','Fabiano','Venanzi','1915-07-07','Merano','M','DVTGCL57B28A952G','BZ','fabiano.venanzi@gmail.com','c13b4fa4dab8a7873f1e867722edb08e','kfqvxcwyyii'),('ZNALSN98M51L378N','Alessandra','Zane','1998-08-11','Trento','F','CRNMRV91T65L378A','TN','alessandra.zane@gmail.com','30d3c69c0b8ab81490ef7f3eb0a3301e','jotvpccbzcdmrbzloffjzgradmcsyfkungfbxvtdpksouwywgxsbypnxejnnyhyugzstcpkyvmblbldhbudsfyopxuimtlfuxdclabfdjkhhcxyavwwazzvxhkgdrmysbypdqiiwcfhsvjjjvqtpkyhdaoaxofsmxucvurmvdyipekdnkdvpzdst');
/*!40000 ALTER TABLE `pazienti` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `provincia`
--

DROP TABLE IF EXISTS `provincia`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `provincia` (
  `cod_prov` char(2) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `nome` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`cod_prov`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `provincia`
--

LOCK TABLES `provincia` WRITE;
/*!40000 ALTER TABLE `provincia` DISABLE KEYS */;
INSERT INTO `provincia` VALUES ('BZ','Bolzano'),('TN','Trento');
/*!40000 ALTER TABLE `provincia` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ricette`
--

DROP TABLE IF EXISTS `ricette`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ricette` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cf_med` char(16) DEFAULT NULL,
  `cf_paz` char(16) DEFAULT NULL,
  `data_presc` date DEFAULT NULL,
  `farmaco` varchar(256) DEFAULT NULL,
  `ora` time DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `cf_med` (`cf_med`),
  KEY `cf_paz` (`cf_paz`),
  CONSTRAINT `ricette_ibfk_1` FOREIGN KEY (`cf_med`) REFERENCES `medici_base` (`cf`),
  CONSTRAINT `ricette_ibfk_2` FOREIGN KEY (`cf_paz`) REFERENCES `pazienti` (`cf`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ricette`
--

LOCK TABLES `ricette` WRITE;
/*!40000 ALTER TABLE `ricette` DISABLE KEYS */;
INSERT INTO `ricette` VALUES (1,'CRTLGU58E01L378A','GRMLBT21P42L378M','2020-01-31','Ibuprofene','16:41:35'),(2,'DVTGCL57B28A952G','BGNGLN29P18B220R','2020-01-31','Voltaren','16:50:29'),(3,'CRTLGU58E01L378A','LNELRZ58H52D651L','2020-01-31','Benagol','17:17:47'),(4,'CRNMRV91T65L378A','CHRCRR77D43H612N','2020-01-31','Brufen','17:17:58'),(6,'CRNMRV91T65L378A','CLMSLV14M11L378O','2020-01-31','Ciprosol','17:21:37'),(7,'CRNMRV91T65L378A','CRDSMN28T14L378D','2020-01-31','Diaxone','17:25:04'),(8,'DVTGCL57B28A952G','BNVLNU98L57M067J','2020-01-31','Emorril','17:25:51'),(9,'CRNMRV91T65L378A','GHLLND39A23H612K','2020-01-31','Fenazil','17:26:25'),(10,'CRNMRV91T65L378A','STRFBA49L14D651V','2020-01-31','Potassion','17:28:02');
/*!40000 ALTER TABLE `ricette` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `specializ`
--

DROP TABLE IF EXISTS `specializ`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `specializ` (
  `nome` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `id_spec` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id_spec`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `specializ`
--

LOCK TABLES `specializ` WRITE;
/*!40000 ALTER TABLE `specializ` DISABLE KEYS */;
INSERT INTO `specializ` VALUES ('Ematologia',1),('Radiologia',2),('Cardiologia',3),('Otorinolaringoiatria',4),('Neurologia',5),('Geriatria',6),('Pediatria',7);
/*!40000 ALTER TABLE `specializ` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ssp`
--

DROP TABLE IF EXISTS `ssp`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ssp` (
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` char(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cod_prov` char(2) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `salt` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`cod_prov`),
  CONSTRAINT `ssp_FK` FOREIGN KEY (`cod_prov`) REFERENCES `provincia` (`cod_prov`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ssp`
--

LOCK TABLES `ssp` WRITE;
/*!40000 ALTER TABLE `ssp` DISABLE KEYS */;
INSERT INTO `ssp` VALUES ('ssp.bolzano@gmail.com','968fb700a0f8e1aa215258ab88d122b5','BZ','opbxaznzzpmszmnkptrrttdmjjrkodecehmdylluycysbwkjdqlacszijyjxghhwjjebukuvhpkjcdknlaqypjuilezlhcjsnhfssiepnjrkotlgcguljkubkxynislaqewlerogwzsbzsiplqxnfuyokktetmjlqlhuaorcndslhscgefhobdrmxddydnrwcyknjirmmfwxhwfvlhqgvqlakyrcofjuollrwwuyghnfmhdgyeyagrg'),('ssp.trento@gmail.com','f2025065a7ddd580006cfcc931f1f215','TN','mksvritwsmmvshkdooekfohwinufatxpowigabiifenspbqipergtbaeoctmyjxm');
/*!40000 ALTER TABLE `ssp` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tipo_esame`
--

DROP TABLE IF EXISTS `tipo_esame`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tipo_esame` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome_esame` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tipo_esame`
--

LOCK TABLES `tipo_esame` WRITE;
/*!40000 ALTER TABLE `tipo_esame` DISABLE KEYS */;
INSERT INTO `tipo_esame` VALUES (1,'Agoaspirazione'),(2,'Risonanza magnetica'),(3,'Mammografia'),(4,'Esame della prostata'),(5,'Colonscopia');
/*!40000 ALTER TABLE `tipo_esame` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tipo_visite`
--

DROP TABLE IF EXISTS `tipo_visite`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tipo_visite` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome_visita` varchar(50) DEFAULT NULL,
  `specializ` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `tipo_esame_FK` (`specializ`),
  CONSTRAINT `tipo_esame_FK` FOREIGN KEY (`specializ`) REFERENCES `specializ` (`id_spec`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tipo_visite`
--

LOCK TABLES `tipo_visite` WRITE;
/*!40000 ALTER TABLE `tipo_visite` DISABLE KEYS */;
INSERT INTO `tipo_visite` VALUES (1,'Prelievo arterioso',1),(2,'Defecografia',2),(3,'RX bacino',2),(4,'RX cranio',2),(5,'Urografia',2),(6,'Stratigrafia trachea',2),(7,'Ecocardiogramma',3),(8,'Eco-stress',3),(9,'Stroboscopia',4),(10,'Ecografia',2),(11,'Test dell\'udito',4);
/*!40000 ALTER TABLE `tipo_visite` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `visita`
--

DROP TABLE IF EXISTS `visita`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `visita` (
  `cf_med` char(16) CHARACTER SET utf8mb4 NOT NULL,
  `cf_paz` char(16) CHARACTER SET utf8mb4 NOT NULL,
  `data_visita` date NOT NULL,
  `risultato` varchar(256) CHARACTER SET utf8mb4 DEFAULT NULL,
  PRIMARY KEY (`cf_med`,`cf_paz`,`data_visita`),
  KEY `cf_paz` (`cf_paz`),
  CONSTRAINT `visita_ibfk_1` FOREIGN KEY (`cf_med`) REFERENCES `medici` (`cf`),
  CONSTRAINT `visita_ibfk_2` FOREIGN KEY (`cf_paz`) REFERENCES `pazienti` (`cf`)
) ENGINE=InnoDB DEFAULT CHARSET=utf16;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `visita`
--

LOCK TABLES `visita` WRITE;
/*!40000 ALTER TABLE `visita` DISABLE KEYS */;
INSERT INTO `visita` VALUES ('CRNMRV91T65L378A','CHRCRR77D43H612N','2019-04-11','Primi sintomi di influenza'),('CRNMRV91T65L378A','CHRCRR77D43H612N','2019-04-18','Raffreddore'),('CRNMRV91T65L378A','CRDSMN28T14L378D','2020-01-28','Primi sintomi di influenza intestinale'),('CRNMRV91T65L378A','GHLLND39A23H612K','2020-01-28','Il paziente presenta un\'ematoma sul quadricipite femorale'),('CRNMRV91T65L378A','LNRDMZ14E18L378S','2020-01-28','Il paziente presenta uno strappo muscolare al linguine'),('CRNMRV91T65L378A','MGRVNT94S44A116C','2020-01-29','Il paziente presenta otite'),('CRNMRV91T65L378A','MNCRNI68H57L378F','2019-11-05','Il paziente è in perfetta saluta'),('CRNMRV91T65L378A','SLLMRA72A41A116','2019-11-05','Il paziente è in perfetta saluta'),('CRNMRV91T65L378A','STRFBA49L14D651V','2019-11-05','Congiuntivite'),('CRNMRV91T65L378A','STRFBA49L14D651V','2020-01-31','Possibile livello scarso di potassio'),('CRNMRV91T65L378A','ZNALSN98M51L378N','2020-01-28','Il paziente presenta una lieve febbre'),('CRNMRV91T65L378A','ZNALSN98M51L378N','2020-01-29','Infiammazione alla gola'),('CRTLGU58E01L378A','BLTDNL68E22L378M','2019-10-17','Sta bene'),('CRTLGU58E01L378A','BNCGRG16S01A372X','2019-10-24','Il paziente presenta una lieve distorsione alla caviglia'),('CRTLGU58E01L378A','CCCGLI13L46A372X','2019-10-24','Nulla di inusuale'),('CRTLGU58E01L378A','CTRLCU95C17L378F','2019-10-17','Influenza'),('CRTLGU58E01L378A','FZZLNE93C43D651D','2019-11-01','Raffreddore'),('CRTLGU58E01L378A','LCTFST89B18A372T','2020-01-28','Il paziente presenta un indolenzimento muscolare alle articolazioni inferiori'),('DVTGCL57B28A952G','LVRLGO58C67B220B','2020-01-30','La signora sta bene'),('GTNSBZ86S05A952W','CMMLSS96M66M067W','2020-01-29','Il paziente presenta dolori lomabri'),('MNRLRN68M57H612G','LTALLN36T52H612N','2020-01-29','Il paziente è in perfetta salute'),('MNRLRN68M57H612G','MRSMRA15C49H612A','2020-01-29','Tutto nella norma'),('MNRLRN68M57H612G','RGNPTR23L03H612X','2019-11-05','Ha il raffreddore'),('MNRLRN68M57H612G','SRRLVC04A71A116L','2019-11-05','Ha l\'influenza'),('MNRLRN68M57H612G','VLLGLR21S59L378K','2020-01-29','Il paziente prensenta una lieve lesione femorale'),('MNRLRN68M57H612G','VLURNZ90B10H612U','2019-11-05','Ha la frebbe');
/*!40000 ALTER TABLE `visita` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `visita_spec`
--

DROP TABLE IF EXISTS `visita_spec`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `visita_spec` (
  `cf_med` char(16) NOT NULL,
  `cf_paz` char(16) NOT NULL,
  `data_visita` date NOT NULL,
  `tipo_visita` int(11) NOT NULL,
  `risultato` varchar(256) DEFAULT NULL,
  `ticket` int(11) DEFAULT NULL,
  `cf_med_visita` char(16) DEFAULT NULL,
  PRIMARY KEY (`cf_med`,`data_visita`,`cf_paz`,`tipo_visita`),
  KEY `cf_paz` (`cf_paz`),
  KEY `tipo_esame` (`tipo_visita`),
  CONSTRAINT `visita_spec_ibfk_1` FOREIGN KEY (`cf_med`) REFERENCES `medici_base` (`cf`),
  CONSTRAINT `visita_spec_ibfk_2` FOREIGN KEY (`cf_paz`) REFERENCES `pazienti` (`cf`),
  CONSTRAINT `visita_spec_ibfk_3` FOREIGN KEY (`tipo_visita`) REFERENCES `tipo_visite` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `visita_spec`
--

LOCK TABLES `visita_spec` WRITE;
/*!40000 ALTER TABLE `visita_spec` DISABLE KEYS */;
INSERT INTO `visita_spec` VALUES ('CRNMRV91T65L378A','CHRCRR77D43H612N','2020-01-31',8,'I battiti sono elevati.',0,'TMMTNN78E22L378Z'),('DVTGCL57B28A952G','BGNGLN29P18B220R','2020-01-30',4,'Il cranio non presenta alcun tipo di lesione',0,'GRDNDR62S12A952M'),('DVTGCL57B28A952G','BGNGLN29P18B220R','2020-01-30',7,'Il cranio non presenta alcun tipo di lesione',0,'GRDNDR62S12A952M'),('DVTGCL57B28A952G','BNVLNU98L57M067J','2020-01-30',3,NULL,1,NULL),('DVTGCL57B28A952G','BNVLNU98L57M067J','2020-01-30',8,NULL,0,NULL),('DVTGCL57B28A952G','NGLSRT36S24B220Q','2020-01-30',3,'Lieve frattura ma non è necessario  l\'intervento',0,'GRDNDR62S12A952M'),('DVTGCL57B28A952G','NGLSRT36S24B220Q','2020-01-30',8,'Nessuna anomalia nei tratti cardiovascolari ',0,'TMMTNN78E22L378Z');
/*!40000 ALTER TABLE `visita_spec` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping routines for database 'serviziSanitari'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-01-31 17:47:07
